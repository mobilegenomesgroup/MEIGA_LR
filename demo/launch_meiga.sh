
# A) Load environment
source /MEIGA/.venv/bin/activate
export PYTHONHASHSEED=0


# B) Set general input
threads=4 # number of threads
technology=ont # technology -> 'rs', 'sq', 'ccs', 'ont'
reference=/reference/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna
databases=/databases/meiga_db
meigaDir=/MEIGA


# C) Set sample specific input
bam=$meigaDir/demo/simulationRegions_tumour.bam
normalBam=$meigaDir/demo/simulationRegions_normal.bam
outDir=$meigaDir/demo/output
mkdir -p $outDir
refs='chr1,chr2,chr3,chr4,chr5,chr6,chr7,chr8,chr9,chr10,chr11,chr12,chr13,chr14,chr15,chr16,chr17,chr18,chr19,chr20,chr21,chr22,chrX,chrY'


# D) Launch MEIGA
time python $meigaDir/MEIGA-LR.py \
$bam \
$technology \
$reference \
$databases \
--normalBam $normalBam \
--refs $refs \
-p $threads \
-o $outDir \
1> $outDir/MEIGA.out \
2> $outDir/MEIGA.err


