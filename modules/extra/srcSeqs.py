#!/usr/bin/env python
#coding: utf-8

'''
Module to obtain reconstructed sources elements sequences from those elements on the reference genome
'''

if __name__ == '__main__':
    
    ####################
    ## Import modules ##
    ####################

    ## DEPENDENCIES ##
    # External
    import multiprocessing as mp
    import pysam
    import subprocess
    import argparse
    import sys
    import os
    import multiprocessing as mp

    # Internal
    from GAPI import log
    from GAPI import unix
    from GAPI import formats
    from GAPI import assembly

    mp.set_start_method('spawn')

    ######################
    ## Get user's input ##
    ######################

    ## 1. Define parser ##
    ### Mandatory arguments
    parser = argparse.ArgumentParser(description='Script for reconstructing haplotype-specific source elements sequences. Source elements coordinates should be provided in targetBins arg')
    parser.add_argument('bam', help='Input bam file. Will correspond to the tumour sample in the PAIRED mode')
    parser.add_argument('targetBins', type=str, help='Bed file containing source elements coordinates')
    parser.add_argument('sex', type=str, help='Donor sex [XX, XY]')
    parser.add_argument('technology', help='Sequencing technology used to generate the data: rs for PacBio RSII, sq for PacBio Sequel, ccs for PacBio CCS reads and ont for Oxford Nanopore')
    parser.add_argument('reference', help='Reference genome in fasta format. An index of the reference generated with samtools faidx must be located in the same directory')
    parser.add_argument('refDir', help='Directory containing reference databases (consensus sequences, source elements...)')

    ### Optional arguments
    ## General
    parser.add_argument('--minMAPQ', default=10, dest='minMAPQ', type=int, help='Minimum mapping quality required for each read. Default: 20')
    parser.add_argument('-p', '--processes', default=1, dest='processes', type=int, help='Number of processes. Default: 1')
    parser.add_argument('-o', '--outDir', default=os.getcwd(), dest='outDir', help='Output directory. Default: current working directory')

    ## 2. Parse user's input and initialize variables ##
    args = parser.parse_args()

    ### Mandatory arguments
    bam = args.bam
    technology = args.technology
    reference = args.reference
    refDir = args.refDir
    sex = args.sex

    ### Optional arguments
    ## General
    targetBins = args.targetBins
    mapq = args.minMAPQ
    processes = args.processes
    outDir = args.outDir
    unix.mkdir(outDir)


    ##############################################
    ## Display configuration to standard output ##
    ##############################################
    scriptName = os.path.basename(sys.argv[0])
    scriptName = os.path.splitext(scriptName)[0]
    version='1.0'
    print()
    print('***** ', scriptName, version, 'configuration *****')
    print('*** Mandatory arguments ***')
    print('bam: ', bam)
    print('targetBins: ', targetBins)
    print('donorSex: ', sex)
    print('technology: ', technology)
    print('reference: ', reference)
    print('refDir: ', refDir, "\n")

    print('*** Optional arguments ***')
    print('** General **')
    print('mapq: ', mapq)
    print('processes: ', processes)
    print('outDir: ', outDir, "\n")

    print('***** Executing ', scriptName, '.... *****', "\n")


    ##########
    ## CORE ## 
    ##########

    ## 1. Create configuration dictionary
    #######################################
    ## Mandatory
    confDict = {}
    confDict['technology_subtype'] = technology
    confDict['technology'] = "NANOPORE" if technology == "ont" else "PACBIO"
    confDict['sex'] = sex

    ## General
    confDict['source'] = 'MEIGA-' + version
    confDict['MAPQ'] = mapq
    confDict['processes'] = processes
    confDict['refDir'] = refDir

    ## 2. Define SRC_REF CLASS
    ##############################
    class SRC_REF():

        def __init__(self, ref, beg, end, bam, confDict, outDir, src_id, offset = 1000):
            '''
            Source region class
            '''
            self.ref = ref
            self.beg = beg
            self.end = end
            self.bam = bam
            self.confDict = confDict
            self.outDir = outDir
            self.src_id = src_id
            
            self.offset = offset
            self.beg_ext = self.beg - offset
            self.end_ext = self.end + offset

            self.nb_hp1 = None
            self.nb_hp2 = None
            self.nb_hpX = None

            self.fa_hp1 = None
            self.fa_hp2 = None
            self.fa_hpX = None

            self.src_hp1 = None
            self.src_hp2 = None
            self.src_hpX = None

        def collect_reads(self):
            '''
            Get read seqs that cover beg or end of a src region
            '''
            ## Open BAM file for reading
            bamFile = pysam.AlignmentFile(self.bam, "rb")
            iterator = bamFile.fetch(self.ref, self.beg, self.end)

            ## Initialize FASTA object
            faSeqs_hp1 = formats.FASTA()
            faSeqs_hp2 = formats.FASTA()
            faSeqs_hpX = formats.FASTA()
            outFasta = self.outDir + '/reads_'

            ## For each read alignment
            for alignmentObj in iterator:

                ### 1. Filter out alignments based on different criteria
                #################################################################
                # MAPQ
                MAPQ = int(alignmentObj.mapping_quality) # Mapping quality
                if MAPQ <= self.confDict['MAPQ']:
                    continue

                # Unmapped reads or secondary alignments
                if (alignmentObj.is_unmapped) or (alignmentObj.is_secondary):
                    continue

                # No query sequence available
                if alignmentObj.query_sequence == None:
                    continue

                # SMS reads
                firstOperation, firstOperationLen = alignmentObj.cigartuples[0]
                lastOperation, lastOperationLen = alignmentObj.cigartuples[-1]
                maxCLIPPING = 1000
                if (firstOperationLen > maxCLIPPING) and (lastOperationLen > maxCLIPPING):
                    
                    if (firstOperation in (4, 5)) and (lastOperation in (4, 5)):
                        continue
                    
                # Internal DEL on src region
                positions = alignmentObj.get_reference_positions()
                positions_inSrc = list(set([bp for bp in positions if (self.beg < bp < self.end)]))

                if len(positions_inSrc) < 1000:
                    continue

                ### 2. Get read seqs that cover beg or end of the src region
                #################################################################
                if ((alignmentObj.reference_start < self.beg - self.offset) and (alignmentObj.reference_end > self.beg + self.offset)) or ((alignmentObj.reference_start < self.end - self.offset) and (alignmentObj.reference_end > self.end + self.offset)):

                    ## get query pos on src region
                    begPos = min(positions, key=lambda x:abs(x-self.beg_ext))
                    endPos = min(positions, key=lambda x:abs(x-self.end_ext))
                    idx = positions.index(begPos)
                    idx2 = positions.index(endPos)

                    ## get hp tag
                    try:
                        # if male and sexual chromosome
                        if (self.confDict['sex'] == 'XY') and (self.ref in ['chrX', 'chrY']):
                            hpRead = 'X'
                        else:
                            hpRead = alignmentObj.get_tag('HP')
                    except:
                        hpRead = 'X'

                    ## substring query_sequence matching src. save it to fasta 
                    if hpRead == 1:
                        faSeqs_hp1.seqDict[alignmentObj.query_name] = alignmentObj.query_sequence[idx:idx2]

                    elif hpRead == 2:
                        faSeqs_hp2.seqDict[alignmentObj.query_name] = alignmentObj.query_sequence[idx:idx2]

                    elif hpRead == 'X':
                        faSeqs_hpX.seqDict[alignmentObj.query_name] = alignmentObj.query_sequence[idx:idx2]

            ## Update attrib about the number of reads per haplotype covering the src element
            self.nb_hp1 = len(faSeqs_hp1.seqDict.values())
            self.nb_hp2 = len(faSeqs_hp2.seqDict.values())
            self.nb_hpX = len(faSeqs_hpX.seqDict.values())

            ## Write all seqs to FASTA files
            if len(faSeqs_hp1.seqDict.values()) > 4:
                faSeqs_hp1.write(outFasta + 'hp1.fa')
                self.fa_hp1 = outFasta + 'hp1.fa'

            if len(faSeqs_hp2.seqDict.values()) > 4:
                faSeqs_hp2.write(outFasta + 'hp2.fa')
                self.fa_hp2 = outFasta + 'hp2.fa'

            if len(faSeqs_hpX.seqDict.values()) > 4:
                faSeqs_hpX.write(outFasta + 'hpX.fa')
                self.fa_hpX = outFasta + 'hpX.fa'

        def create_consensus(self):
            '''
            Obtain a high quality src sequence 
            '''
            targetInterval = self.ref + ":" + str(self.beg_ext) + "-" + str(self.end_ext)
            binSeqs = []

            if self.fa_hp1:
                outDir_hp1 = self.outDir + '/hp1'
                unix.mkdir(outDir_hp1)
                consensusFasta_hp1 = self.polish(targetInterval, self.fa_hp1, outDir_hp1)
                srcSeq = self.get_src_seq(consensusFasta_hp1)

                if srcSeq:
                    self.src_hp1 = srcSeq
                    binId = self.src_id + "_hp1"
                    binSeqs.append([binId, srcSeq])

            if self.fa_hp2:
                outDir_hp2 = self.outDir + '/hp2'
                unix.mkdir(outDir_hp2)
                consensusFasta_hp2 = self.polish(targetInterval, self.fa_hp2, outDir_hp2)
                srcSeq = self.get_src_seq(consensusFasta_hp2)

                if srcSeq:
                    self.src_hp2 = srcSeq
                    binId = self.src_id + "_hp2"
                    binSeqs.append([binId, srcSeq])

            if self.fa_hpX:
                outDir_hpX = self.outDir + '/hpX'
                unix.mkdir(outDir_hpX)
                consensusFasta_hpX = self.polish(targetInterval, self.fa_hpX, outDir_hpX)
                srcSeq = self.get_src_seq(consensusFasta_hpX)

                if srcSeq:
                    self.src_hpX = srcSeq
                    binId = self.src_id + "_hpX"
                    binSeqs.append([binId, srcSeq])

            return(binSeqs)

        def polish(self, targetInterval, supportingReadsFasta, outDir):
            '''
            Reconstruct src sequence using local assembly + read polishing
            '''
            ## A. Run wtbgs2 to obtain a consensus
            unpolishedFasta = assembly.cons_wtdbg2(supportingReadsFasta, outDir, 1, 'ont')

            ## If wtbgs2 fails or assembly too short, use reference seq as template to be polished
            if unpolishedFasta:

                # A) unpolishedFasta is empty
                if (os.stat(unpolishedFasta).st_size == 0):
                    unpolishedFasta = None
                
                else:
                    # read fasta a check whether there is ctg1 and it is not too short
                    FASTA = formats.FASTA()
                    FASTA.read(unpolishedFasta)
                    if 'ctg1' in FASTA.seqDict:
                        assemblyLen = len(FASTA.seqDict['ctg1'])
                        srcLen = self.end - self.beg

                        # B) assembly too short
                        if assemblyLen < srcLen:
                            unpolishedFasta = None

                    # C) assembly does not contain ctg1 (main ctg)
                    else:
                        unpolishedFasta = None

            if unpolishedFasta is None:

                print("Using template from reference genome...")
                print(outDir)

                ## 0. Create logs directory
                logDir = outDir + '/Logs'
                unix.mkdir(logDir)

                ## 1. Write raw consensus sequence
                unpolishedFasta = outDir + '/raw_consensus.fa'
                err = open(logDir + '/target.err', 'w') 
                command = 'samtools faidx ' + reference + ' ' + targetInterval + ' > ' + unpolishedFasta
                status = subprocess.call(command, stderr=err, shell=True)

                if status != 0:
                    step = 'TARGET'
                    msg = 'Extraction of reference target region failed' 
                    log.step(step, msg)
                    return None

            ## B. Consensus polishing using Racon
            polishedFasta = assembly.polish_racon(unpolishedFasta, supportingReadsFasta, 'NANOPORE', 3, outDir)

            return polishedFasta
                
        def get_src_seq(self, consensusSeq):
            '''
            Extract source sequence
            '''
            ## 0. Create logs directory
            logDir = self.outDir + '/Logs'
            unix.mkdir(logDir)

            ## 1. Align the sequences into the target region 
            err = open(logDir + '/align.err', 'w')
            SAM = self.outDir + '/alignments.sam' 
            targetRef = self.confDict['refDir'] + '/reference_sequences_L1HS.fa'
            command = 'minimap2 -Y -a ' + targetRef + ' ' + consensusSeq + ' > ' + SAM
            status = subprocess.call(command, stderr=err, shell=True)

            if status != 0:
                step = 'ALIGN'
                msg = 'Local alignment failed' 
                log.step(step, msg)
                return None

            ## 2. Extract L1 seq
            ## Open BAM file for reading
            bamFile = pysam.AlignmentFile(SAM, "rb")
            iterator = bamFile.fetch()
            
            for alignmentObj in iterator:

                if alignmentObj.is_supplementary or alignmentObj.is_secondary:
                    continue

                if alignmentObj.mapping_quality < 30:
                    continue
                
                if alignmentObj.query_alignment_length < 5000:
                    continue
                
                idx = alignmentObj.query_alignment_start
                idx2 = alignmentObj.query_alignment_end

                return alignmentObj.query_sequence[idx:idx2]

            return None

        def call(self):
            '''
            Do the magic
            '''
            self.collect_reads()
            
            binSeqs = self.create_consensus()

            return binSeqs


    ## 3. Execute method for each bin
    #####################################
    ## 0. Read bin in input bed
    BED = formats.BED()
    BED.read(targetBins, 'List', None)        
    bins = [ [line.ref, line.beg, line.end, line.optional['strand'], line.optional['src_id'], line.optional['nb_fill'], line.optional['nb_empty'], line.optional['genotype']] for line in BED.lines ]

    ## 1. Open output file 
    outFilePath = outDir + '/src_seqs_polished.tsv'
    outFile = open(outFilePath, 'w')

    ## 2. Write header 
    row = "#ref\tstart\tend\tstrand\tsrc_id\tnb_fill\tnb_empty\tgenotype\tnb_hp1\tseq_hp1\tnb_hp2\tseq_hp2\tnb_hpX\tseq_hpX\n"
    outFile.write(row)
    outFa = formats.FASTA()

    # for each bin (src element)
    for bin in bins:

        # define args
        ref = bin[0]
        beg = bin[1]
        end = bin[2]
        strand = bin[3]
        src_id = bin[4]

        # create binDir
        binDir = outDir + '/' + ref + '_' + str(beg) + '_' + str(end)
        unix.mkdir(binDir)

        # do the magic
        src_ref = SRC_REF(ref, beg, end, bam, confDict, binDir, src_id)
        binSeqs = src_ref.call()

        # write src seq into output file
        row = "\t".join([str(src_ref.nb_hp1), str(src_ref.src_hp1), str(src_ref.nb_hp2), str(src_ref.src_hp2), str(src_ref.nb_hpX), str(src_ref.src_hpX)])
        outFile.write("\t".join([str(i) for i in bin]) + "\t" + row + "\n")

        # if result
        if binSeqs:
            
            ## if seq reconstructed for hp1, hp2 and hpX, discard hpX
            if len(binSeqs) == 3:
                binSeqs = [seq for seq in binSeqs if 'hpX' not in seq[0]]
            
            for binSeq in binSeqs:
                outFa.seqDict[binSeq[0]] = binSeq[1]

        # clean binDir
        unix.rm([binDir])
    
    # write output to fasta file
    outFa.write(outDir + '/src_seqs_polished.fa')
    outFile.close()

    print('***** Finished! *****')
    print()
