#!/usr/bin/env python
#coding: utf-8

'''
Module to filter out transductions as false translocations in Sniffles calls 
'''

if __name__ == '__main__':

    # External
    import argparse

    # GAPI
    from GAPI import bamtools
    from GAPI import databases
    from GAPI import formats
    from GAPI import unix
        
    ######################
    ## Get user's input ##
    ######################

    ## 1. Define parser ##
    ### Mandatory arguments
    parser = argparse.ArgumentParser(description='Filter Sniffles calls')
    parser.add_argument('bam', help='Input bam file')
    parser.add_argument('calls', help='Sniflles calls on bedpe format')
    parser.add_argument('outDir', help='Output directory')
    parser.add_argument('annotationsDir', help='MEIGA annotations directory')

    ## 2. Parse user´s input and initialize variables ##
    args = parser.parse_args()

    ### Mandatory arguments
    bam = args.bam
    outDir = args.outDir
    bed2filter = args.calls
    annotationsDir = args.annotationsDir

    outPath = outDir + '/sniffles.somatic.BND.filtered.bedpe'
    sourceBed = annotationsDir + '/srcElements.bed'
    threads = 1

    ## Create bed file containing transduced regions
    refLengths = bamtools.get_ref_lengths(bam)
    transducedPath = databases.create_transduced_bed(sourceBed, ['3'], 10000, -50, outDir)
    tdsAnnot = formats.bed2binDb(transducedPath, refLengths, threads)
    srcAnnot = formats.bed2binDb(sourceBed, refLengths, threads)

    ###############
    ## Functions ##
    ###############

    def filterBED(bed2filter):
        '''
        '''
        bedFile = open(bed2filter)

        ## Write entries into output bed file
        with open(outPath, 'w') as outFile:

            # For line in the file
            for line in bedFile:

                # Skip comments and blank lines
                if line.startswith('#') or not line:
                    continue

                fields = line.split()
                ref1 = fields[0]
                beg1 = int(fields[1])
                end1 = int(fields[2])
                ref2 = fields[3]
                beg2 = int(fields[4])
                end2 = int(fields[5])

                # 1. Filter entries on td regions
                if filter_metacluster_tdRegion(ref1, beg1, end1, tdsAnnot, buffer = 50):

                    if filter_metacluster_tdRegion(ref2, beg2, end2, tdsAnnot, buffer = 50):

                        # 2. Filter entries on source elements               
                        if filter_metacluster_tdRegion(ref1, beg1, end1, srcAnnot, buffer = 50):

                            if filter_metacluster_tdRegion(ref2, beg2, end2, srcAnnot, buffer = 50):

                                row = "\t".join(fields)
                                outFile.write(row + '\n')

        unix.rm([transducedPath])

    def filter_metacluster_tdRegion(ref, beg, end, tdAnnot, buffer = 50):
        '''
        '''
        PASS = True

        ## Do not filter out metacluster if no input range on that particular reference 
        if ref not in tdAnnot:
            return PASS
        
        ## Select transduction bin database for the corresponding ref            
        tdBinDb = tdAnnot[ref]

        ## Intersect metacluster interval with transducted regions  
        tdMatches = tdBinDb.collect_interval(beg-buffer, end+buffer, 'ALL')

        if tdMatches: PASS = False

        return PASS

    ## RUN PROGRAM
    filterBED(bed2filter)
