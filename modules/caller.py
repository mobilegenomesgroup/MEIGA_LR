'''
Module 'callers' - Contains classes and functions for calling variants from next generation sequencing data
'''

## DEPENDENCIES ##
# External
import multiprocessing as mp
import os
import time

# Internal
from GAPI import log
from GAPI import unix
from GAPI import bamtools
from GAPI import structures
from GAPI import clusters
from GAPI import output
from GAPI import annotation
from GAPI import filters
from GAPI import formats
from GAPI import bnds


## FUNCTIONS ##

## CLASSES ##
class SV_caller():
    '''
    Structural variation (SV) caller 
    '''
    def __init__(self, mode, bam, normalBam, reference, refDir, confDict, outDir):

        self.mode = mode
        self.bam = bam
        self.normalBam = normalBam
        self.reference = reference
        self.refDir = refDir
        self.confDict = confDict
        self.outDir = outDir
        self.repeatsBinDb = None

        ## Compute reference lengths
        self.refLengths = bamtools.get_ref_lengths(self.bam)

    def minimap2_index(self):
        '''
        Return path to minimap2 index file
        '''
        index = os.path.splitext(self.reference)[0] + '.mmi' 

        return index

    def load_annotations(self):
        '''
        Load set of annotations into bin databases. Set 'annotation' attribute with one key per annotation type
        and bin databases as values
        '''
        if self.confDict['technology'] != 'ILLUMINA':

            annotDir = self.outDir + '/LOAD_ANNOT/'
            unix.mkdir(annotDir)
            annotations2load = ['REPEATS', 'RETROTRANSPOSONS', 'TRANSDUCTIONS']
    
            if True: # at one point include flag for pseudogene search
                annotations2load.append('EXONS')
    
            if self.confDict['germlineMEI'] is not None:
                annotations2load.append('GERMLINE-MEI')
    
            self.annotations = annotation.load_annotations(annotations2load, self.refLengths, self.refDir, self.confDict['germlineMEI'], self.confDict['transductionSearch'], self.confDict['processes'], annotDir, self.reference)        
            
            if self.confDict['noCleanUp']: unix.rm([annotDir])


class MEI_caller(SV_caller):
    '''
    Mobile Element Insertion (MEI) caller for long read sequencing data
    '''
    def __init__(self, mode, bam, normalBam, reference, refDir, confDict, outDir):

        SV_caller.__init__(self, mode, bam, normalBam, reference, refDir, confDict, outDir)

    def call(self, transducedSomatic = None):
        '''
        Search MEI genome wide or in a set of target genomic regions
        '''

        ### 1. Create SV clusters 
        #############################
        msg = '1. Create SV clusters'
        log.header(msg)
        allMetaclusters = self.make_clusters()
        
        ### 2. Annotate SV clusters intervals 
        #########################################
        msg = '2. Annotate SV clusters intervals'
        log.header(msg)
        allMetaclusters = self.annotate_clusters(allMetaclusters)

        ### 3. Characterize INS metaclusters
        ########################################
        msg = '3. Characterize INS metaclusters'
        log.header(msg)
        if 'INS' in allMetaclusters:

            ### 3.1. Determine what type of sequence has been inserted for INS metaclusters ###
            ###################################################################################
            msg = '3.1. Determine what type of sequence has been inserted for INS metaclusters'
            log.header(msg)

            # Create output directory
            outDir = self.outDir + '/INS_TYPE/'
            unix.mkdir(outDir)

            # Infer insertion type
            clusters.INS_type_metaclusters(allMetaclusters['INS'], self.reference, self.annotations, self.confDict['processes'], outDir)
            
            # Remove output directory
            if self.confDict['noCleanUp']: unix.rm([outDir])
            
            ### 3.2. Resolve structure for solo, partnered and orphan transductions ###
            ###########################################################################
            msg = '3.2. Resolve structure for solo, partnered and orphan transductions'
            log.header(msg)
            
            # Load somatic source elements annotations
            consensus = self.refDir + '/consensusDb.fa'
            transduced = self.refDir + '/transducedDb.src_masked.trimmed.fa'
            transducedSomatic =  self.outDir + '/transduced_somatic.fa' if self.confDict['transductionSearch'] else None

            # Create output directory
            outDir = self.outDir + '/STRUCTURE/'
            unix.mkdir(outDir)

            # Structure inference
            allMetaclusters['INS'] = clusters.structure_inference_parallel(allMetaclusters['INS'], consensus, transduced, transducedSomatic, self.confDict['processes'], outDir)

            # Remove output directory
            if self.confDict['noCleanUp']: unix.rm([outDir])
        
            # if any insertion detected
            if ('INS' in allMetaclusters) and (allMetaclusters['INS']):
                
                ### 3.3. Assess MEI novelty ###
                ###############################
                msg = '3.3. Assess MEI novelty'
                log.header(msg)
                if self.confDict['germlineMEI'] is not None:
                    steps = ['GERMLINE-MEI']
                    annotation.annotate(allMetaclusters['INS'], steps, self.annotations, self.confDict['annovarDir'], None)

                ### 3.4. Apply second round of filtering ###
                ############################################
                msg = '3.4. Apply second round of filtering'
                log.header(msg)
                filters2Apply = ['ITYPE', 'PERC-RESOLVED', 'LOW-COMPLEXITY', 'EXPANDED_REPEAT', 'MEI_LEN', 'SHORT_CLIP']
                metaclustersPass, metaclustersFailed = filters.filter_metaclusters_parallel(allMetaclusters['INS'], filters2Apply, self.confDict, self.bam, self.normalBam, self.outDir)

                ## Extra filtering for somatic calls
                # filters2Apply = ['AREAMAPQ']
                germlinePass = []
                somaticPass = []
                if self.normalBam:

                    # Split PASS by mutation origin
                    for i in metaclustersPass:
                        if i.mutOrigin == 'germline':
                            germlinePass.append(i)
                        else:
                            somaticPass.append(i)

                    filters2Apply = ['GENOTYPE']
                    somaticPass, somaticFailed = filters.filter_metaclusters_parallel(somaticPass, filters2Apply, self.confDict, self.bam, self.normalBam, self.outDir)
                    for metacluster in somaticFailed: metacluster.failedFilters = [] # clean filters
                    germlinePass += somaticFailed

                    filters2Apply = ['SVA_ANNOT']
                    somaticPass, somaticFailed = filters.filter_metaclusters_parallel(somaticPass, filters2Apply, self.confDict, self.bam, self.normalBam, self.outDir)
                    metaclustersFailed += somaticFailed

                ### 3.5. Report SV calls into output files ###
                ##############################################
                msg = '3.5. Report SV calls into output files'
                log.header(msg)
                
                if somaticPass:
                    outFileName = 'INS_MEIGA.somatic.PASS'
                    output.INS2VCF(somaticPass, self.minimap2_index(), self.refLengths, self.confDict['source'], self.confDict['build'], self.confDict['species'], outFileName, self.outDir, self.confDict['extraFields'])
                    del somaticPass

                if germlinePass:
                    outFileName = 'INS_MEIGA.germline.PASS'
                    output.INS2VCF(germlinePass, self.minimap2_index(), self.refLengths, self.confDict['source'], self.confDict['build'], self.confDict['species'], outFileName, self.outDir, self.confDict['extraFields'])
                    del germlinePass

                if not self.normalBam and metaclustersPass:
                    outFileName = 'INS_MEIGA.PASS'
                    output.INS2VCF(metaclustersPass, self.minimap2_index(), self.refLengths, self.confDict['source'], self.confDict['build'], self.confDict['species'], outFileName, self.outDir, self.confDict['extraFields'])
                    del metaclustersPass
                    
                if metaclustersFailed:
                    # A. write them to output
                    outFileName = 'INS_MEIGA.FAILED'
                    output.INS2VCF(metaclustersFailed, self.minimap2_index(), self.refLengths, self.confDict['source'], self.confDict['build'], self.confDict['species'], outFileName, self.outDir, self.confDict['extraFields'])

                    # B. pass metaclustersFailed to BND analysis
                    if 'BND' in self.confDict['targetSV']:
                        metaclustersBNDs = bnds.failedINS2BND_metaclusters(metaclustersFailed, self.bam, self.confDict)
                        del metaclustersFailed
                        
                        if 'BND' in metaclustersBNDs:

                            if 'BND' in allMetaclusters:
                                allMetaclusters['BND'] = allMetaclusters['BND'] + metaclustersBNDs['BND']

                            else:
                                allMetaclusters['BND'] =  metaclustersBNDs['BND']

        ### 4. Identify BND junctions
        #################################
        msg = '4. Identify BND junctions'
        log.header(msg)
        if 'BND' in allMetaclusters:

            # Create output directory
            bndDir = self.outDir + '/BND_JUNCTIONS/'
            unix.mkdir(bndDir)

            # Create mobile elements seqs fasta
            transducedSomatic =  self.outDir + '/transduced_somatic.fa' if self.confDict['transductionSearch'] else None
            meFasta = bnds.get_me_fa(self.refDir, transducedSomatic) if transducedSomatic else self.refDir + '/reference_sequences_masked.fa'

            # TODO multiprocess
            ### Filter germline clusters if paired mode
            if self.mode == "SINGLE":
                allMetaclusters['BND'] = [metacluster for metacluster in allMetaclusters['BND'] if (metacluster.supportingReads()[0] >= self.confDict['minReads'])]
                
            else:
                minNormalReads = 1
                allMetaclusters['BND'] = [metacluster for metacluster in allMetaclusters['BND'] if (metacluster.supportingReads()[2] <= minNormalReads) and (metacluster.supportingReads()[0] >= self.confDict['minReads'])]

            ############################################################

            # TODO multiprocess
            fastaClipBnds = bnds.get_bnds_clip_fa(bndDir, allMetaclusters['BND'])
            
            metaclusters, noAnchorMetaclusters, fastaAnchors = bnds.search_bridges(allMetaclusters['BND'], fastaClipBnds, meFasta, self.confDict['processes'], bndDir)
            
            ## create junctions of DEL metaaclusters
            delJunctions = []
            filteredMetaclusters = []

            for metacluster in metaclusters:
                
                newJunction = bnds.del2clipping(metacluster, self.reference, self.confDict['processes'], self.confDict, self.bam, self.outDir)

                if newJunction:
                    delJunctions.append(newJunction)
                
                else:
                    filteredMetaclusters.append(metacluster)

            ## Create junction from other metaclusters
            allJunctions = bnds.search_junctions(filteredMetaclusters, noAnchorMetaclusters, fastaAnchors, self.reference, self.confDict['processes'], bndDir, self.confDict['targetRefs'])
                        
            allJunctions += delJunctions # add DEL jucntions

            ### Report BND junctions
            if allJunctions:
                
                bnds.polish_junctions_v2(allJunctions, self.confDict, meFasta, self.reference, self.confDict['processes'], bndDir)
                #bnds.polish_junctions(allJunctions, self.confDict, meFasta, self.reference, self.confDict['processes'], bndDir)
                
                bnds.junctionType(allJunctions)

                # ## Filtering 
                filters2Apply = ['LOW-COMPLEXITY', 'EXPANDED_REPEAT', 'ORPHAN_NO_BRIGE', 'NORMAL_DEPTH']
                processes = 4
                allJunctions, junctionsFail = filters.filter_junctions_parallel(allJunctions, filters2Apply, self.confDict, self.normalBam, self.reference, self.outDir, processes)
                
                if allJunctions:

                    if self.confDict['annovarDir']: 
                        
                        # Create output directory
                        annotDir = self.outDir + '/ANNOT/'
                        unix.mkdir(annotDir)
                        bnds.gene_annotation(allJunctions, self.confDict['annovarDir'], annotDir)

                    output.write_junctions(allJunctions, self.outDir, self.confDict['extraFields'])

            # Remove output directory
            if self.confDict['noCleanUp']: unix.rm([bndDir])

        if transducedSomatic: 
            refSeq = self.outDir + '/reference_sequences_masked.fa'
            unix.rm([transducedSomatic, self.outDir + '/transduced_somatic.bed', refSeq, refSeq + '.amb', refSeq + '.ann', refSeq + '.bwt', refSeq + '.pac', refSeq + '.sa', self.outDir + '/reference_sequences_masked.mmi', self.outDir + '/index.err'])

    def make_clusters(self):
        '''
        Search for structural variant (SV) clusters 

        Output:
            1. metaclustersPass: dictionary containing one key per SV type and the list of metaclusters identified of each given SV type
        '''
        ### 1. Define genomic bins to search for SV ##
        bins = bamtools.binning(self.confDict['targetBins'], self.bam, self.confDict['binSize'], self.confDict['targetRefs'])
        
        ### 2. Search for SV clusters in each bin ##
        # Create output directory
        unix.mkdir(self.outDir + '/CLUSTER/')

        # Genomic bins will be distributed into X processes
        pool = mp.Pool(processes=self.confDict['processes'])
        metaclustersPass = pool.starmap(self.make_clusters_bin, bins)
        pool.close()
        pool.join()

        # Remove output directory
        if self.confDict['noCleanUp']: unix.rm([self.outDir + '/CLUSTER/'])

        ### 3. Collapse metaclusters in a single dict
        metaclustersPass = structures.merge_dictionaries(metaclustersPass)       
                
        return metaclustersPass

    def make_clusters_bin(self, ref, beg, end):
        '''
        Search for structural variant (SV) clusters in a genomic bin/window
        '''
        ## 0. Set bin id and create bin directory ##
        binId = '_'.join([str(ref), str(beg), str(end)])
        msg = 'SV calling in bin: ' + binId
        log.subHeader(msg)
        start = time.time()

        binDir = self.outDir + '/CLUSTER/' + binId
        unix.mkdir(binDir)

        ## 1. Search for SV candidate events in the bam file/s ##
        # a) Single sample mode
        if self.mode == "SINGLE":
            eventsDict = bamtools.collectSV(ref, beg, end, self.bam, self.confDict, None, hp = self.confDict['hpTag'])

        # b) Paired sample mode (tumour & matched normal)
        else:
            eventsDict = bamtools.collectSV_paired(ref, beg, end, self.bam, self.normalBam, self.confDict)

        SV_types = sorted(eventsDict.keys())
        counts = [str(len(eventsDict[SV_type])) for SV_type in SV_types]

        step = 'COLLECT'
        msg = 'Number of SV events in bin (' + ','.join(['binId'] + SV_types) + '): ' + '\t'.join([binId] + counts)
        log.step(step, msg)
        
        ## 2. Organize all the SV events into genomic bins prior clustering ##
        step = 'BINNING'
        msg = 'Organize all the SV events into genomic bins prior metaclustering'
        log.step(step, msg)

        ## Define bin database sizes
        binSizes = [100, 1000, 10000, 100000, 1000000]
        
        ## Create bins
        eventsBinDb = structures.create_bin_database_interval(ref, beg, end, eventsDict, binSizes)

        ## 3. Group events into clusters ##
        step = 'CLUSTERING'
        msg = 'Group events into clusters' 
        log.step(step, msg)
        clustersBinDb = clusters.create_clusters(eventsBinDb, self.confDict)
        
        msg = 'Number of created clusters: ' + str(clustersBinDb.nbEvents()[0])
        log.step(step, msg)

        ## 4. Polish clusters ##
        step = 'POLISH'
        msg = 'Polish SV clusters' 
        log.step(step, msg)
        clusters.polish_clusters(clustersBinDb, self.confDict['minClusterSize'], self.confDict['genotyper'])
        
        ## 5. Group events into metaclusters ##
        step = 'META-CLUSTERING'
        msg = 'Group events into metaclusters' 
        log.step(step, msg)

        buffer = 10 if self.confDict['genotyper'] else 50
        metaclusters = clusters.create_metaclusters(clustersBinDb, buffer)        
        msg = 'Number of created metaclusters: ' + str(len(metaclusters)) 
        log.step(step, msg)
        
        ## 7. Filter metaclusters ##
        step = 'FILTER 1'
        msg = 'Filter out metaclusters' 
        log.step(step, msg)
        
        filters2Apply = ['MIN-NBREADS', 'MAX-NBREADS']
        metaclustersDict = {}
        metaclustersDict['INS'] = metaclusters
        metaclustersPassDict = filters.filter_metaclusters_pass(metaclustersDict, filters2Apply, self.confDict, self.bam)
        metaclusters = metaclustersPassDict['INS'] if 'INS' in metaclustersPassDict else []
            
        ## 6. Infer structural variant type ##
        step = 'SV-TYPE'
        msg = 'Infer structural variant type' 
        log.step(step, msg)

        outDir = binDir + '/SV_TYPE/' 
        metaclustersSVType = clusters.SV_type_metaclusters(metaclusters, self.confDict['minINSlen'], self.confDict['technology'], outDir, self.confDict)
        
        # Do cleanup
        if self.confDict['noCleanUp']: unix.rm([outDir])

        ## 7. Filter metaclusters ##
        step = 'FILTER 2'
        msg = 'Filter out metaclusters' 
        log.step(step, msg)
        
        filters2Apply = ['CV', 'SV-TYPE']
        metaclustersSVType = filters.filter_metaclusters_pass(metaclustersSVType, filters2Apply, self.confDict, self.bam)

        ## 8. Generate consensus event for SV metaclusters ##
        step = 'CONSENSUS'
        msg = 'Generate consensus event for SV metaclusters' 
        log.step(step, msg)

        targetSV = ['INS']
        outDir = binDir + '/CONSENSUS/' 
        clusters.create_consensus(metaclustersSVType, self.confDict, self.reference, targetSV, outDir)       

        ## 9. Lighten up metaclusters ##
        clusters.lighten_up_metaclusters(metaclustersSVType, self.bam, self.confDict)

        # Do cleanup
        if self.confDict['noCleanUp']: unix.rm([outDir, binDir])
        
        ## Print time taken to process bin
        end = time.time()
        time_taken = end - start
        msg = 'SV calling in bin: ' + binId + ' finished in ' + str(time_taken) + ' - ' + ';'.join(['-'.join([k, str(len(v))]) for k,v in metaclustersSVType.items()]) 
        log.info(msg)

        return metaclustersSVType

    def annotate_clusters(self, allMetaclusters):
        
        # Load annotations
        self.load_annotations()

        # Create output directory
        annotDir = self.outDir + '/ANNOT/'
        unix.mkdir(annotDir)
        
        # Define annotation steps
        steps = ['REPEAT']
        if self.confDict['annovarDir'] is not None: steps.append('GENE')

        # For each cluster type
        for SV_type in allMetaclusters:
            
            # 1. For BNDs, filter those on src regions
            if ('TRANSDUCTIONS' in self.annotations) and (SV_type == 'BND'):

                # 1. Filter metaclusters on td regions
                allMetaclusters[SV_type] = [metacluster for metacluster in allMetaclusters[SV_type] if filters.filter_metacluster_tdRegion(metacluster, self.annotations['TRANSDUCTIONS'])]
                
                # 2. Filter metaclusters on source elements               
                sourceBed = self.refDir + '/srcElements.bed'
                srcAnnot = formats.bed2binDb(sourceBed, self.refLengths, self.confDict['processes'])
                allMetaclusters[SV_type] = [metacluster for metacluster in allMetaclusters[SV_type] if filters.filter_metacluster_tdRegion(metacluster, srcAnnot)]

            # 2. Annotate all metaclusters
            annotation.annotate(allMetaclusters[SV_type], steps, self.annotations, self.confDict['annovarDir'], annotDir)

        # Remove annotation directory
        if self.confDict['noCleanUp']: unix.rm([annotDir])

        return allMetaclusters
