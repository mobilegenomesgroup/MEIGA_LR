# MEIGA

`MEIGA` is a method for calling somatic retrotransposition in long-read sequencing data.

- [Overview](#overview)
- [System Requirements](#system-requirements)
- [Installation Guide](#installation-guide)
- [Documentation](#documentation)
- [License](#license)
- [Issues](https://gitlab.com/mobilegenomesgroup/MEIGA_LR/-/issues)



# Overview

MEIGA (Mobile Element Integration Genome Analyser) is a method for comprehensively characterizing the patterns of somatic retrotransposition in long-read sequencing data. MEIGA is capable of detecting not only insertions mediated by retrotranspons but also other types of rearreagements mediated by their activity. These include Deletions (DEL), duplications (DUP), inversions (INV), translocations (TRA) or other complex rearrangements mediated by the integration of L1 and other mobile elements. Retrotransposition events can be further classified based on the type of sequence that has been retrotransposed. These events can be categorized as follows:

+ Solo insertons, which are full or partial sequences of L1, Alu and SVA.
+ L1 partnered transductions, in which a L1 sequence along with a piece of sequence downstream are retrotransposed.
+ L1 orphan transductions, in which only the unique sequence downstream of an active L1 is retrotransposed without the cognate L1. 
+ Processed pseudogenes, which include events where cellular mRNA is retrotransposed using L1 machinery.
+ Solitary poly(A) tracts from severely truncated retrotranspositions.



# System Requirements

`MEIGA` can potentially be installed on any *Linux* cluster. The package has been tested on the following system:
+ Linux: Rocky Linux 8.4

The running time and memory usage of `MEIGA` greatly depend on the coverage of the input BAM files and the retrotransposition rate of the interrogated samples. However, for pairs of tumour-normal samples sequenced to 30x and analysed using 24 cores, the average processing time for MEIGA is 2 hours with a memory usage of 30 GB.



# Installation Guide

## Meet MEIGA dependencies

`MEIGA` relies on the following dependencies:

1. samtools (version 1.19), which can be downloaded from https://github.com/samtools.
2. bedtools (version 2.31.0), which can be downloaded from https://github.com/arq5x/bedtools2.
3. bwa (version 0.7.17), which can be downloaded from https://github.com/lh3/bwa.
4. minimap2 (version 2.24), which can be downloaded from https://github.com/lh3/minimap2.
5. wtdbg2 (version 2.5), which can be downloaded from https://github.com/ruanjue/wtdbg2.
6. racon (version 1.5.0), which can be downloaded from https://github.com/lbcb-sci/racon.
7. python (version 3.7.8+). Python packages included in file **requirements.txt** can be installed using a pip based environment as explained below.


## Download MEIGA
```bash
git clone https://gitlab.com/mobilegenomesgroup/MEIGA_LR.git
mv MEIGA_LR MEIGA
```

## Set up MEIGA environment
```bash
python -m venv MEIGA/.venv
source MEIGA/.venv/bin/activate
pip install --upgrade pip
pip install wheel
pip install -r MEIGA/requirements.txt --no-cache-dir
```

**Note:** Binaries need to be added to MEIGA/.venv/bin or $PATH. For minimap2 is important to include not only minimap2, but also paftools.js and k8 binaries with execution permission (https://github.com/lh3/minimap2/blob/master/misc/README.md#introduction).


## Download MEIGA databases

They can be downloaded from https://zenodo.org/records/12527513 (DOI: 10.5281/zenodo.12527513) and decompressed as follows:
```
# 1. download
wget https://zenodo.org/records/12527513/files/meiga_db.tar.gz

# 2. extract
tar -zxvf meiga_db.tar.gz
```

# Documentation

## Test installation
A test dataset has been included in MEIGA and can be found at [MEIGA Demo](https://gitlab.com/mobilegenomesgroup/MEIGA_LR/-/tree/master/demo). This comprises a pair of BAM files containing 23 different somatic retrotranspositions of various types, including 10 L1 solo insertions, 4 L1 orphan insertions, 3 L1 partnered insertions and 5 L1 solo deletions. To run the demo, you need to modify the following paths in the `launch_meiga.sh` file and update the Python environment path:

+ reference: Path to human reference genome (GRCh38)
+ databases: Path to MEIGA databases (GRCh38)
+ meiga: Path to MEIGA installation
+ output: Path to output directory

**Note:** BWA and Minimap2 indexes should be available in the same folder as the reference file. MEIGA can be adapted for other assemblies or species if repeat annotations are available.

The first time MEIGA is run, it builds a cache for its database, resulting in a longer initial runtime. For example, for the test dataset, the first run takes 8 minutes using 4 cores and 40 GB of memory. Subsequent runs under the same conditions take only 4 minutes.


## Run MEIGA
```bash
# A) Load environment
source /MEIGA/.venv/bin/activate
export PYTHONHASHSEED=0


# B) Set general input
threads=4 # number of threads
technology=ont # technology -> 'rs', 'sq', 'ccs', 'ont'
reference=/reference/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna
databases=/databases/meiga_db
meigaDir=/MEIGA


# C) Set sample specific input
bam=$meigaDir/demo/simulationRegions_tumour.bam
normalBam=$meigaDir/demo/simulationRegions_normal.bam
outDir=$meigaDir/demo/output
mkdir -p $outDir
refs='chr1,chr2,chr3,chr4,chr5,chr6,chr7,chr8,chr9,chr10,chr11,chr12,chr13,chr14,chr15,chr16,chr17,chr18,chr19,chr20,chr21,chr22,chrX,chrY'


# D) Launch MEIGA
time python $meigaDir/MEIGA-LR.py \
$bam \
$technology \
$reference \
$databases \
--normalBam $normalBam \
--refs $refs \
-p $threads \
-o $outDir \
1> $outDir/MEIGA.out \
2> $outDir/MEIGA.err

```

## Understand the output
`MEIGA` outputs are collected in two different files:
+ ME_INS/INS_MEIGA.somatic.PASS.vcf
+ ME_RG/RG_MEIGA.PASS.bedpe

The first file contains all somatic retrotransposon insertions detected in a standard VCF format. The second file reports all somatic rearrangements mediated by retrotransposition, including deletion-like, duplication-like, inversion-like, and translocation-like events. An example of these outputs can be found at [MEIGA Demo](https://gitlab.com/mobilegenomesgroup/MEIGA_LR/-/tree/master/demo). Select FILTER == 'PASS' to obtain a final callset.



# License

This project is covered under the **GNU GENERAL PUBLIC LICENSE**.
