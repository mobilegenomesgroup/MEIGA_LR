'''
Module 'filters' - Contains functions for filtering clusters
'''
## External
import pysam
import statistics
import intervals as I
from itertools import combinations
from collections import Counter
import multiprocessing as mp
import os
import numpy as np
import mappy



## Internal
from GAPI import gRanges
from GAPI import bamtools
from GAPI import retrotransposons
from GAPI import formats
from GAPI import alignment
from GAPI import unix


###############
## FUNCTIONS ##
###############

def filter_clipping(clipping, filters2Apply, confDict):
    '''
    Apply selected filters to a clipping cluster provided as input

    Input:
        1. clipping: clipping cluster object
        2. filters2Apply: list containing the filters to apply (only those filters that make sense with the cluster type will be applied)
        5. confDict:

    Output:
        1. failedFilters -> list containing those filters that the clipping cluster didn´t pass.
    '''        
    failedFilters = []

    ## 1. FILTER 1: Minimum number of reads per cluster
    if 'MIN-NBREADS' in filters2Apply: 
        if not filter_min_nb_reads(clipping, confDict['minNbCLIPPING'], confDict['minNormalReads']):
            failedFilters.append('MIN-NBREADS')
        
    ## 2. FILTER 2: Filter out those clusters with suppl outside target reference ##
    if 'SUPPL-REF' in filters2Apply: 
        if not filter_clipping_suppl_ref(clipping, confDict['targetRefs']):
            failedFilters.append('SUPPL-REF')

    ## 3. FILTER 3: Filter out those clusters whose supplementary alignments map over any source element downstream region 
    if 'SUPPL-SRC' in filters2Apply:
        if not filter_clipping_suppl_position(clipping, confDict['rangesDict'], 10000):
            failedFilters.append('SUPPL-SRC')
    
    ## 4. FILTER 4: Average mapping quality of supplementary alignments 
    if 'SUPPL-MAPQ' in filters2Apply:
        if not filter_suppl_MAPQ(clipping, 10):
            failedFilters.append('SUPPL-MAPQ')

    ## 5. FILTER 5: filter out clusters formed by tumour and normal reads. Discard germline variation
    if 'GERMLINE' in filters2Apply:
        if not filter_germline(clipping, confDict['minNormalReads']):
            failedFilters.append('GERMLINE')
    
    ## 6. FILTER 6: Filter out clusters based on cluster coordinates ##
    if 'CLUSTER-RANGE' in filters2Apply:
        if not filter_clusterRange_clipping(clipping):
            failedFilters.append('CLUSTER-RANGE')

    return failedFilters

def filter_metaclusters_parallel(metaclusters, filters2Apply, confDict, bam, normalBam, outDir):
    '''
    Function to apply filters to a set of metaclusters organized in a dictionary (It uses threading)

    Input:
        1. metaclusters: list of metaclusters
        2. filters2Apply: list containing the filters to apply (only those filters that make sense with the cluster type will be applied)
        3. confDict

    Output:
        1. metaclustersPassDict: Dictionary with same structure as the input one, containing those metaclusters that passed all the filters.
        2. metaclustersFailDict: Dictionary with same structure as the input one, containing those metaclusters that don't passed all the filters.
    '''

    ## 1. Initialize lists
    metaclustersPass = [] 
    metaclustersFail = []

    ## 2. Create tuple list for multiprocessing
    metaclusters = [(metacluster, filters2Apply, confDict, bam, normalBam, outDir) for metacluster in metaclusters]

    ## 3. Get filters info for each metacluster
    pool = mp.Pool(processes=confDict['processes'])
    metaclusters = pool.starmap(filter_metacluster, metaclusters)
    pool.close()
    pool.join()

    ## 4. Split metaclusters according to whether they have passed the filters
    for entry in metaclusters:

        metacluster, failedFilters = entry
        metacluster.failedFilters = failedFilters

        if failedFilters:
            metaclustersFail.append(metacluster)

        else:
            metaclustersPass.append(metacluster)

    return metaclustersPass, metaclustersFail 

def filter_metacluster(metacluster, filters2Apply, confDict, bam, normalBam, outDir):
    '''
    Apply selected filters to one metacluster.

    Input:
        1. metacluster: metacluster object
        2. filters2Apply: list containing the filters to apply (only those filters that make sense with the cluster type will be applied)
        3. confDict
        4. bam

    Output:
        1. (metacluster, failedFilters) -> tuple of metacluster object and a list containing those filters that the metacluster doesn't pass.
    '''        
    failedFilters = []

    ## 1. FILTER 1: Minimum number of reads per cluster
    if 'MIN-NBREADS' in filters2Apply: 
        if not filter_min_nb_reads(metacluster, confDict['minReads'], confDict['minNormalReads']):
            failedFilters.append('MIN-NBREADS')
            return (metacluster, failedFilters)

    ## 2. FILTER 2: Maximum number of reads per cluster
    if 'MAX-NBREADS' in filters2Apply: 
        if not filter_max_nb_reads(metacluster, confDict['maxClusterSize']):
            failedFilters.append('MAX-NBREADS')
            return (metacluster, failedFilters)

    ## 3. FILTER 3: Maximum Coefficient of Variance per cluster
    if ('CV' in filters2Apply) and ('INS' in metacluster.subclusters): 
        if not filter_max_cv(metacluster, confDict['maxClusterCV']):
            failedFilters.append('CV')
            return (metacluster, failedFilters)

    ## 4. FILTER 4: Whether a metacluster is germline
    if 'GERMLINE' in filters2Apply:
        if not filter_germline_metaclusters(metacluster, confDict['minNormalReads']):
            failedFilters.append('GERMLINE')
            return (metacluster, failedFilters)

    ## 5. FILTER 5. Filter based on mechanism
    if 'MECHANISM' in filters2Apply:
        if not filter_mechanism(metacluster):
            failedFilters.append('MECHANISM')
            return (metacluster, failedFilters)

    ## 6. FILTER 6: Whether a metacluster has a SV_type assigned or not
    if 'SV-TYPE' in filters2Apply: 
        if not filter_SV_type(metacluster, confDict['targetSV']):
            failedFilters.append('SV-TYPE')
            return (metacluster, failedFilters)
        
    ## 7. FILTER 7. Filter based on the insertion type
    if 'ITYPE' in filters2Apply:
        if not filter_itype(metacluster):
            failedFilters.append('ITYPE')
            return (metacluster, failedFilters)
        
    ## 8. FILTER 8: Minimum percentage of inserted sequence resolved
    if ('PERC-RESOLVED' in filters2Apply) and (metacluster.SV_type == 'INS') and ('PERC_RESOLVED' in metacluster.SV_features): 
        if not filter_perc_resolved(metacluster, confDict['minPercResolved']):
            failedFilters.append('PERC-RESOLVED')
            return (metacluster, failedFilters)
            
    ## 9. FILTER 9: Discard microsatellites
    if ('LOW-COMPLEXITY' in filters2Apply) and (metacluster.SV_type == 'INS'):
        if not filter_low_complexity(metacluster):
            failedFilters.append('LOW-COMPLEXITY')
            return (metacluster, failedFilters)
    
    ## 10. FILTER 10: Discard microsatellites
    if ('EXPANDED_REPEAT' in filters2Apply) and (metacluster.SV_type == 'INS'):
        if not filter_expanded_repeat(metacluster):
            failedFilters.append('EXPANDED_REPEAT')
            return (metacluster, failedFilters)
        
    ## 11. FILTER 11: SVA Annotation
    if 'SVA_ANNOT' in filters2Apply:
        if not filter_annot(metacluster):
            failedFilters.append('SVA_ANNOT')
            return (metacluster, failedFilters)

    ## 12. FILTER 12: MEI length
    if 'MEI_LEN' in filters2Apply:
        if not filter_len(metacluster):
            failedFilters.append('MEI_LEN')
            return (metacluster, failedFilters)

    ## 13. FILTER 13: Short mei w/o enough spanning reads
    if 'SHORT_CLIP' in filters2Apply:
        if not short_nbSpan(metacluster):
            failedFilters.append('SHORT_CLIP')
            return (metacluster, failedFilters)

    ## 14. FILTER 14: Genotype
    if 'GENOTYPE' in filters2Apply:
        if not is_somatic(metacluster, confDict, normalBam, outDir):
            failedFilters.append('GENOTYPE')
            return (metacluster, failedFilters)
           
    return (metacluster, failedFilters)

def filter_metaclusters_pass(metaclustersDict, filters2Apply, confDict, bam):
    '''
    Function to apply filters to a set of metaclusters organized in a dictionary

    Input:
        1. metaclustersDict: dictionary with the following structure: keys -> SV_type, value -> list of metaclusters corresponding to this SV_type.
        2. filters2Apply: list containing the filters to apply (only those filters that make sense with the cluster type will be applied)
        3. confDict

    Output:
        1. metaclustersPassDict: Dictionary with same structure as the input one, containing those metaclusters that passed all the filters.
    '''

    metaclustersPassDict = {}

    ## For each type of SV
    for SV_type, metaclusters in metaclustersDict.items():

        ## Collect metaclusters with no filter failed
        metaclustersPassDict[SV_type] = [metacluster for metacluster in metaclusters if filter_metacluster_pass(metacluster, filters2Apply, confDict, bam) == []]

    return metaclustersPassDict

def filter_metacluster_pass(metacluster, filters2Apply, confDict, bam):
    '''
    Apply selected filters to one metacluster.

    Input:
        1. metacluster: metacluster object
        2. filters2Apply: list containing the filters to apply (only those filters that make sense with the cluster type will be applied)
        3. confDict
        4. bam

    Output:
        1. failedFilters -> list containing those filters that the metacluster doesn't pass.
    '''  
    ## 1. FILTER 1: Minimum number of reads per cluster
    if 'MIN-NBREADS' in filters2Apply: 
        if not filter_min_nb_reads(metacluster, confDict['minReads'], confDict['minNormalReads']):
            return ['MIN-NBREADS']

    ## 2. FILTER 2: Maximum number of reads per cluster
    if 'MAX-NBREADS' in filters2Apply: 
        if not filter_max_nb_reads(metacluster, confDict['maxClusterSize']):
            return ['MAX-NBREADS']

    ## 3. FILTER 3: Maximum Coefficient of Variance per cluster
    if ('CV' in filters2Apply) and ('INS' in metacluster.subclusters): 
        if not filter_max_cv(metacluster, confDict['maxClusterCV']):
            return ['CV']

    ## 4. FILTER 4: Whether a metacluster has a SV_type assigned or not
    if 'SV-TYPE' in filters2Apply: 
        if not filter_SV_type(metacluster, confDict['targetSV']):
            return ['SV-TYPE']

    ## 5. FILTER 5: Minimum percentage of inserted sequence resolved
    if ('PERC-RESOLVED' in filters2Apply) and (metacluster.SV_type == 'INS') and ('PERC_RESOLVED' in metacluster.SV_features): 
        if not filter_perc_resolved(metacluster, confDict['minPercResolved']):
            return ['PERC-RESOLVED']
            
    ## 6. FILTER 6: Discard microsatellites
    if ('LOW-COMPLEXITY' in filters2Apply) and (metacluster.SV_type == 'INS'):
        if not filter_low_complexity(metacluster):
            return ['LOW-COMPLEXITY']
    
    ## 6. FILTER 6: Discard microsatellites
    if ('EXPANDED_REPEAT' in filters2Apply) and (metacluster.SV_type == 'INS'):
        if not filter_expanded_repeat(metacluster):
            return ['EXPANDED_REPEAT']
    
    ## 7. FILTER 7: Whether a metacluster is germline
    if 'GERMLINE' in filters2Apply:
        if not filter_germline_metaclusters(metacluster, confDict['minNormalReads']):
            return ['GERMLINE']

    ## 8. FILTER 8. Filter based on the insertion type
    if 'ITYPE' in filters2Apply:
        if not filter_itype(metacluster):
            return ['ITYPE']

    ## 9. FILTER 9. Filter based on mechanism
    if 'MECHANISM' in filters2Apply:
        if not filter_mechanism(metacluster):
            return ['MECHANISM']

    return []

def filter_mechanism(metacluster):
    '''
    Filter cluster by insertion mechanism (TPRT: Target primed reverse transcription and EI: Endonuclease independent candidate)

    Input:
        1. cluster: cluster object

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    if ('MECHANISM' in metacluster.SV_features) and (metacluster.SV_features['MECHANISM'] == 'TPRT'):
        PASS = True
    else:
        PASS = False
    return PASS

def filter_itype(metacluster):
    '''
    Filter cluster by insertion type

    Input:
        1. cluster: cluster object

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't    
    '''
    if metacluster.SV_features['INS_TYPE'] in ['solo', 'partnered', 'orphan', 'pseudogene', 'poly(A/T)']:
        PASS = True
    else:
        PASS = False
    return PASS

def filter_min_nb_reads(cluster, minReads, minNormalReads):
    '''
    Filter cluster by comparing the number of supporting events with a minimum treshold

    Input:
        1. cluster: cluster object
        2. minReads: min number of events threshold
        3. minReads: min number of events threshold for normal sample

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    if cluster.events:
        
        ## 1. Compute number of events supporting the cluster 
        nbTotal, nbTumour, nbNormal = cluster.supportingReads()[0:3]

        ## 2. Compare the number of events supporting the cluster against the minimum required
        # 2.1 Paired mode:
        if nbTumour != None:

            if nbTumour >= minReads and nbNormal >= minNormalReads:
                cluster.mutOrigin = 'germline'
                PASS = True

            elif nbTumour >= minReads and not nbNormal >= minNormalReads:
                cluster.mutOrigin = 'somatic'
                PASS = True

            elif nbNormal >= minReads:
                cluster.mutOrigin = 'germline'
                PASS = True

            else:
                PASS = False

        # 2.1 Single mode:
        else:
            # If running in single mode (do no set mutation origin because it doesn't make sense)
            if nbTotal >= minReads:
                PASS = True
            else:
                PASS = False
                
    else:
        PASS = False    
    
    return PASS

    
def filter_low_complexity(event, mei = True):
    
    # it is a MEI
    if mei:
        # if polyA insertion, skip filter
        if 'INS_TYPE' in event.SV_features and event.SV_features['INS_TYPE'] == 'poly(A/T)':
            return True
        
        # if inserted sequence not available, skip filter
        elif event.consensusEvent is None:
            return True

        else:
            # get inserted seq
            insSeq = event.consensusEvent.pick_insert()

    # it is a junction
    else:
        if event.bridge:
            insSeq = event.bridge.bridgeSeq
        else:
            return True

    # find polyA tracks
    perc_polyA, polyA_tracks, uniq_positions = retrotransposons.perc_polyA(insSeq, windowSize = 10)

    # if polyA > 40% inserted seq, skip filter 
    if perc_polyA > 0.4: return True
    
    # trim polyA tracks from sequence
    if polyA_tracks:
        
        complexSeq = ''
        r1 = I.closed(0, len(insSeq))
        r2 = [I.closed(polyA[0], polyA[1]) for polyA in polyA_tracks]
        
        for interval in r2:
            r1 = r1 - interval
        
        for i in r1:
            complexSeq += insSeq[i.lower:i.upper]
            
    else:
        complexSeq = insSeq
        
    for K in [1,2,3,4]:
        
        res = [complexSeq[x:y] for x, y in combinations(range(len(complexSeq) + 1), r = 2) if len(complexSeq[x:y]) == K ]
        cumulative_diff = sum([abs(value/len(res) - 0.25**K) for value in Counter(res).values()])

        if cumulative_diff > 0.83:
            return False
    
    return True

def filter_expanded_repeat(event, mei = True):
    
    PASS = True

    if mei:
        repeats = event.repeatAnnot if hasattr(event, 'repeatAnnot') else []
        insSeq = event.consensusEvent.pick_insert() if event.consensusEvent else None

    else:
        repeatsA = event.metaclusterA.repeatAnnot if hasattr(event.metaclusterA, 'repeatAnnot') else []
        repeatsB = event.metaclusterB.repeatAnnot if hasattr(event.metaclusterB, 'repeatAnnot') else []
        if repeatsA == None: repeatsA = []
        if repeatsB == None: repeatsB = []
        repeats =  repeatsA + repeatsB
        insSeq = event.bridge.bridgeSeq if event.bridge else None

    # if inserted sequence recovered and event on repeats
    if repeats and insSeq:
        
        for repeat in repeats:

            # if repeat is simple repeat but not a monomer '(A)n','(T)n'...
            if repeat['family'] == "Simple_repeat" and len(repeat['subfamily']) > 4:
                
                # extract repeat subunit
                rep_subunit = repeat['subfamily'][1:-2]
                
                # estimate ratio of repeat in inserted seq
                len_seq = len(insSeq)
                len_rep = len(rep_subunit)
                nb_rep = insSeq.count(rep_subunit)
                
                obs_fq = nb_rep / (len_seq - len_rep)
                exp_fq  = 0.25**len_rep

                if abs(obs_fq - exp_fq) > 0.2:
                    PASS = False
    
    return PASS

def filter_annot(mei):
    '''
    Filter mei if SVA that lands on SVA
    '''

    PASS = True

    if ('FAMILY' in mei.SV_features and mei.SV_features['FAMILY']):
        
        identity = set(mei.SV_features['FAMILY'])

        if set(identity).intersection({'SVA'}):

            repeats = mei.repeatAnnot if hasattr(mei, 'repeatAnnot') else []
            repeats = set([repeat['family'] for repeat in repeats]) if repeats else {}
        
            if repeats and set(identity).intersection(set(repeats)):

                PASS = False

    return PASS

def filter_len(mei):
    '''
    Filter mei by size
    '''

    PASS = True

    mei_len = mei.consensusEvent.length if mei.consensusEvent is not None else None

    if mei_len and mei.SV_features['INS_TYPE'] == 'solo':

        if ('FAMILY' in mei.SV_features):
            
            if 'L1' in mei.SV_features['FAMILY']:

                if mei_len > 7000:
                    PASS = False

            elif 'Alu' in mei.SV_features['FAMILY']:

                if mei_len > 500:
                    PASS = False

            elif 'SVA' in mei.SV_features['FAMILY']:

                if mei_len > 3000:
                    PASS = False
    
    return PASS

def ins_cluster_sdLen(ins_cluster, minClusterSize):
    '''
    Filter objects INS_cluster if events length is not similar 
    '''

    PASS = True

    lengths = [x.length for x in ins_cluster.events]
    ins_len = int(np.median(lengths))

    if ins_len:

        ratioLen = [x.length/ins_len for x in ins_cluster.events]
        
        if sum([(0.8 < i < 1.2) for i in ratioLen]) < minClusterSize:

            PASS = False

    return PASS

def short_nbSpan(mei):
    '''
    Filter short insertions if not supported by spanning reads
    '''

    PASS = True

    is_short = ((mei.consensusEvent is not None) and (mei.consensusEvent.length < 200))
    ins_type = mei.SV_features['INS_TYPE'] if 'INS_TYPE' in mei.SV_features else None

    if is_short or (ins_type == 'poly(A/T)'):
        
        if (mei.nbCLIPPING > 0) and (mei.nbINS > 0) and (mei.nbCLIPPING/mei.nbINS > 0.2):

            PASS = False

    return PASS

def is_somatic(cluster, confDict, normalBam, outDir):
    '''
    Double check there is no SVs on normal
    '''

    PASS = True
    offset = 25

    # Collect SV in region 
    confDict['minCLIPPINGlen'] = 50
    confDict['minMAPQ'] = 0
    confDict['minINSlen'] = 10
    eventsDict_N = bamtools.collectSV(cluster.ref, cluster.beg-offset, cluster.end+offset, normalBam, confDict, 'NORMAL')

    # Write consensus insertion seq to fasta
    FASTA = formats.FASTA()
    FASTA.seqDict['ins'] = cluster.consensusEvent.pick_insert()      
    fastaPath = outDir + '/' + str(cluster.id) + '.fa'
    FASTA.write(fastaPath) 
    
    # Set INS length
    insLens = [i.length for i in cluster.events if i.type == "INS"]
    insLen = int(np.median(insLens)) if insLens else cluster.consensusEvent.length
    ins_type = cluster.SV_features['INS_TYPE'] if 'INS_TYPE' in cluster.SV_features else None

    # Set aligner 
    a = mappy.Aligner(fn_idx_in=fastaPath, preset=None)

    # If INS in region
    if 'INS' in eventsDict_N:

        for i in eventsDict_N['INS']:

            if ins_type == 'poly(A/T)':

                # find polyA tracks
                perc_polyA = retrotransposons.perc_polyA(i.pick_insert(), windowSize = 10)[0]

                # if polyA > 40% inserted seq, skip filter 
                if perc_polyA > 0.4:

                    PASS = False
                    unix.rm([fastaPath]) # clean up

                    return PASS

            # If INS have similar size than consensus
            if (0.7 < i.length/insLen < 1.3):

                seq = i.pick_insert()

                # Align against consensus
                for hit in a.map(seq):
                    
                    # If match, fail filter --> Not somatic
                    if (hit.r_en - hit.r_st)/insLen > 0.7:

                        PASS = False
                        unix.rm([fastaPath]) # clean up

                        return PASS

    # # If INS in region
    # if 'CLIPPING' in eventsDict_N:

    #     if len(eventsDict_N['CLIPPING'] > 1):

    #         bkps = [i.bkp for i in eventsDict_N['CLIPPING']]

    # clean up
    unix.rm([fastaPath])
    
    return PASS

def filter_max_nb_reads(metacluster, maxNbEvents):
    '''
    Filter metacluster by comparing the number of cluster supporting events with a maximum treshold

    Input:
        1. metacluster: metacluster object
        2. maxNbEvents: maximum number of events threshold
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    ## 1. Compute number of events supporting the cluster 
    nbTotal = metacluster.nbEvents()[0]

    ## 2. Compare the number of events supporting the cluster against the maximum required
    if nbTotal <= maxNbEvents:
        PASS = True
    else:
        PASS = False
    
    return PASS

def filter_max_cv(metacluster, maxClusterCV):
    '''
    Filter metacluster by comparing its Coefficient of Variation with a maximum threshold.

    Input:
        1. metacluster: metacluster object
        2. maxClusterCV: maximum Coefficient of Variation threshold
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    ## 1. Compute CV of the cluster 
    cv = metacluster.subclusters['INS'].cv_len()[1]

    ## 2. Compare the cluster CV against the maximum required
    if cv <= maxClusterCV:
        PASS = True
    else:
        PASS = False

    return PASS

def filter_perc_resolved(metacluster, minPercResolved):
    '''
    Filter metacluster by comparing the % of inserted sequence resolved with a minimum threshold

    Input:
        1. metacluster: metacluster object
        2. minPercResolved: minimum % resolved

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    if (metacluster.SV_features['PERC_RESOLVED'] >= minPercResolved):
        PASS = True

    else:
        PASS = False

    return PASS

def filter_SV_type(metacluster, targetSV):
    '''
    Filter metacluster by checking its SV type

    Input:
        1. metacluster: metacluster object
        2. targetSV: list containing list of target SV types
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    if metacluster.SV_type in targetSV:
        PASS = True 
        
    else:
        PASS = False

    return PASS

def filter_suppl_MAPQ(clipping, minMAPQ):
    '''
    Filter out clipping cluster based on the average mapping quality of its supplementary alignments

    Input:
        1. clipping: clipping cluster object
        2. minMAPQ: minimum mapping quality

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't    
    '''

    ## Compute average mapping quality for supplementary alignments
    MAPQs = [event.mapQ for event in clipping.supplCluster.events]
    meanMAPQ = statistics.mean(MAPQs)

    ## Apply filter
    if meanMAPQ >= minMAPQ:
        PASS = True

    else:
        PASS = False
    
    return PASS

def filter_clipping_suppl_ref(clipping, targetRefs):
    '''
    Filter out clipping cluster if its supplementary cluster not located over a target reference

    Input:
        1. clipping: clipping cluster
        2. targetRefs: List of target references

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    ## Retrieve suppl. alignment reference 
    if clipping.supplCluster.ref in targetRefs:
        PASS = True 

    else:
        PASS = False

    return PASS

def filter_clipping_suppl_position(clipping, ranges, buffer):
    '''
    Filter out clipping cluster if suppl. alignment within one of the provided regions
    
    Input:
        1. clipping: clipping cluster object
        2. ranges: Dictionary with reference ids as keys and the list of ranges on each reference as values
        3. buffer: Extend each range at their begin and end coordinate by a number of nucleotides == buffer length

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    ## Do not filter out clipping cluster if no input range on that particular reference 
    if clipping.supplCluster.ref not in ranges:
        PASS = True
        return PASS
        
    ## Assess overlap between mates interval and provided regions. 
    PASS = True

    for interval in ranges[clipping.supplCluster.ref]:
        rangeBeg, rangeEnd = interval

        # Add buffer
        rangeBeg = rangeBeg - buffer
        rangeEnd = rangeEnd + buffer

        # Assess overlap
        overlap, overlapLen = gRanges.overlap(clipping.supplCluster.beg, clipping.supplCluster.end, rangeBeg, rangeEnd)

        if overlap:
            PASS = False

    return PASS

def filter_metacluster_tdRegion(metacluster, tdAnnot, buffer = 50):
    '''
    Filter out metacluster if within one of the provided TD regions
    
    Input:
        1. metacluster: metacluster cluster object
        2. tdAnnot: Dictionary with reference ids as keys and the list of ranges on each reference as values
        3. buffer: Extend each range at their begin and end coordinates by a number of nucleotides

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    PASS = True
                    
    ## Do not filter out metacluster if no input range on that particular reference 
    if metacluster.ref not in tdAnnot:
        return PASS

    ## Select transduction bin database for the corresponding ref            
    tdBinDb = tdAnnot[metacluster.ref]

    ## Intersect metacluster interval with transducted regions  
    tdMatches = tdBinDb.collect_interval(metacluster.beg-buffer, metacluster.end+buffer, 'ALL')

    if tdMatches: PASS = False

    return PASS

def filter_germline(cluster, minNormal):
    '''
    Filter out those clusters formed by tumour and normal reads
    
    Input:
        1. cluster: cluster object
        2. minNormal: minimum number of reads supporting a SV in normal sample
    
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    count = 0
        
    for event in cluster.events:
        
        if event.sample == 'NORMAL':
            count += 1
                
    if count < minNormal:
        PASS = True
        
    else:
        PASS = False
        
    return PASS

def filter_germline_metaclusters(metacluster, minNormal):
    '''
    Filter out those clusters formed by tumour and normal reads
    
    Input:
        1. cluster: cluster object
        2. minNormal: minimum number of reads supporting a SV in normal sample
    
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    
    PASS = True
    
    nbNormal = metacluster.supportingReads()[2]
    
    if nbNormal >= minNormal:
        PASS = False
        
    return PASS

def region_mapq(cluster, bam):
    '''
    Set region MAPQ and AS
    '''

    offset = 50

    # open BAM file for reading
    bamFile = pysam.AlignmentFile(bam, "rb")

    # extract alignments MAPQ
    iterator = bamFile.fetch(cluster.ref, cluster.beg-offset, cluster.end+offset)

    ### 1. MAPQ
    mapqs = [int(alignmentObj.mapping_quality) for alignmentObj in iterator]
    median = int(np.median(mapqs))

    ### 2. Chaining score
    chain_scores = []
    iterator = bamFile.fetch(cluster.ref, cluster.beg-offset, cluster.end+offset)
    for alignmentObj in iterator:

        try:
            chain_scores.append(alignmentObj.get_tag('s2')/alignmentObj.get_tag('s1'))

        except:
            pass

    delta_s = [(i < 0.5) for i in chain_scores]

    # set cluster attributes
    cluster.x_mapq = median
    cluster.x_s = round(sum(delta_s)/len(delta_s), 2)
    
def filter_clusterRange_clipping(cluster):
    '''
    Filter out those clipping clusters in which all clippings have the same 
    coordinates relative to the read. This filter is only applied to clusters 
    formed by more than a clipping alignment
    
    ----------****>                       ---------******>
             55   75                              40    75
       -------*******>                    ---------******>
              38    75                            40    75
     ---------*****>                      ---------******>
              61   75                             40    75
                          clusterCoord 
    [55, 75, 38, 75, 61, 75]        [40, 75, 40, 75, 40, 75]
          True              PASS                False      
    
    Input:
        1. cluster: cluster formed by CLIPPING events
    
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    
    PASS = True
       
    # if there is more than a clipping
    if len(cluster.events) > 1:
               
        # with clipping events beg and end coordinates are the same. However, readCoordinates()
        # returns the aligment coordinates relative to the read. If more than 2 coordinates, 
        # it is not a piled up cluster of clippings        
        clusterCoord = [event.readCoordinates() for event in cluster.events]
        clusterCoord_flatList = [item for sublist in clusterCoord for item in sublist]
        n_clusterCoord = len(set(clusterCoord_flatList))
              
        if(n_clusterCoord <= 2):
            PASS = False
        
    return PASS

def filter_junctions_parallel(junctions, filters2Apply, confDict, normalBam, reference, outDir, processes):
    '''
    Function to apply filters to a set of junctions organized in a dictionary (It uses threading)

    Input:
        1. junctions: list of junctions
        2. filters2Apply: list containing the filters to apply (only those filters that make sense with the cluster type will be applied)
        3. confDict

    Output:
        1. junctionsPass: List of junctions that passed all the filters.
        2. junctionsFail: List of junctions that don't passed all the filters.
    '''

    ## 1. Initialize lists
    junctionsPass = [] 
    junctionsFail = []

    ## 2. Create tuple list for multiprocessing
    junctions = [(junction, filters2Apply, confDict, normalBam, reference, outDir) for junction in junctions]

    ## 3. Get filters info for each junction
    pool = mp.Pool(processes=processes)
    junctions = pool.starmap(filter_junction, junctions)
    pool.close()
    pool.join()

    ## 4. Split junctions according to whether they have passed the filters
    for entry in junctions:

        junction, failedFilters = entry
        junction.failedFilters = failedFilters

        if failedFilters:
            junctionsFail.append(junction)

        else:
            junctionsPass.append(junction)

    return junctionsPass, junctionsFail 

def filter_junction(junction, filters2Apply, confDict, normalBam, reference, outDir):
    '''
    '''
    failedFilters = []
    
    # junction.svType == 'TRA'
            
    ## 6. FILTER 6: Discard microsatellites
    if junction.bridge:

        if ('LOW-COMPLEXITY' in filters2Apply) and (junction.bridge.bridgeType in ['orphan', 'partnered']):
            if not filter_low_complexity(junction, mei=False):
                failedFilters.append('LOW-COMPLEXITY')
                return (junction, failedFilters)
        
        ## 6. FILTER 6: Discard microsatellites
        if ('EXPANDED_REPEAT' in filters2Apply) and (junction.bridge.bridgeType in ['orphan', 'partnered']):
            if not filter_expanded_repeat(junction, mei=False):
                failedFilters.append('EXPANDED_REPEAT')
                return (junction, failedFilters)

    if (normalBam) and ('NORMAL_DEPTH' in filters2Apply):
        if not junction_normalDepth(junction, normalBam, confDict['minReads']):
            failedFilters.append('NORMAL_DEPTH')
            return (junction, failedFilters)
        
    if junction.bridge:

        if ('ORPHAN_NO_BRIGE' in filters2Apply) and (junction.bridge.bridgeType == 'orphan'):
            if not junction_noBridgeOrphan(junction, reference, 1, outDir):
                failedFilters.append('ORPHAN_NO_BRIGE')
                return (junction, failedFilters)

    return (junction, failedFilters)

def junction_normalDepth(junction, normalBam, minReads, minMAPQ = 20):
    '''
    Filter to discard DEL that already appear in the matched normal or
    events that happen where there is no reads on the normal
    '''

    PASS = True
    offset = 5 # bp

    for cluster in [junction.metaclusterA, junction.metaclusterB]:

        if cluster.strand == "+":
            ref = cluster.ref
            bkp = cluster.bkpPos + offset

        else:
            ref = cluster.ref
            bkp = cluster.bkpPos - offset

        # open normal bam file
        bamFile = pysam.AlignmentFile(normalBam, "rb")

        # count number of DEL in the middle of reported deletion
        count_del=0
        count_depth=0 

        for pileupcolumn in bamFile.pileup(ref, bkp, bkp+1):

            if pileupcolumn.pos == bkp:

                for pileupread in pileupcolumn.pileups:

                    if pileupread.alignment.mapping_quality >= minMAPQ:
                        count_depth += 1

                    if pileupread.is_del:
                        count_del += 1

        # if no enough coverage with MAPQ>20 on normal bam
        if count_depth < minReads:
            PASS = False
            return (PASS)

        # if DEL
        elif junction.svType == 'DEL': 

            # if more than 30% of reads in normal, discard --> not somatic
            if (count_del > 0) and count_del/count_depth > 0.3:
                PASS = False
                return (PASS)
        
    return (PASS)

def junction_noBridgeOrphan(junction, reference, processes, outDir):
    '''
    Filter to discard false orphan bridges that indeed align next to one of the junction bkps
    '''
    PASS = True
    
    # if otphan with no polyA
    if (junction.bridge.bridgeSeq) and (junction.bridge.bridgeType == 'orphan') and (junction.bridge.polyA is False):
        
        # create junction specific outDir
        junctionDir = outDir + '/' + junction.id
        unix.mkdir(junctionDir)

        # bridge seq to fasta
        bridgeFasta = formats.FASTA()
        bridgeFasta.seqDict = {'bridge': junction.bridge.bridgeSeq}
        fastaPath = junctionDir + '/supportingReads_altSeq.fa'
        bridgeFasta.write(fastaPath)
        
        # align bridge to genome
        index = os.path.splitext(reference)[0] + '.mmi'
        PAF_genome = alignment.alignment_minimap2(fastaPath, index, 'bridge_orphan', processes, junctionDir)

        # if hits in reference
        if os.path.isfile(PAF_genome):
            allHits_genome = alignment.organize_hits_paf(PAF_genome)

            if 'bridge' in allHits_genome:
                hits_genome = allHits_genome['bridge']

                if len(hits_genome.alignments) == 1:
                    hit = hits_genome.alignments[0]

                    if (hit.qEnd - hit.qBeg) > hit.qLen - (hit.qLen/10):
                        offset = 2*len(junction.bridge.bridgeSeq)

                        for metacluster in [junction.metaclusterA, junction.metaclusterB]:
                            
                            # if bridge aligns next to bkp, do not pass the filter
                            if (hit.tName == metacluster.ref):
                                boolean = gRanges.overlap(hit.tBeg, hit.tEnd, metacluster.bkpPos-offset, metacluster.bkpPos+offset)[0]

                                if boolean:
                                    PASS = False
                                    break
            
        ## clean up 
        unix.rm([junctionDir])
    
    return(PASS)

