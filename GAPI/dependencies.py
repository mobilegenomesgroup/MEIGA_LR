'''
Module 'dependencies' - function to check if all the program dependencies are satisfied 

 A) Python modules
 B) External Programs
 C) files

'''

## DEPENDENCIES ##
# External
import pkgutil
import shutil
import os


## FUNCTIONS ##

def file_exists(ifile, text="", index=True):
    '''
    Check input files exist and their indexes
    '''
    if not os.path.exists( ifile ) :
        print("\n\n\n*** ERROR *** MEIGA can't locate the folowing files: ")
        print("%s file: %s \n" %(text, ifile) )
        exit(1)

    if not index: return ifile

    ext = { ".bam" : ".bai",
            ".fa"  : ".fai", 
            ".fna"  : ".mmi", 
            ".cram" : ".crai" }
    
    last = os.path.splitext(ifile)[-1]
    index = ifile + ext[last] if last != ".fna" else os.path.splitext(ifile)[0] + ext[last]

    if not os.path.exists(index): 
            print("\n\n\n*** ERROR *** MEIGA can't locate the folowing indexes: ")
            print("%s index: %s \n" %(text, ifile) )
            exit(1)

    return ifile

def missing_python_dependencies():
    '''
    Check if all the python dependencies are satisfied 
    if not print what is missing and how can be obtained
        
    Output: 
    1. isMissing: Boolean
    '''
    ## 0. Set variables
    isMissing = False

    ## 1. List of needed python packages
    needed_python_packages = ["numpy", "scipy", "pysam", "mappy", "cigar", "intervals", "pybedtools", "ordered_set", "pandas"]
    
    ## 2.  Select not founded packages
    missing_packages = []
    for p in needed_python_packages:
       
       if pkgutil.find_loader(p) is None:
           missing_packages.append(p)

    ## 3. check if the set is not empty 
    if bool(missing_packages):

        print("\n\n\n*** ERROR **** MEIGA dependencies not satisfied\nPython packages missing : install them with")
        
        ## 4. print list of shell commands to install the missing packages
        for missing_package in missing_packages:

            print("pip3 install " + missing_package)

        print("\n")
        isMissing = True

    return isMissing

def missing_input_files(listPaths):
    '''
    Check if all the program dependencies are satisfied.
    If not, print what is missing and how can be obtained
    
    Input: 
    1. List of required input files
        
    Output: 
    1. isMissing: Boolean
    '''
    ## 0. Set variables
    isMissing = False
    missing_files_directories = "\n\n\n*** ERROR *** MEIGA can't locate the folowing files/directories\n"
    is_missing_files_directories = False

    for path in listPaths:

        if path != None and not os.path.exists(path):
            
            is_missing_files_directories = True
            missing_files_directories += path + "\n"

    if is_missing_files_directories:

        isMissing = True
        print(missing_files_directories)
        print("\n")

    return isMissing

def missing_program_dependencies(annovarDir): 
    '''
    Check if all program dependencies are satisfied
    Both binaries and environment variables are checked
    
    Output: 
    1. isMissing: Boolean
    '''
    ## 0. Set variables
    isMissing = False
    
    ## 1. List required programs: binaries and environment variables
    bins = ["samtools", "bedtools", "bwa", "racon", "minimap2", "paftools.js", "k8", "wtdbg2"]
    envVars = []

    # if gene annotation active
    if annovarDir:
        bins.append("perl")
        envVars.append("ANNOVAR")

    ## 2. Get missing programs
    missingBins = [bin for bin in bins if not shutil.which(bin)]
    missingVars = [var for var in envVars if not var in os.environ.keys()]

    ## 3. If any program is missing 
    if missingBins or missingVars:
        
        isMissing = True
        print("\n\n\n*** ERROR *** MEIGA dependencies not satisfied")
        print("\n")
        
    # if missing bins
    if missingBins: 
        
        print("The following binaries are missing: ")
        for bin in missingBins: print(bin)
        print("\n")

    # if missing vars
    if missingVars: 
        
        print("The following environment variables are missing: ")
        for var in missingVars: print(var)
        print("\n")

    return isMissing

def missing_db(refDir, annovarDir):
    '''
    Check if program databases are satisfied
    
    Input:
    1. refDir: Directory containing reference databases (consensus sequences, source elements...)
    2. annovarDir: Directory containing annovar reference files
    
    Output: 
    1. isMissing: Boolean
    '''
    ## 0. Set variables
    isMissing = False

    ## 1. List required databases
    refFiles = ['repeats.bed', 'retrotransposons.bed', 'srcElements.bed', 'exons.bed']
    annovarFiles = ['build_annot.txt', 'build_annotMrna.fa']

    ## 2. Check if any is missing in the dirs provided
    missing_refFiles = [os.path.exists(refDir + '/' + file) for file in refFiles]

    if annovarDir:
        missing_annovarFiles = [os.path.exists(annovarDir + '/' + file) for file in annovarFiles]

    ## 3. If any database is missing 
    if not all(missing_refFiles):
        
        isMissing = True
        
        # print error message
        print("\n\n\n*** ERROR *** MEIGA database dependencies not satisfied.\nSome of the following REF files couldn't be located at", refDir)
        for file in refFiles: print(file)
        print("\n")

    if annovarDir and not all(missing_annovarFiles):
        
        isMissing = True

        # print error message
        print("\n\n\n*** ERROR *** MEIGA database dependencies not satisfied.\nSome of the following ANNOVAR files couldn't be located at", annovarDir)
        for file in annovarFiles: print(file)   
        print("\n")
        
    return isMissing


