'''
Module 'bnds' - Contains functions for dealing with ME-mediated BNDs
'''

## DEPENDENCIES ##
# External
import os
import subprocess
import numpy as np


# Internal
from GAPI import alignment
from GAPI import formats
from GAPI import unix
from GAPI import gRanges
from GAPI import clusters
from GAPI import retrotransposons
from GAPI import log
from GAPI import sequences
from GAPI import filters
from GAPI import annotation
from GAPI import events
from GAPI import structures



###############
## FUNCTIONS ##
###############

def get_me_fa(refDir, somaticTd):
    '''
    Create fasta file with all ME consensus sequences plus somatic and germline transduced regions.
    Returns path to the generated fasta file.

    Input:
    1. Path to reference DB directory
    2. Path to somatic tds fasta 

    Output:
    1. Path to fasta with ME consensus plus somatic and germline transduced sequences
    '''
    ## 1. Generate index containing consensus retrotranposon sequences + source elements downstream regions
    ## Consensus retrotransposon sequences
    consensusPath = refDir + '/consensusDb.fa'
    consensus = formats.FASTA()
    consensus.read(consensusPath)

    ## Transduced regions
    transducedPath = refDir + '/transducedDb.fa.masked.pA'
    transduced = formats.FASTA()
    transduced.read(transducedPath)

    ## Somatic transduced regions
    somaticTdDir = os.path.dirname(somaticTd)
    transduced_somatic = formats.FASTA()
    transduced_somatic.read(somaticTd)

    ## Merge in a single fasta
    allSeqs = formats.FASTA()
    allSeqs.seqDict = {**consensus.seqDict, **transduced.seqDict, **transduced_somatic.seqDict}

    ## Write full fasta
    fastaPath = somaticTdDir + '/reference_sequences_masked.fa'
    allSeqs.write(fastaPath)

    ## Index fasta 
    fileName = 'reference_sequences_masked'  
    index = alignment.index_minimap2(fastaPath, fileName, somaticTdDir)
    index = alignment.index_bwa(fastaPath, somaticTdDir)

    return(fastaPath)

def get_bnds_clip_fa(outDir, bndMetaclusters):
    '''
    Create fasta file with the longest clipping sequence for each metacluster in bndMetaclusters.
    Returns path to the generated fasta file
    '''
    fastaClipBnds = formats.FASTA() 
    fastaPath = outDir + '/clipping_bndMetaclusters.fa'

    for metacluster in bndMetaclusters:

        id = metacluster.id
        ref = metacluster.ref
        bkp = metacluster.bkpPos
        seq = metacluster.longestClip.altSeq

        idFa = "_".join([str(id), ref, str(bkp)])
        fastaClipBnds.seqDict[idFa] = seq

    fastaClipBnds.write(fastaPath)

    return(fastaPath)

def search_bridges(bndMetaclusters, fastaClipBnds, meFasta, processes, outDir):
    '''
    '''
    # # a) bwa mem
    # SAM_ME = alignment.alignment_bwa(fastaClipBnds, meFasta, 'consensus_metacluster', processes, outDir)
    # PAF_ME = alignment.sam2paf(SAM_ME, 'consensus_metacluster', outDir)
    
    # # b) minimap2
    me_index = os.path.splitext(meFasta)[0] + '.mmi'
    PAF_ME = alignment.alignment_minimap2(fastaClipBnds, me_index, 'consensus_metacluster', processes, outDir)

    ## Organize hits according to their corresponding metacluster
    allHits_bnds = alignment.organize_hits_paf(PAF_ME)
    # [allHits_bnds[metaId].selectPrimary() for metaId in allHits_bnds.keys()]

    ## 3. Create new FA with selected META
    #########################################
    faBNDs = formats.FASTA() 
    outFasta = outDir + '/consensus_metaclusters_genome.fa'

    noAnchorMetaclusters = []
    metaclusters = []

    offset = 200

    # For each metacluster
    for metacluster in bndMetaclusters:

        ## Check if metacluster is of DEL type
        lst = [i.typeC for i in metacluster.events if i.type == 'CLIPPING']

        if (max(lst, key=lst.count) == 'DEL'):
            metacluster.type = 'DEL'

            # Ask for more support
            if (metacluster.supportingReads()[0] < 4):
                continue
        
        else:
            metacluster.type = 'CLIPPING'
        
        ## Set metacluster attributes
        id = metacluster.id
        ref = metacluster.ref
        bkp = metacluster.bkpPos
        clipSide = 'right' if metacluster.strand == '+' else 'left'

        ## 3.1 Collect consensus inserted sequence hits
        metaId = "_".join([str(id), ref, str(bkp)])

        ## 4.1 Collect polyA hits
        seq = metacluster.longestClip.altSeq
        seq, pA_chain = is_there_polyA(seq, formats.PAF_chain([]), id=metaId)

        if pA_chain.alignments:
            metacluster.SV_features['BRIDGE'] = [pA_chain.alignments[0]]

        ## Hits in the reference genome
        if (metaId in allHits_bnds):
            
            # select primary alignments for metacluster. Update PAF alignments
            allHits_bnds[metaId].selectPrimary()
            hits_genome = allHits_bnds[metaId]

            if hits_genome.alignments:

                ## 1. Create chain of alignments ##
                chain = hits_genome.chain_td()
                seq = metacluster.longestClip.altSeq
                seq, chain = is_there_polyA(seq, chain)

                if clipSide == 'right':

                    ## For each hit
                    sortedAlignments = sorted(chain.alignments, key=lambda x: x.qBeg, reverse=False)
                    qLen = 0

                    for hit in sortedAlignments:
                        
                        boolean, overlapLen, coord = gRanges.overlap_extended(hit.qBeg, hit.qEnd, qLen, qLen+offset)

                        if boolean:

                            if 'BRIDGE' in metacluster.SV_features:

                                identities = [ i.tName.split('|')[1] for i in metacluster.SV_features['BRIDGE'] if ('|' in i.tName) ]
                                td_identities = [ i.tName for i in metacluster.SV_features['BRIDGE'] if ('td' in i.tName) ]

                                # if pre-existing identities more than 'poly(A/T)', append only if compatible identity
                                if identities:

                                    if ('Alu' in hit.tName) and ('Alu' not in identities):
                                        break

                                    if ('SVA' in hit.tName) and ('SVA' not in identities):
                                        break

                                    if ('L1' in hit.tName) and (('Alu' in identities) or ('SVA' in identities)):
                                        break

                                    if ('td' in hit.tName) and (td_identities):
                                        if hit.tName not in td_identities:
                                            break
                                    
                                metacluster.SV_features['BRIDGE'].append(hit)

                            else:

                                metacluster.SV_features['BRIDGE'] = [hit]
                                
                            qLen = hit.qEnd
                        
                        else:
                            break

                    if 'BRIDGE' in metacluster.SV_features: 
                        metaclusters.append(metacluster)
                
                else:

                    ## For each hit
                    sortedAlignments = sorted(chain.alignments, key=lambda x: x.qEnd, reverse=True)
                    qLen = sortedAlignments[0].qLen

                    for hit in sortedAlignments:
                        
                        boolean, overlapLen, coord = gRanges.overlap_extended(hit.qBeg, hit.qEnd, qLen-offset, qLen)

                        if boolean:

                            if 'BRIDGE' in metacluster.SV_features:

                                identities = [ i.tName.split('|')[1] for i in metacluster.SV_features['BRIDGE'] if ('|' in i.tName) ]
                                td_identities = [ i.tName for i in metacluster.SV_features['BRIDGE'] if ('td' in i.tName) ]

                                # if pre-existing identities more than 'poly(A/T)', append only if compatible identity
                                if identities:

                                    if ('Alu' in hit.tName) and ('Alu' not in identities):
                                        break

                                    if ('SVA' in hit.tName) and ('SVA' not in identities):
                                        break

                                    if ('L1' in hit.tName) and (('Alu' in identities) or ('SVA' in identities)):
                                        break
                                    
                                    if ('td' in hit.tName) and (td_identities):
                                        if hit.tName not in td_identities:
                                            break

                                metacluster.SV_features['BRIDGE'].append(hit)

                            else:

                                metacluster.SV_features['BRIDGE'] = [hit]
                                
                            qLen = hit.qBeg
                        
                        else:
                            break

                    if 'BRIDGE' in metacluster.SV_features: 
                        metaclusters.append(metacluster)

        ## write anchorSeq to fasta
        # select only metaclusters with bridge
        if 'BRIDGE' in metacluster.SV_features:

            anchor = False
            seq = metacluster.longestClip.altSeq
            
            # get all possible anchors
            nbBridges = len(metacluster.SV_features['BRIDGE'])
            for i in reversed(range(nbBridges)):
                
                if metacluster.strand == '+': 
                    hitEnd = metacluster.SV_features['BRIDGE'][i].qEnd
                    anchorSeq = seq[hitEnd:]

                else:
                    hitBeg = metacluster.SV_features['BRIDGE'][i].qBeg
                    anchorSeq = seq[:hitBeg]

                if len(anchorSeq) > 100:

                    id = metacluster.id
                    ref = metacluster.ref
                    bkp = metacluster.bkpPos
                    anchor = True

                    idFa = "_".join([str(id), ref, str(bkp), str(i)])
                    faBNDs.seqDict[idFa] = anchorSeq

            if not anchor:
                noAnchorMetaclusters.append(metacluster)        

            # set bridgeSeq
            hitBeg = min([hit.qBeg for hit in metacluster.SV_features['BRIDGE']])
            hitEnd = max([hit.qEnd for hit in metacluster.SV_features['BRIDGE']])
            metacluster.bridgeSeq = seq[hitBeg:hitEnd]

    faBNDs.write(outFasta)

    return(metaclusters, noAnchorMetaclusters, outFasta)

def del2clipping(metacluster, reference, processes, confDict, bam, outDir):
    '''
    '''
    ## if metacluster is of DEL type
    if metacluster.type == 'DEL':

        ## 0. Create directory 
        outDir = outDir + "/" + metacluster.id 
        unix.mkdir(outDir)
        newClippings = []

        ## 1.1 Collect metacluster supporting reads and write a fasta
        supportingReadsA = metacluster.collect_altSeqs(offset = 0)
        supportingReads = formats.FASTA()
        supportingReads.seqDict = {**supportingReadsA.seqDict}
        supportingReadsFasta = outDir + '/supportingReads_altSeq.fa'
        supportingReads.write(supportingReadsFasta)

        ## Launch alignment
        index = os.path.splitext(reference)[0] + '.mmi'
        PAF_path = alignment.alignment_minimap2(supportingReadsFasta, index, 'del2clipping', processes, outDir)
        allHits_bnds = alignment.organize_hits_paf(PAF_path)

        for readNAme in allHits_bnds.keys():

            allHits_bnds[readNAme].selectPrimary()

            sortedAlignments = sorted([i for i in allHits_bnds[readNAme].alignments if (i.MAPQ >= confDict['minMAPQ'] and i.tName == metacluster.ref)], key=lambda x: x.qLen, reverse=False)

            if sortedAlignments:

                for eventA in metacluster.events:

                    if readNAme == eventA.readName:

                        firstOperationLen = 2000
                        refBkp = sortedAlignments[0].tBeg
                        readBkp = eventA.readBkp + sortedAlignments[0].qBeg

                        left_CLIPPING = events.CLIPPING(eventA.ref, refBkp, refBkp, firstOperationLen, eventA.alignedLen, 'left', None, eventA.pair, eventA.readName, eventA.readSeq, readBkp, None, eventA.sample, 'DEL')
                        newClippings.append(left_CLIPPING)

                        break

        if newClippings:

            eventsDict = {}
            eventsDict['LEFT-CLIPPING'] = newClippings
            metaclusters = newClips2metaclusters(eventsDict, confDict, bam, outDir)
            
            if metaclusters:
                
                ## Clean up 
                unix.rm([outDir])

                # Select metacluster with greater read support to create junction
                selectedMeta = sorted(metaclusters, key=lambda x: x.nbTotal, reverse=True)[0]

                # Add alignLenStd attrib
                metacluster.alignLenStd = np.std([i.alignedLen for i in metacluster.events])
                selectedMeta.alignLenStd = np.std([i.alignedLen for i in selectedMeta.events])

                # Create junction
                junction = clusters.BND_junction(metacluster, selectedMeta)
                
                return(junction)
    
        ## Clean up 
        unix.rm([outDir])

    return(None)

def newClips2metaclusters(eventsDict, confDict, bam, outDir):
    '''
    '''
    ## Define bin database sizes
    binSizes = [100, 1000, 10000, 100000, 1000000]
    
    ## Create bins
    offset = 1000
    ref = eventsDict['LEFT-CLIPPING'][0].ref
    beg = eventsDict['LEFT-CLIPPING'][0].beg - offset
    end = eventsDict['LEFT-CLIPPING'][0].beg + offset

    eventsBinDb = structures.create_bin_database_interval(ref, beg, end, eventsDict, binSizes)

    ## 3. Group events into clusters ##
    step = 'CLUSTERING'
    msg = 'Group events into clusters' 
    log.step(step, msg)
    clustersBinDb = clusters.create_clusters(eventsBinDb, confDict)
    
    msg = 'Number of created clusters: ' + str(clustersBinDb.nbEvents()[0])
    log.step(step, msg)

    ## 4. Polish clusters ##
    step = 'POLISH'
    msg = 'Polish SV clusters' 
    log.step(step, msg)
    clusters.polish_clusters(clustersBinDb, confDict['minClusterSize'], confDict['genotyper'])
    
    ## 5. Group events into metaclusters ##
    step = 'META-CLUSTERING'
    msg = 'Group events into metaclusters' 
    log.step(step, msg)

    buffer = 10 if confDict['genotyper'] else 50
    metaclusters = clusters.create_metaclusters(clustersBinDb, buffer)        
    msg = 'Number of created metaclusters: ' + str(len(metaclusters)) 
    log.step(step, msg)

    ## 7. Filter metaclusters ##
    step = 'FILTER 1'
    msg = 'Filter out metaclusters' 
    log.step(step, msg)
    
    filters2Apply = ['MIN-NBREADS', 'MAX-NBREADS']
    metaclustersDict = {}
    metaclustersDict['INS'] = metaclusters
    metaclustersPassDict = filters.filter_metaclusters_pass(metaclustersDict, filters2Apply, confDict, bam)
    metaclusters = metaclustersPassDict['INS'] if 'INS' in metaclustersPassDict else []

    ## 6. Infer structural variant type ##
    step = 'SV-TYPE'
    msg = 'Infer structural variant type' 
    log.step(step, msg)

    outDir = outDir + '/SV_TYPE/' 
    metaclustersSVType = clusters.SV_type_metaclusters(metaclusters, confDict['minINSlen'], confDict['technology'], outDir, confDict, complAlignments = False)
    clusters.lighten_up_metaclusters(metaclustersSVType, bam, confDict)

    if 'BND' in metaclustersSVType:
        return(metaclustersSVType['BND'])

    else:
        return([])


def search_junctions(metaclusters, noAnchorMetaclusters, fastaAnchorBnds, reference, processes, outDir, targetRefs):
    '''
    '''
    ## 4. PAF alignment to whole-genome
    ########################################
    index = os.path.splitext(reference)[0] + '.mmi'
    pafBNDs = alignment.alignment_minimap2(fastaAnchorBnds, index, "consensus_metacluster_genome", processes, outDir)

    ## Organize hits according to their corresponding metacluster
    allHits_bnds = alignment.organize_hits_paf(pafBNDs)

    selectedMetaclusters = []
    # For each metacluster
    for metacluster in metaclusters:

        id = metacluster.id
        ref = metacluster.ref
        bkp = metacluster.bkpPos
        meta_id = "_".join([str(id), ref, str(bkp)])
        metacluster.SV_features['ANCHOR'] = []
        clipSide = 'right' if metacluster.strand == '+' else 'left'
        metacluster.alignLenStd = np.std([i.alignedLen for i in metacluster.events])

        ## Hits in the reference genome
        keys = [id for id in allHits_bnds.keys() if meta_id in id]
        for metaId in keys:

            # select primary alignments for metacluster. Update PAF alignments
            allHits_bnds[metaId].selectPrimary()
            hits_genome = allHits_bnds[metaId]

            ## 1. Create chain of alignments ##
            chain = hits_genome.chain_td()

            ## 2. Select alignment closer to bkp after bridge ##
            # if metacluster plus -- qBeg
            if clipSide == 'right':
                
                sortedAlignments = sorted(chain.alignments, key=lambda x: x.qBeg, reverse=False)

                counter = 0
                for anchor in sortedAlignments:

                    if counter >= 2: break ## allow only two anchors to be added
                    counter += 1

                    if anchor.strand == '+':
                        anchor = [str(anchor.tName), anchor.tBeg, str(anchor.strand)]

                        if anchor not in metacluster.SV_features['ANCHOR']:
                            metacluster.SV_features['ANCHOR'].append(anchor)

                    else:
                        anchor = [str(anchor.tName), anchor.tEnd, str(anchor.strand)]

                        if anchor not in metacluster.SV_features['ANCHOR']:
                            metacluster.SV_features['ANCHOR'].append(anchor)  

            # if metacluster minus -- qEnd
            else:

                sortedAlignments = sorted(chain.alignments, key=lambda x: x.qEnd, reverse=True)

                counter = 0
                for anchor in sortedAlignments:

                    if counter >= 2: break ## allow only to anchors to be added
                    counter += 1

                    if anchor.strand == '+':
                        anchor = [str(anchor.tName), anchor.tEnd, str(anchor.strand)]

                        if anchor not in metacluster.SV_features['ANCHOR']:
                            metacluster.SV_features['ANCHOR'].append(anchor)

                    else:
                        anchor = [str(anchor.tName), anchor.tBeg, str(anchor.strand)]

                        if anchor not in metacluster.SV_features['ANCHOR']:
                            metacluster.SV_features['ANCHOR'].append(anchor)

        if metacluster.SV_features['ANCHOR']:
            selectedMetaclusters.append(metacluster)
    
    metaclusters = selectedMetaclusters + noAnchorMetaclusters

    for metacluster in noAnchorMetaclusters:
        metacluster.alignLenStd = np.std([i.alignedLen for i in metacluster.events])

    allJunctions = []
    includedMetaclusters = []

    # sort metacluster by std alignedLen (approach to avoid anchor-bigBridges anchors)
    selectedMetaclusters = sorted(selectedMetaclusters, key=lambda x: x.alignLenStd, reverse=True)
    metaclusters = sorted(metaclusters, key=lambda x: x.alignLenStd, reverse=True)

    for metacluster in selectedMetaclusters:

        # offset = 150
        offset = 350
        lst = [i.typeC for i in metacluster.events if i.type == 'CLIPPING']
        if max(lst,key=lst.count) == 'DEL': offset = 500

        for anchor in metacluster.SV_features['ANCHOR']:
            
            ref, bkp, strand = anchor

            for metaPartner in metaclusters:

                if (metaPartner.id not in includedMetaclusters) and (metacluster.id not in includedMetaclusters) and (metaPartner.ref == ref):

                    if (metaPartner.id != metacluster.id):

                        if abs(bkp-metaPartner.bkpPos) <= offset:
                            
                            ## Get read support
                            readIdsA = set(metacluster.reads)
                            readIdsB = set(metaPartner.reads)
                            sharedReads = readIdsA.intersection(readIdsB)
                            nbSharedReads = len(list(sharedReads))

                            if (nbSharedReads >= 1):

                                sortedJunction = sorted([metacluster, metaPartner], key=lambda x: (x.ref, x.bkpPos), reverse=False)
                                allJunctions.append(clusters.BND_junction(sortedJunction[0], sortedJunction[1]))
                                includedMetaclusters.append(metacluster.id)
                                includedMetaclusters.append(metaPartner.id)
                                
                    else:

                        # if INV
                        ## TODO: review if all inverted alignments meet this condition

                        # if anchor and metaPartner strands are the same
                        if len(list(set([strand, metaPartner.strand]))) == 1:

                            if abs(bkp-metaPartner.bkpPos) <= offset:

                                allJunctions.append(clusters.BND_junction(metacluster, metacluster))
                                includedMetaclusters.append(metacluster.id)

    ## 1. Open output file 
    rgDir = outDir + '../ME_RG'
    unix.mkdir(rgDir)
    outFilePath = outDir + '../ME_RG/BND_MEIGA.FAILED.bed'
    outFile = open(outFilePath, 'w')

    for metacluster in selectedMetaclusters:

        if metacluster.ref not in targetRefs:
            continue

        ## write to std output no paired BND metaclusters 
        if metacluster.id not in includedMetaclusters:
            
            if ('BRIDGE' in metacluster.SV_features) and ('ANCHOR' in metacluster.SV_features):

                hits = [hit.tName for hit in metacluster.SV_features['BRIDGE']]
                if ('poly(A/T)' in hits) or ('|L1|' in hits): 

                    ## Write entry
                    anchorRef, anchorBkp, anchorStrand = metacluster.SV_features['ANCHOR'][0]
                    row = metacluster.ref + "\t" + str(metacluster.bkpPos) + "\t" + str(metacluster.bkpPos) + "\n" + anchorRef + "\t" + str(anchorBkp)  + "\t" + str(anchorBkp) + "\n"
                    outFile.write(row)

    outFile.close()
    
    return(allJunctions)

def polish_junctions(allJunctions, confDict, meFasta, reference, processes, outDir):
    '''
    '''

    fa_junctions = formats.FASTA() 
    outFasta = outDir + 'polished_junctions_genome.fa'

    ## implement consensus step:
    for junction in allJunctions:

        junctionDir = outDir + junction.metaclusterA.id + "_" + junction.metaclusterB.id

        ## 1. Polish metacluster´s consensus sequence
        junction.polish_junctions(confDict, reference, junctionDir, assemblyStep = True)

        id = "_".join([str(junction.metaclusterA.id), str(junction.metaclusterB.id)])
        index = 1
        for k, seq in junction.consensusFasta.seqDict.items():
            fa_junctions.seqDict[id + "_" + str(index)] = seq
            index += 1

    ## alignment to whole-genome
    fa_junctions.write(outFasta)
    index = os.path.splitext(reference)[0] + '.mmi'
    paf_junctions = alignment.alignment_minimap2(outFasta, index, 'polished_junctions_genome', processes, outDir)

    ## Organize hits according to their corresponding metacluster
    allHits_junctions = alignment.organize_hits_paf(paf_junctions)
    
    # For each metacluster
    for junction in allJunctions:

        junctionId = "_".join([str(junction.metaclusterA.id), str(junction.metaclusterB.id)]) + "_1"

        ## Hits in the reference genome
        if junctionId in allHits_junctions:
            
            allHits_junctions[junctionId].selectPrimary()
            hits_genome = allHits_junctions[junctionId]

            ## 1. Create chain of alignments ##
            chain = hits_genome.chain_td()
            sortedAlignments = sorted(chain.alignments, key=lambda x: x.qBeg, reverse=False)

            # sortedJunction = sorted([metacluster, metaPartner], key=lambda x: (x.ref, x.bkpPos), reverse=False)

            # check anchors
            offset = 150

            if len(sortedAlignments) >= 2:

                anchorA = sortedAlignments[0]
                anchorB = sortedAlignments[-1]

                anchorA_bkp = None
                anchorB_bkp = None

                metaclusters = [junction.metaclusterA, junction.metaclusterB]
                for metacluster in [junction.metaclusterA, junction.metaclusterB]:

                    ref = metacluster.ref
                    bkp = metacluster.bkpPos

                    # set anchorA
                    if anchorA.tName == ref:
                        
                        if (anchorA.strand == '+') and (anchorA.tEnd in range(bkp-offset, bkp+offset)):
                            anchorA_bkp = anchorA.qEnd
                            metaclusters.remove(metacluster)
                            break

                        elif (anchorA.strand == '-') and (anchorA.tBeg in range(bkp-offset, bkp+offset)):
                            anchorA_bkp = anchorA.qEnd
                            metaclusters.remove(metacluster)
                            break
                
                if anchorA_bkp:

                    # set anchorB
                    ref = metaclusters[0].ref
                    bkp = metaclusters[0].bkpPos

                    if anchorB.tName == ref:

                        if (anchorB.strand == '+') and (anchorB.tBeg in range(bkp-offset, bkp+offset)):
                            anchorB_bkp = anchorB.qBeg

                        elif (anchorB.strand == '-') and (anchorB.tEnd in range(bkp-offset, bkp+offset)):
                            anchorB_bkp = anchorB.qBeg

                ## create bridge
                if anchorA_bkp and anchorB_bkp:
                    
                    seq = fa_junctions.seqDict[junctionId]
                    seqBridge = seq[anchorA_bkp:anchorB_bkp]

                    ## Generate fasta containing representative inserted sequence at BND junction
                    fasta = formats.FASTA()
                    fasta.seqDict['insert'] = seqBridge
                    junctionDir = outDir + '/' + junctionId + '/'
                    unix.mkdir(junctionDir)
                    insertPath = junctionDir + 'insert.fa'
                    fasta.write(insertPath)
                    
                    ## Infer structure
                    me_index = os.path.splitext(meFasta)[0] + '.mmi'
                    structure = retrotransposons.retrotransposon_structure(insertPath, me_index, junctionDir)

                    ## TODO: Review structure. There is more info to extract. Better to change output to VCF?
                    if structure:

                        supplClusters = []

                        info = {}
                        info['bridgeType'] = structure['INS_TYPE']
                        info['family'] = ','.join(structure['FAMILY']) 
                        info['srcId'] = ','.join(structure['CYTOBAND']) if ('CYTOBAND' in structure and structure['CYTOBAND']) else 'None'
                        info['bridgeSeq'] = seqBridge
                        info['bridgeLen'] = len(seqBridge)
                        info['polyA'] = structure['POLYA']
                        info['qHits'] = [hit for hit in structure['HITS'] if hit.primary]
                        info['percRes'] = structure['PERC_RESOLVED']
                        info['mechanism'] = structure['MECHANISM']
                        info['strand'] = structure['STRAND']

                        junction.bridge = clusters.BRIDGE(supplClusters, info)

def polish_junctions_v2(allJunctions, confDict, meFasta, reference, processes, outDir):
    '''
    '''

    fa_junctions = formats.FASTA() 
    outFasta = outDir + 'polished_junctions_genome.fa'

    targetIntervals = []

    ## For each junction
    for junction in allJunctions:

        junctionDir = outDir + junction.metaclusterA.id + "_" + junction.metaclusterB.id

        ## 1. Polish metacluster's consensus sequence
        junction.polish_junctions(confDict, reference, junctionDir, assemblyStep = False)

        id = "_".join([str(junction.metaclusterA.id), str(junction.metaclusterB.id)])
        index = 1
        for k, seq in junction.consensusFasta.seqDict.items():
            fa_junctions.seqDict[id + "_" + str(index)] = seq
            index += 1

        ## 2. Get reference sequences at bkps
        offset = 3000
        for metacluster in [junction.metaclusterA, junction.metaclusterB]:

            ref = metacluster.ref
            bkp = metacluster.bkpPos

            if metacluster.strand == '+':
                coord = ref + ':' + str(bkp-offset) + '-' + str(bkp)
                targetIntervals.append(coord)
                metacluster.targetRef_id = coord

            elif metacluster.strand == '-':
                coord = ref + ':' + str(bkp) + '-' + str(bkp+offset)
                targetIntervals.append(coord)
                metacluster.targetRef_id = coord

    ## Get target reference sequences
    fa_junctions.write(outFasta)
    targetIntervals = ' '.join(targetIntervals)
    paf_junctions = targeted_alignment_minimap2(outFasta, targetIntervals, reference, outDir)

    ## Organize hits according to their corresponding metacluster
    allHits_junctions = alignment.organize_hits_paf(paf_junctions)
    
    # For each metacluster
    for junction in allJunctions:

        junctionId = "_".join([str(junction.metaclusterA.id), str(junction.metaclusterB.id)]) + "_1"

        ## Hits in the reference genome
        if junctionId in allHits_junctions:
            
            ## 1. Create chain of alignments for junction ##
            allHits_junctions[junctionId].selectPrimary()
            hits_genome = allHits_junctions[junctionId]
            chain = hits_genome.chain_td()
            sortedAlignments = sorted(chain.alignments, key=lambda x: x.nbMatches, reverse=True)

            ## 2. Check anchors ##
            buffer = 100
            bkpsBeg = []
            bkpsEnd = []

            for metacluster in [junction.metaclusterA, junction.metaclusterB]:

                a = [ aln for aln in sortedAlignments if (aln.tName == metacluster.targetRef_id) and (aln.MAPQ >= 20)]

                for anchor in a:
                    
                    strands = list(set([metacluster.strand, anchor.strand]))
                    if (strands == ["+"]) or (strands == ["-"]):

                        if not bkpsEnd:
                            
                            if anchor.tEnd in range(offset-buffer, offset+buffer):
                                bkpsEnd.append(anchor.qEnd)
                    
                    else:

                        if not bkpsBeg:

                            if anchor.tBeg in range(-buffer, buffer):
                                bkpsBeg.append(anchor.qBeg)

            if (len(bkpsBeg) == 1) and (len(bkpsEnd) == 1):

                bkps = bkpsBeg + bkpsEnd
                anchorA_bkp = min(bkps)
                anchorB_bkp = max(bkps)

                ## create bridge
                if anchorA_bkp and anchorB_bkp:
                    
                    seq = fa_junctions.seqDict[junctionId]
                    seqBridge = seq[anchorA_bkp:anchorB_bkp]
            
            else:

                if hasattr(junction.metaclusterA, 'bridgeSeq'): 

                    if hasattr(junction.metaclusterB, 'bridgeSeq'): 

                        if len(junction.metaclusterA.bridgeSeq) > len(junction.metaclusterB.bridgeSeq):
                            seqBridge = junction.metaclusterA.bridgeSeq
                        
                        else:
                            seqBridge = junction.metaclusterB.bridgeSeq

                    else:
                        seqBridge = junction.metaclusterA.bridgeSeq

                elif hasattr(junction.metaclusterB, 'bridgeSeq'):
                    seqBridge = junction.metaclusterB.bridgeSeq
                
                else:
                    seqBridge = junction.metaclusterB.bridgeSeq

            ## Generate fasta containing representative inserted sequence at BND junction
            fasta = formats.FASTA()
            fasta.seqDict['insert'] = seqBridge
            junctionDir = outDir + '/' + junctionId + '/'
            unix.mkdir(junctionDir)
            insertPath = junctionDir + 'insert.fa'
            fasta.write(insertPath)
            
            ## Infer structure
            me_index = os.path.splitext(meFasta)[0] + '.mmi'
            structure = retrotransposons.retrotransposon_structure(insertPath, me_index, junctionDir)

            ## TODO: Review structure. There is more info to extract. Better to change output to VCF?
            if structure:

                supplClusters = []

                info = {}
                info['bridgeType'] = structure['INS_TYPE']
                info['family'] = ','.join(structure['FAMILY']) 
                info['srcId'] = ','.join(structure['CYTOBAND']) if ('CYTOBAND' in structure and structure['CYTOBAND']) else 'None'
                info['bridgeSeq'] = seqBridge
                info['bridgeLen'] = len(seqBridge)
                info['polyA'] = structure['POLYA']
                info['qHits'] = [hit for hit in structure['HITS'] if hit.primary]
                info['percRes'] = structure['PERC_RESOLVED']
                info['mechanism'] = structure['MECHANISM']
                info['strand'] = structure['STRAND']

                junction.bridge = clusters.BRIDGE(supplClusters, info)

def targeted_alignment_minimap2(FASTA, targetInterval, reference, outDir):
    '''
    '''
    ## 0. Create logs directory
    logDir = outDir + '/Logs'
    unix.mkdir(logDir)

    ## 1. Extract the reference target region prior alignment 
    target = outDir + '/target.fa'
    err = open(logDir + '/target.err', 'w') 
    command = 'samtools faidx ' + reference + ' ' + targetInterval + ' > ' + target
    status = subprocess.call(command, stderr=err, shell=True)

    if status != 0:
        step = 'TARGET'
        msg = 'Extraction of reference target region failed' 
        log.step(step, msg)
        return None

    ## 2. Align the sequences into the target region 
    PAF = outDir + '/target_alignments.paf'
    err = open(logDir + '/align.err', 'w') 
    command = 'minimap2 -c ' + target + ' ' + FASTA + ' > ' + PAF
    status = subprocess.call(command, stderr=err, shell=True)

    if status != 0:
        step = 'ALIGN'
        msg = 'Local alignment failed' 
        log.step(step, msg)
        return None

    return PAF


def is_there_polyA(sequence, chain, id='metacluster'):
    '''
    Infer insertion strand based on two criteria:
        1) Location of polyA/T tail at sequence ends
        2) Alignment strand for the insert 3' end over the template sequence

    Input: 
        1. sequence: consensus inserted sequence
        2. chain: Sequence chain of alignments over retrotranposon consensus sequences and/or transduced regions

    Output:
        1. strand: Insertion strand (+, - or None) 
        2. polyA: boolean specifying if polyA/T sequence was found
    '''
    lenSeq = len(sequence)

    ## Configuration for monomere search:
    windowSize = 8
    maxWindowDist = 2
    minMonomerSize = 10
    minPurity = 80  

    ## Seach poly(A) monomers
    targetMonomer = 'A'
    monomersA = sequences.find_monomers(sequence, targetMonomer, windowSize, maxWindowDist, minMonomerSize, minPurity)

    ## Seach poly(T) monomers
    targetMonomer = 'T'
    monomersT = sequences.find_monomers(sequence, targetMonomer, windowSize, maxWindowDist, minMonomerSize, minPurity)

    if (not monomersA) and (not monomersT):
        return sequence, chain
    
    polyA = None
    if monomersA:

        ## Select monomer closest to sequence end
        candidate = monomersA[-1]

        ## Filter out monomer if more than Xbp from end
        seqLen = len(sequence)
        dist2end = seqLen - candidate.end

        if dist2end <= 30:
            sequence = sequence[:candidate.beg]
            polyA = candidate
            strand = '+'

        else:
            sequence = sequence

    if monomersT:

        ## Select monomer closest to sequence end
        candidate = monomersT[0]

        ## Filter out monomer if more than Xbp from beg
        dist2end = candidate.beg

        if candidate.beg <= 30:
            sequence = sequence[candidate.end:]
            polyA = candidate
            strand = '-'

        else:
            sequence = sequence

    if polyA and chain.alignments:
        firstAlignment = chain.alignments[0]
        fields = [firstAlignment.qName, firstAlignment.qLen, polyA.beg, polyA.end, strand, 'poly(A/T)', 0, 0, 0, 0, 0, 0, 'tp:A:P']            
        alignment = formats.PAF_alignment(fields)
        chain.alignments.append(alignment) 

        return sequence, chain
    
    elif polyA:
        fields = [id, lenSeq, polyA.beg, polyA.end, strand, 'poly(A/T)', 0, 0, 0, 0, 0, 0, 'tp:A:P']            
        alignment = formats.PAF_alignment(fields)
        chain.alignments.append(alignment) 

        return sequence, chain

    return sequence, chain


def failedINS2BND_metaclusters(metaclusters, bam, confDict):
    '''
    Infer the SV type supported by each metacluster

    Input:
        1. metaclusters: list of metaclusters
        2. minINDELlen: minimum INS and DEL lenght
        3. technology: sequencing technology (NANOPORE, PACBIO or ILLUMINA)
        4. rootOutDir: root output directory

    Output:
        1. metaclustersSVType: dictionary containing one key per SV type and the list of metaclusters identified as value
    '''
    metaclustersSVType = {}

    for metacluster in metaclusters:
        
        # for insertions greater than 1000 bp, discard. Likely clippings belongin to insertion (Real insertion, not spurious)
        if (metacluster.consensusEvent is not None) and (metacluster.consensusEvent.length >= 1000):
            continue

        #) Split metacluster
        metacluster.subclusters = metacluster.create_subclusters()        
        subClusterTypes = list(metacluster.subclusters.keys())
        new_metaclusters = []

        if ('RIGHT-CLIPPING' in subClusterTypes) or ('LEFT-CLIPPING' in subClusterTypes):
            new_metaclusters = [metacluster]
            metacluster.SV_type = 'BND'
            new_metaclusters = [ clusters.META_cluster([cluster]) for key,cluster in metacluster.subclusters.items() if key != 'INS' ]
            new_metaclusters = [ metacluster for metacluster in new_metaclusters if filters.filter_min_nb_reads(metacluster, confDict['minReads'], confDict['minNormalReads']) ]
            for clusterA in new_metaclusters: 
                clusterA.SV_type = 'BND'
                clusterA.create_subclusters()
             
        for new_metacluster in new_metaclusters:
            
            # get suppClusters for potential BNDs
            if new_metacluster.SV_type == 'BND':
                
                subClusterTypes = list(new_metacluster.subclusters.keys())
                
                ## Retrieve clipping cluster
                # a) Right clipping
                if 'RIGHT-CLIPPING' in subClusterTypes:
                    clippingCluster = new_metacluster.subclusters['RIGHT-CLIPPING']
                    new_metacluster.longestClip = clippingCluster.longest_clipping_seq(offset = 0)
                    new_metacluster.consensusEvent = clippingCluster.consEventBridge()
                    new_metacluster.consensusFasta = formats.FASTA()
                    new_metacluster.consensusFasta.seqDict[new_metacluster.consensusEvent.readName] = new_metacluster.consensusEvent.readSeq
                    new_metacluster.strand = '+'

                # b) Left clipping
                elif 'LEFT-CLIPPING' in subClusterTypes:
                    clippingCluster = new_metacluster.subclusters['LEFT-CLIPPING']
                    new_metacluster.longestClip = clippingCluster.longest_clipping_seq(offset = 0)
                    new_metacluster.consensusEvent = clippingCluster.consEventBridge()
                    new_metacluster.consensusFasta = formats.FASTA()
                    new_metacluster.consensusFasta.seqDict[new_metacluster.consensusEvent.readName] = new_metacluster.consensusEvent.readSeq
                    new_metacluster.strand = '-'
                    
                ## Determine BND breakpoint position
                new_metacluster.bkpPos = clippingCluster.infer_breakpoint()

                ## For each clipping event composing the cluster select complementary suppl. alignments
                complementaryAlignments = clippingCluster.search4complAlignments()

                ## Cluster supplementary alignment positions
                new_metacluster.supplClusters =  clippingCluster.cluster_suppl_positions(complementaryAlignments)

            # A) Initialize list containing metaclusters of a given SV type
            if new_metacluster.SV_type not in metaclustersSVType:
                metaclustersSVType[new_metacluster.SV_type] = [new_metacluster]

            # B) Add metacluster to the list        
            else:
                metaclustersSVType[new_metacluster.SV_type].append(new_metacluster)   

    clusters.lighten_up_metaclusters(metaclustersSVType, bam, confDict) 

    return metaclustersSVType

def junctionType(junctions):
    
    for junction in junctions:
        
        # set clusters A and B
        if (junction.junctionType() == 'intrachromosomal') and (junction.metaclusterA.bkpPos > junction.metaclusterB.bkpPos): 
            clusterA = junction.metaclusterB
            clusterB = junction.metaclusterA
        else:
            clusterA = junction.metaclusterA
            clusterB = junction.metaclusterB
        
        # set interval or bkp annotation 
        if (junction.junctionType() != 'intrachromosomal'):
            junction.svType = 'TRA'
            junction.annot_bkp = True

        elif (junction.junctionType() == 'intrachromosomal') and (clusterA.strand == "+") and (clusterB.strand == "-"):
            junction.svType = 'DEL'
            junction.annot_bkp = False

        elif (junction.junctionType() == 'intrachromosomal') and (clusterA.strand == "-") and (clusterB.strand == "+"):
            junction.svType = 'DUP'
            junction.annot_bkp = False

        elif (junction.junctionType() == 'intrachromosomal') and (((clusterA.strand == "+") and (clusterB.strand == "+")) or ((clusterA.strand == "-") and (clusterB.strand == "-"))):
            junction.svType = 'INV'
            junction.annot_bkp = True
        
        else:
            junction.svType = 'UNK'
            junction.annot_bkp = True

def gene_annotation(junctions, annovarDir, outDir):
    '''
    '''
    ## 1. Write header
    outPath = outDir + '/' + 'events.annovar'
    outFile = open(outPath, 'w')

    ## 2. Write events
    for junction in junctions:
        
        # set clusters A and B
        if (junction.junctionType() == 'intrachromosomal') and (junction.metaclusterA.bkpPos > junction.metaclusterB.bkpPos): 
            clusterA = junction.metaclusterB
            clusterB = junction.metaclusterA
        else:
            clusterA = junction.metaclusterA
            clusterB = junction.metaclusterB

        # tmp: bkp annotation for all SVs. Interval annot should be used for DUP and DEL instead
        # cluster A
        ref = clusterA.ref
        beg = clusterA.bkpPos
        name = ref + ':' + str(beg) + '-' + str(beg) + '-' + junction.svType + '_A'
        fields = [ref, str(beg), str(beg), '0', '0', 'comments: ' + name]
        row = "\t".join(fields)
        outFile.write(row + '\n')
        
        # clusterB
        ref = clusterB.ref
        beg = clusterB.bkpPos
        name = ref + ':' + str(beg) + '-' + str(beg) + '-' + junction.svType + '_B'
        fields = [ref, str(beg), str(beg), '0', '0', 'comments: ' + name]
        row = "\t".join(fields)
        outFile.write(row + '\n')

        # if junction.annot_bkp:
        #     # cluster A
        #     ref = clusterA.ref
        #     beg = clusterA.bkpPos
        #     name = ref + ':' + str(beg) + '-' + str(beg) + '-' + junction.svType + '_A'
        #     fields = [ref, str(beg), str(beg), '0', '0', 'comments: ' + name]
        #     row = "\t".join(fields)
        #     outFile.write(row + '\n')
            
        #     # clusterB
        #     ref = clusterB.ref
        #     beg = clusterB.bkpPos
        #     name = ref + ':' + str(beg) + '-' + str(beg) + '-' + junction.svType + '_B'
        #     fields = [ref, str(beg), str(beg), '0', '0', 'comments: ' + name]
        #     row = "\t".join(fields)
        #     outFile.write(row + '\n')

        # else:
        #     # interval annot
        #     ref = clusterA.ref
        #     beg = clusterA.bkpPos
        #     end = clusterB.bkpPos
        #     name = ref + ':' + str(beg) + '-' + str(end) + '-' + junction.svType
        #     fields = [ref, str(beg), str(end), '0', '0', 'comments: ' + name]
        #     row = "\t".join(fields)
        #     outFile.write(row + '\n')
    
    outFile.close()

    ## 3. Annotate events intervals with ANNOVAR 
    ## 2. Create input file containing events intervals for ANNOVAR 
    out1, out2 = annotation.run_annovar(outPath, annovarDir, outDir)

    ## 4. Add gene annotation info to the events
    addGnAnnot2events(junctions, out1)
    
    ## Do cleanup
    unix.rm([outDir, out1, out2])

def addGnAnnot2events(junctions, out1):
    '''
    Read annovar output file and incorporate gene annotation information to the corresponding event objects

    Input: 
        1. events: List containing input events to be annotated. Events should be objects containing ref, beg and end attributes.
        2. out1: Annovar output file 1 (region annotation for all the variants) 

    Output:
        New 'geneAnnot' attribute set for each input event. 
        'geneAnnot' is a tuple(region,gene) 
    '''
    ## 1. Organize events into a dict
    eventsDict = {}

    ## 2. Write events
    for junction in junctions:
        
        # set clusters A and B
        if (junction.junctionType() == 'intrachromosomal') and (junction.metaclusterA.bkpPos > junction.metaclusterB.bkpPos): 
            clusterA = junction.metaclusterB
            clusterB = junction.metaclusterA

        else:
            clusterA = junction.metaclusterA
            clusterB = junction.metaclusterB
        
        # tmp: bkp annotation for all SVs. Interval annot should be used for DUP and DEL instead
        # cluster A
        ref = clusterA.ref
        beg = clusterA.bkpPos
        name = ref + ':' + str(beg) + '-' + str(beg) + '-' + junction.svType + '_A'
        eventsDict[name] = junction
        
        # clusterB
        ref = clusterB.ref
        beg = clusterB.bkpPos
        name = ref + ':' + str(beg) + '-' + str(beg) + '-' + junction.svType + '_B'
        eventsDict[name] = junction

        # if junction.annot_bkp:
        #     # cluster A
        #     ref = clusterA.ref
        #     beg = clusterA.bkpPos
        #     name = ref + ':' + str(beg) + '-' + str(beg) + '-' + junction.svType + '_A'
        #     eventsDict[name] = junction
            
        #     # clusterB
        #     ref = clusterB.ref
        #     beg = clusterB.bkpPos
        #     name = ref + ':' + str(beg) + '-' + str(beg) + '-' + junction.svType + '_B'
        #     eventsDict[name] = junction


        # else:
        #     # interval annot
        #     ref = clusterA.ref
        #     beg = clusterA.bkpPos
        #     end = clusterB.bkpPos
        #     name = ref + ':' + str(beg) + '-' + str(end) + '-' + junction.svType
        #     eventsDict[name] = junction

    ## 2. Add to each event gene annotation info    
    out1File = open(out1, "r")

    # Read line by line adding the relevant info to the corresponding event in each iteration
    for line in out1File:
        fields = line.split()
        region = fields[0]
        gene = fields[1]
        name = fields[8]

        if '_A' in name:     
            eventsDict[name].metaclusterA.geneAnnot = (region, gene)
        elif '_B' in name:
            eventsDict[name].metaclusterB.geneAnnot = (region, gene)
        else:
            eventsDict[name].metaclusterA.geneAnnot = (region, gene)

