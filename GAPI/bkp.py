'''
Module to solve the bkp 
'''

## External
import operator
import itertools

## Internal
from GAPI import formats
from GAPI import assembly
from GAPI import alignment
from GAPI import events
from GAPI import bamtools


def bkp(metaclusters):
    '''
    Determine breakpoints
    
    Input: 
    1. List of metaclusters
    
    Output:
    There is no output, but refLeftBkp and refRightBkp attributes are filled
    '''
    for metacluster in metaclusters:
        
        leftBkps = []
        rightBkps = []

        # Choose bkp with highest number of clipping events
        for event in metacluster.events:
            
            if event.type == 'CLIPPING':
                
                if event.clippedSide == 'left':
                    leftBkps.append(event.beg)
                    
                elif event.clippedSide == 'right':
                    rightBkps.append(event.beg)

        # Fill refLeftBkp and refRightBkp attributes
        if len(leftBkps) > 0:
            leftBkp = max(set(leftBkps), key=leftBkps.count)
            metacluster.refLeftBkp = leftBkp
            
        if len(rightBkps) > 0:
            rightBkp = max(set(rightBkps), key=rightBkps.count)
            metacluster.refRightBkp = rightBkp
