'''
Module 'assembly' - Contains funtions to create contigs from a set of input sequences
'''

## DEPENDENCIES ##
# External
import subprocess
import os
import pysam

# Internal
from GAPI import log
from GAPI import unix
from GAPI import alignment
from GAPI import formats


## FUNCTIONS ##
    
def assemble_overlap(fastaA, fastaB, technology, outDir):
    '''
    Assemble two sets sequences via the identification of reciprocal overlap at the sequence ends. 
    (custom algorithm based on minimap2 alignments)

     -: aligned; _: clipped      sequence_bkp
    sequence A:     -------------------*_____________________
    sequence B:                                    |________|______________*-------------------
    assembled:      ---------------------------------------------------------------------------

    Input:
        1. fastaA: FASTA file containing set of sequences A
        2. fastaB: FASTA file containing set of sequences B
        3. technology: sequencing technology (NANOPORE, PACBIO or ILLUMINA)
        4. outDir: output directory

    Output:
        1. contigFile: FASTA file containing the generated contig or 'None' if no reciprocal overlap is found 
    '''    
    ## 0. Create directories ##
    logDir = outDir + '/Logs'
    unix.mkdir(logDir)

    ## 1. Align sequences A vs B ##
    ## Set preset according to the technology
    preset = alignment.minimap2_presets(technology)
    
    ## Do alignment 
    PAF_file = outDir + '/overlaps.paf'
    err = open(logDir + '/minimap2.err', 'w') 
    command = 'minimap2 -x ' + preset + ' ' + fastaB + ' ' + fastaA + ' > ' + PAF_file

    status = subprocess.call(command, stderr=err, shell=True)

    if status != 0:
        step = 'ASSEMBLE-OVERLAP'
        msg = 'Sequences A vs B alignment failed' 
        log.step(step, msg)

    ## 2. Read PAF alignments ##
    PAF = formats.PAF()
    PAF.read(PAF_file)

    # Exit function if no hit found
    if not PAF.alignments:
        return None

    ## 3. Filter overlaps 
    # Filtering criteria:
    # Pick alignments on the + strand 
    # Pick alignments starting within 250 from sequences ends. 
    filtered = []

    for overlap in PAF.alignments:

        # Compute the distance between the corresponding sequence end and the overlap begin or end position
        distA = overlap.qLen - overlap.qEnd 
        distB = overlap.tBeg
    
        # Apply filters
        if (overlap.strand == '+') and (distA <= 250) and (distB <= 250):
            filtered.append(overlap)

    # Replace raw by filtered overlaps
    PAF.alignments = filtered

    # Exit function if all the overlapping hits have been filtered
    if not PAF.alignments:
        return None

    ## 4. Pick longest overlap passing the filters
    # Sort PAF in decreasing overlap lengths
    PAF.alignments = PAF.sortByLen()

    # Pick longest overlap
    overlapLongest = PAF.alignments[0]

    ## 5. Concatenate overlapping sequences to generate a contig 
    ## Read input fasta files
    sequencesA = formats.FASTA()
    sequencesA.read(fastaA)
    
    sequencesB = formats.FASTA()
    sequencesB.read(fastaB)
    
    ## Contig generation  
    #  -: aligned; _: clipped                     sequence_bkp    qBeg       qEnd
    # sequence A (query):                ---------------*___________|_________|___      sequence_bkp
    # sequence B (template):                                    ____|_________|______________*-------------------
    #                                                             tBeg       tEnd
    # contig:                            -------------------------------------|----------------------------------
    #                                                  sequence_1        :qEnd tEnd:       sequence_2
    seqA = sequencesA.seqDict[overlapLongest.qName][:overlapLongest.qEnd]
    seqB = sequencesB.seqDict[overlapLongest.tName][overlapLongest.tEnd:]
    contigSeq = seqA + seqB 

    ## Write contig into fasta
    contig = formats.FASTA()
    contig.seqDict['CONTIG'] = contigSeq

    contigFile = outDir + '/contig.fa'
    contig.write(contigFile)

    return contigFile

def cons_wtdbg2(fastaPath, outDir, processes, preset):
    '''
    Wrapper to generate wtdbg2 consensus sequence from fasta file
    '''
    # change preset argument as indicated by wtdbg2 for small genomes 
    if preset in ["ont", "sq"] : preset = "preset2"
    
    err_wtdbg2 = open(outDir + '/wtdbg2.err', 'w')
    # wtdbg2 doesn't ensure reproducibility. Depends on threading. https://github.com/ruanjue/wtdbg2/issues/145
    # cmd_wtdbg2 = 'wtdbg2 -l 256 -e 1 -S 1 --rescue-low-cov-edges --node-len 256 --ctg-min-length 256 --ctg-min-nodes 1 -t ' + str(processes) + ' -i ' + fastaPath + ' -fo ' + outDir + '/assembly'
    cmd_wtdbg2 = 'wtdbg2 -x ' + preset + ' -R -L 200 -l 256 -e 1 -S 1 --rescue-low-cov-edges --node-len 256 --ctg-min-length 256 --ctg-min-nodes 1 -t ' + str(processes) + ' -i ' + fastaPath + ' -fo ' + outDir + '/assembly'
    status_wtdbg2 = subprocess.call(cmd_wtdbg2, stderr=err_wtdbg2, shell=True)

    if status_wtdbg2 != 0:
        step = 'Meta-cluster consensus'
        msg = 'wtdbg2 step 1 failed' + cmd_wtdbg2
        log.step(step, msg)
        return None

    err_cns = open(outDir + '/wtdbg2_cns.err', 'w')
    cmd_cns = 'wtpoa-cns -t ' + str(processes) + ' -i ' + outDir + '/assembly.ctg.lay.gz -fo ' + outDir + '/consensus.ctg.fa'
    status_cns = subprocess.call(cmd_cns, stderr=err_cns, shell=True)

    if status_cns != 0:
        step = 'Meta-cluster consensus'
        msg = 'wtdbg2 step 2 failed' + cmd_cns
        log.step(step, msg)
        return None

    return outDir + '/consensus.ctg.fa'
    
def polish_racon(templates, sequences, technology, nbRounds, outDir):
    '''
    Use a collection of sequences to polish a set of target sequences (i.e. assembled contig) 
    
    Input:
        1. templates: FASTA file containing sequences to be polished
        2. sequences: FASTA file containing set of sequences used to polish the templates
        3. technology: sequencing technology (NANOPORE, PACBIO or ILLUMINA)
        4. nbRounds: number of polishing rounds to be performed 
        5. outDir: output directory
        
    Output:
        1. polished: FASTA file containing polished sequences or 'None' if pipeline fails at any step
    '''
    ## Create logs directory:
    logDir = outDir + '/Logs'
    unix.mkdir(logDir)

    ## Set preset according to the technology
    preset = alignment.minimap2_presets(technology)

    ## Start with polished set as None
    polished = None            

    ## For each polishing round
    for roundId in range(1, nbRounds + 1):
        
        ## 1. Align reads against the template ##
        PAF = outDir + '/alignments_' + str(roundId) + '.paf'
        err = open(logDir + '/minimap2_' + str(roundId) + '.err', 'w') 
        command = 'minimap2 -x ' + preset + ' ' + templates + ' ' + sequences + ' > ' + PAF
        status = subprocess.call(command, stderr=err, shell=True)

        if status != 0:
            step = 'POLISH-RACON'
            msg = 'Alignment of sequences against template failed' 
            log.step(step, msg)

            polished = None            
            break

        ## 2. Template polishing with racon ##
        polished = outDir + '/polished_' + str(roundId) + '.fa'
        err = open(logDir + '/racon_' + str(roundId) + '.err', 'w') 
        command = 'racon --include-unpolished ' + sequences + ' ' + PAF + ' ' + templates + ' > ' + polished
        status = subprocess.call(command, stderr=err, shell=True)

        if status != 0:
            step = 'POLISH-RACON'
            msg = 'Template polishing failed' + '_' + command
            log.step(step, msg)

            polished = None            
            break

        ## 3. Set polished as templates prior attempting a new polishing round
        templates = polished

    return polished


def getConsensusSeq(FASTA_file, outDir):
    '''
    Build consensus seq from fasta file

    Input:
        1. FASTA_file: Fasta file
        2. outDir
    Output:
        1. consensusPath: consensus file
        2. consensusSeq: consensus sequence
    '''

    # 1. Check that the fasta file is not empty:
    if not os.stat(FASTA_file).st_size == 0:
        
        # 2. Make multiple sequence alignment
        msfPath = FASTA_file.replace("fa", "msf")
        # TODO: Add muscle to environment
        err = open(outDir + '/muscle.err', 'w') 
        command = 'muscle -in ' + FASTA_file + ' -out ' + msfPath + ' -msf'

        status = subprocess.call(command, stderr=err, shell=True)

        if status != 0:
            step = 'MUSCLE'
            msg = 'Muscle alignment failed' 
            log.step(step, msg)

        # 3. Generate consensus sequence (cons tool from EMBOSS package)
        consensusPath = FASTA_file.replace("_supportingReads", "_consensus")
        # TODO: Add emboss to environment
        err = open(outDir + '/cons.err', 'w') 
        command = 'cons -sequence ' + msfPath + ' -outseq ' + consensusPath + ' -identity 0 -plurality 0'
        status = subprocess.call(command, stderr=err, shell=True)

        if status != 0:
            step = 'CONS'
            msg = 'Cons failed' 
            log.step(step, msg)

        # 4. Check that consensus file exists and is not empty
        if os.path.exists(consensusPath) and not os.stat(consensusPath).st_size == 0:
            
            # 5. Read consensus sequence 
            consensusFastaObj = formats.FASTA()
            consensusFastaObj.read(consensusPath)
            consensusSeq = consensusFastaObj.seqDict["EMBOSS_001"].upper()

            # 6. Replace '-' by 'N' for ambiguous bases:
            consensusSeq = consensusSeq.replace('-', 'N')

            # 7. Convert consensus sequence into upper case:
            consensusSeq = consensusSeq.upper()
            
        else:
            consensusSeq = None

    return consensusPath, consensusSeq


def extractReadSeq_4canu(metaclusters, bam, outDir, buffer = 5, delta = 10000):
    '''
    Extract sequence (+/- 10 kb) from metacluster supporting reads and write then 
    to fasta files. Input for Canu alternative assembly

    Input:
        1. metaclusters: list of metaclusters
        2. bam: 
        2. outDir
    Output:
        1. consensusPath: consensus file
        2. consensusSeq: consensus sequence
    '''
    # open bam file
    bamFile = pysam.AlignmentFile(bam, "rb" )
    
    # loop over metaclusters
    for metacluster in metaclusters:
        
        # define supporting reads list
        suppReads = list(set(metacluster.reads))
        
        # define bkps
        chr = metacluster.ref
        bkp = metacluster.median_pos()[0]
        start = bkp - buffer
        end = bkp + buffer
        
        # create output dir
        outPath = outDir + '/alt_assemblies/' + str(chr) + '_' + str(bkp)
        os.makedirs(outPath, exist_ok = True)
        outFile = str(outPath) + '/alignmentsRegion.fa'
        
        with open(outFile, "w") as fasta:
            
            # create pileup object
            for pileupcolumn in bamFile.pileup(chr, start=start, end=end):
                
                # if pileup position corresponds to start or end
                if pileupcolumn.pos in [start, end]:
                    
                    # for every read in position
                    for pileupread in pileupcolumn.pileups:
                        
                        # if read belongs to metacluster supporting reads
                        if pileupread.alignment.query_name in suppReads:
                            
                            # get bkp position relative to read sequence
                            if pileupread.query_position:
                                
                                readBkp = int(pileupread.query_position)
                                lenRead = len(pileupread.alignment.query_sequence)
                                
                                startBkp = readBkp - delta if readBkp - delta > 0 else 0
                                endBkp = readBkp + delta if readBkp + delta < lenRead else lenRead
                                
                                readName = pileupread.alignment.query_name
                                sequence = pileupread.alignment.query_sequence[int(startBkp):int(endBkp)]
                        
                                fasta.write(">%s\n" % readName)
                                fasta.write("%s\n" % sequence)     
                                
                                # drop read from suppReads list
                                suppReads.remove(pileupread.alignment.query_name)       

    # close bam file
    bamFile.close()