'''
Module 'clusters' - Contains classes for dealing with genomic variation
'''

## DEPENDENCIES ##
# External
import sys
import multiprocessing as mp
import numpy as np
import pysam
import math
import itertools
import os
from operator import itemgetter
from collections import Counter
import scipy
import uuid

# Internal
from GAPI import log
from GAPI import formats
from GAPI import unix
from GAPI import clustering
from GAPI import events
from GAPI import structures
from GAPI import alignment
from GAPI import bamtools 
from GAPI import assembly
from GAPI import retrotransposons
from GAPI import filters
from GAPI import bkp
from GAPI import annotation
from GAPI import sequences
from GAPI import gRanges
from GAPI import stats
from GAPI import depth

###############
## FUNCTIONS ##
###############

def create_cluster(events, clusterType):
    '''
    Function to create a cluster object instance

    Input:
        1. events: list of events/clusters that will compose the cluster
        2. clusterType: type of cluster (META: metacluster; INS: insertion; DEL: deletion; CLIPPING: clipping; DISCORDANT: discordant paired-end)

    Output:
        1. cluster: cluster object instance
    '''
    cluster = ''

    ## a) Create META cluster
    # Note: events should correspond to a list of clusters 
    if (clusterType == 'META'):
        cluster = META_cluster(events)

    ## b) Create INS cluster
    elif (clusterType == 'INS'):
        cluster = INS_cluster(events)

    ## c) Create DEL cluster
    elif (clusterType == 'DEL'):
        cluster = DEL_cluster(events)

    ## d) Create CLIPPING cluster
    elif (clusterType == 'CLIPPING') or (clusterType == 'LEFT-CLIPPING') or (clusterType == 'RIGHT-CLIPPING'):
        cluster = CLIPPING_cluster(events)

    ## e) Create SUPPLEMENTARY cluster 
    elif 'SUPPLEMENTARY' in clusterType:
        cluster = SUPPLEMENTARY_cluster(events)

    ## f) Create INS_VCF cluster
    elif 'INS_VCF' in clusterType:
        cluster = formats.INS_cluster(events)

    ## g) Unexpected cluster type
    else:
        log.info('Error at \'create_cluster\'. Unexpected cluster type')
        sys.exit(1)

    return cluster


def merge_clusters(clusters, clusterType):
    '''
    Merge a set of clusters/metaclusters into a single cluster/metacluster instance

    Input:
        1. clusters: list of clusters/metaclusters that will be merged
        2. clusterType: type of cluster to be merged (INS: insertion; DEL: deletion; CLIPPING: clipping; META: metacluster)

    Output:
        1. cluster: merged cluster/metacluster instance
    '''
    # A) Merge metaclusters
    if clusterType == 'META':
        subclusters = []
         
        for metacluster in clusters:
            # NOTE MERGE SR2020: To avoid key META error in clustering.reciprocal_overlap_clustering
            #subclusters = subclusters + list(metacluster.subclusters.values())
            subclusters = subclusters + list(metacluster.rawSubclusters)

        mergedCluster = create_cluster(subclusters, clusterType)

        # NOTE MERGE SR2020: To avoid key META error in clustering.reciprocal_overlap_clustering
        for metacluster in clusters:
            for clusterNew in metacluster.rawSubclusters:
                clusterNew.clusterId = mergedCluster.id

    # B) Merge standard clusters
    else:

        events = []
        for cluster in clusters:
            events = events + cluster.events

        mergedCluster = create_cluster(events, clusterType)

    return mergedCluster


def cluster_by_matePos(discordants, refLengths, minClusterSize):
    '''
    Apply an extra clustering step to discordant read pair clusters based on mate position

    Input:
        1. discordants: list of discordant clusters
        2. refLengths: dictionary containing references as keys and their lengths as values
        3. minClusterSize: minimum cluster size

    Output:
        1. outDiscordants: list of discordant clusters after applying mate position based clustering
    '''     
    outDiscordants = []

    ## For each cluster
    for cluster in discordants:

        ## Cluster by mate position
        newClusters = cluster_events_by_matePos(cluster.events, refLengths, minClusterSize)
            
        ## Add newly created clusters to the list
        outDiscordants = outDiscordants + newClusters

    return outDiscordants

def cluster_by_supplPos(clippings, refLengths, minClusterSize, clippingSide):
    '''
    Apply an extra clustering step to clipping clusters based on suppl. alignment position

    Input:
        1. clippings: list of clipping clusters
        2. refLengths: dictionary containing references as keys and their lengths as values
        3. minClusterSize: minimum cluster size
        4. clippingSide: 'LEFT-CLIPPING' or 'RIGHT-CLIPPING'

    Output:
        1. outClippings: list of clipping clusters after applying supplementary alignment position based clustering
    '''     
    outClippings = []

    ## For each cluster
    for cluster in clippings:

        ## Perform extra clustering based on suppl. alignment positions
        newClusters = cluster_events_by_supplPos(cluster, refLengths, minClusterSize, clippingSide)
            
        ## Add newly created clusters to the list
        outClippings = outClippings + newClusters

    return outClippings

def cluster_events_by_supplPos(clippingCluster, refLengths, minClusterSize, clippingSide):
    '''
    Cluster events in an input clipping cluster according to the position of their supplementary alignments
    
    Input:
        1. clippingCluster: clipping cluster
        2. refLengths: dictionary containing references as keys and their lengths as values
        3. minClusterSize: minimum cluster size
        4. clippingSide: 'LEFT-CLIPPING' or 'RIGHT-CLIPPING'

    Output:
        1. clippingClusters: list of clipping clusters
    '''   
    ## 1. Organize clippings at the cluster into a dictionary 
    clippingsDict = {}

    for clipping in clippingCluster.events:
        clippingsDict[clipping.id] = clipping
    
    ## 2. Retrieve complementary suppl. alignments for each clipping event 
    complementaryAlignments = clippingCluster.search4complAlignments()

    ## 3. Cluster supplementary alignment positions
    supplClusters = clippingCluster.cluster_suppl_positions(complementaryAlignments)
    
    ## 4. Create new clipping clusters of discordants based on mate clusters
    clippingClusters = []

    supplClusters = structures.dict2list(supplClusters)
 
    # For each mate cluster in the reference
    for supplCluster in supplClusters:
        
        # Retrieve original discordants for mates
        clippings = [clippingsDict[supplAlign.clippingId] for supplAlign in supplCluster.events]

        # Create cluster
        clippingCluster = create_cluster(clippings, clippingSide)

        # Add supplementary alignments cluster to the clipping cluster
        clippingCluster.supplCluster = supplCluster

        # Add clipping cluster to the list
        clippingClusters.append(clippingCluster)

    return clippingClusters

def cluster_events_by_matePos(discordants, refLengths, minClusterSize):
    '''
    Cluster discordant read pair events based on their mate alignment position

    Input:
        1. discordants: list of discordant events
        2. refLengths: dictionary containing references as keys and their lengths as values
        3. minClusterSize: minimum cluster size

    Output:
        1. discordantClusters: list of discordant clusters
    '''   
    ## 1. Organize discordant into a dictionary according to supporting read id
    discordantsDict = {}

    for discordant in discordants:
        discordantsDict[discordant.fullReadName()] = discordant
    
    ## 2. Produce discordant objects for mates:
    mates = events.discordants2mates(discordants)

    ## 3. Organize mates into a bin database prior clustering
    ## Create dictionary containing mates
    matesDict = events.events2nestedDict(mates, 'DISCORDANT_MATE')

    ## Create bin database 
    matesBinDb = structures.create_bin_database(refLengths, matesDict)

    ## 4. Cluster mates according to their alignment positions
    mateClusters = []

    # For each reference
    for ref in matesBinDb:
        binDb = matesBinDb[ref]
        binLevel = binDb.binSizes[0]
        # clusters = clustering.distance_clustering(binDb, binLevel, ['DISCORDANT_MATE'], 'DISCORDANT', binLevel, minClusterSize)
        clusters = clustering.distance_clustering(binDb, binLevel, ['DISCORDANT_MATE'], 'DISCORDANT', 500, minClusterSize)
        mateClusters = mateClusters + clusters
    
    ## 5. Make clusters of discordants based on mate clusters
    discordantClusters = []

    # For each mate cluster in the reference
    for mateCluster in mateClusters:
        
        # Retrieve original discordants for mates
        discordants = [discordantsDict[mate.fullReadName_mate()] for mate in mateCluster.events]

        # Create cluster
        discordantCluster = create_cluster(discordants, 'DISCORDANT')

        # Add group to the list
        discordantClusters.append(discordantCluster)

    return discordantClusters

def create_clusters(eventsBinDb, confDict):
    '''
    Group SV events into distinct clusters according to their SV type
    
    Input:
        1. eventsBinDb: Data structure containing a set of events organized in genomic bins
        2. confDict: 
            * maxInsDist: Maximum distance bewteen two adjacent INS to be clustered together
            * maxBkpDist: Maximum distance bewteen two adjacent breakpoints for CLIPPING clustering    
            * minClusterSize: minimum number of reads composing a root cluster
            * minPercOverlap: minimum percentage of reciprocal overlap for DEL clustering 

    Output:
        1. clustersBinDb: bin database structure containing SV clusters
    ''' 

    ## 1. Create clusters ##
    clustersDict = {}

    ## For each event type, group events into clusters 
    for SV_type in eventsBinDb.eventTypes:

        ## A) Perfom clustering for INS events
        if SV_type == 'INS':

            binLevel = eventsBinDb.binSizes[0]
            clustersDict[SV_type] = clustering.distance_clustering(eventsBinDb, binLevel, [SV_type], SV_type, confDict['maxInsDist'], confDict['minClusterSize'])      
            clustersDict[SV_type] = [ins_cluster for ins_cluster in clustersDict[SV_type] if filters.ins_cluster_sdLen(ins_cluster, confDict['minClusterSize'])]

        ## B) Perform clustering for CLIPPING events
        elif SV_type in ['CLIPPING', 'RIGHT-CLIPPING', 'LEFT-CLIPPING']:
            
            binLevel = eventsBinDb.binSizes[0]            
            clustersDict[SV_type] = clustering.distance_clustering(eventsBinDb, binLevel, [SV_type], SV_type, confDict['maxBkpDist'], confDict['minClusterSize'])      

        ## C) Perform clustering based on reciprocal overlap for DEL 
        elif SV_type == 'DEL':

            clustersDict[SV_type] = clustering.reciprocal_overlap_clustering(eventsBinDb, 20, confDict['minClusterSize'], [SV_type], 0, SV_type)

        ## D) Perform clustering based on reciprocal overlap for DISCORDANT
        elif SV_type == 'DISCORDANT':

            clustersDict[SV_type] = clustering.reciprocal_overlap_clustering(eventsBinDb, 1, confDict['minClusterSize'], [SV_type], confDict['equalOrientBuffer'], SV_type)    

    ## 2. Organize clusters into bins ##    
    binSizes = [100, 1000, 10000, 100000, 1000000]
    clustersBinDb = structures.create_bin_database_interval(eventsBinDb.ref, eventsBinDb.beg, eventsBinDb.end, clustersDict, binSizes)

    return clustersBinDb


def polish_clusters(clustersBinDb, minClusterSize, genotyper):
    '''
    Apply set of steps for refining clusters of SV events

    Input:
        1. clustersBinDb: Data structure containing a set of clusters organized in genomic bins  
        2. minClusterSize: minimum cluster size
        3. genotyper: True if genotyper mode on

    Output: 
        It does not return variables as output. Just modify the bin database
    '''
    ## 1. Polish INS clusters
    if 'INS' in clustersBinDb.eventTypes:
        
        # Collect all clusters
        INS_clusters = clustersBinDb.collect(['INS'])

        # Correct INS fragmentation
        for cluster in INS_clusters:    

            cluster.correct_fragmentation()
        
        # if genotyper mode, don't run - it decreases sensibility
        if not genotyper:
            
            # Search for subclusters and remove outliers
            for cluster in INS_clusters:

                subclusters = cluster.identify_subclusters(minClusterSize)
                
                # Replace cluster by newly created subclusters in the bin database
                if subclusters:

                    clustersBinDb.remove([cluster], 'INS')
                    clustersBinDb.add(subclusters, 'INS')

    ## 2. Polish DEL clusters (TO DO) 

    ## 3. Polish CLIPPING clusters (TO DO)


def create_metaclusters(clustersBinDb, buffer = 200):
    '''    
    Group SV cluster events into metaclusters

    Input:
        1. clustersBinDb: Data structure containing a set of clusters organized in genomic bins
        2. buffer: buffer to extend cluster coordinates

    Output:
        1. metaclusters: list containing newly created metaclusters
    '''
    metaclusters = clustering.reciprocal_overlap_clustering(clustersBinDb, 1, 1, clustersBinDb.eventTypes, buffer, 'META')

    return metaclusters


def create_metaclusters_distanceClustering(clustersBinDb, binSize, buffer):
    '''    
    Group SV cluster events into metaclusters

    Input:
        1. clustersBinDb: Data structure containing a set of clusters organized in genomic bins
        2. buffer: buffer to extend cluster coordinates

    Output:
        1. metaclusters: list containing newly created metaclusters
    '''
    metaclusters = clustering.distance_clustering(clustersBinDb, binSize, clustersBinDb.eventTypes, 'META', buffer, 1)
    
    return metaclusters

def SV_type_metaclusters(metaclusters, minINDELlen, technology, rootOutDir, confDict, complAlignments = True):
    '''
    Infer the SV type supported by each metacluster

    Input:
        1. metaclusters: list of metaclusters
        2. minINDELlen: minimum INS and DEL lenght
        3. technology: sequencing technology (NANOPORE, PACBIO or ILLUMINA)
        4. rootOutDir: root output directory

    Output:
        1. metaclustersSVType: dictionary containing one key per SV type and the list of metaclusters identified as value
    '''
    metaclustersSVType = {}

    for metacluster in metaclusters:

        metaInterval = '_'.join([str(metacluster.ref), str(metacluster.beg), str(metacluster.end)])
        outDir = rootOutDir + '/' + metaInterval
        metacluster.determine_SV_type(minINDELlen, technology, outDir)
        
        #) Split metacluster if neeeded
        new_metaclusters = [metacluster]
        subClusterTypes = list(metacluster.subclusters.keys())
        
        ## A. INS and DEL events together, split clusters
        if (metacluster.SV_type == 'INS') and (set(subClusterTypes) == {'INS', 'RIGHT-CLIPPING'}):

            delEvents = [x for x in metacluster.events if (x.type == 'CLIPPING') and (x.typeC == 'DEL')]
            if delEvents:
                    
                # create metaclusters and filter them by min nb of reads
                new_metaclusters = [ META_cluster([cluster]) for cluster in metacluster.subclusters.values()]
                new_metaclusters = [ metacluster for metacluster in new_metaclusters if filters.filter_min_nb_reads(metacluster, confDict['minReads'], confDict['minNormalReads']) ]
                
                # fill subclusters attrib
                for clusterA in new_metaclusters: 
                    clusterA.determine_SV_type(minINDELlen, technology, outDir)
                    clusterA.create_subclusters()

        ## B. Double bkp BNDs that are not INS
        elif (metacluster.SV_type == 'BND') and ('RIGHT-CLIPPING' in subClusterTypes) and ('LEFT-CLIPPING' in subClusterTypes):

            # clean outliers
            for key, cluster in metacluster.subclusters.items():
                
                if key in ['RIGHT-CLIPPING', 'LEFT-CLIPPING']:
                    cluster.discard_outliers(offset = 3)

            # create metaclusters
            new_metaclusters = [ META_cluster([cluster]) for cluster in metacluster.subclusters.values()]

            # filter metaclusters by min nb of reads
            new_metaclusters = [ metacluster for metacluster in new_metaclusters if filters.filter_min_nb_reads(metacluster, confDict['minReads'], confDict['minNormalReads']) ]
            
            # fill subclusters attrib
            for clusterA in new_metaclusters: 
                clusterA.SV_type = 'BND'
                clusterA.create_subclusters()
             
        for new_metacluster in new_metaclusters:
            
            # get suppClusters for potential BNDs
            if new_metacluster.SV_type == 'BND':
                
                subClusterTypes = list(new_metacluster.subclusters.keys())
                
                ## Retrieve clipping cluster
                # a) Right clipping
                if 'RIGHT-CLIPPING' in subClusterTypes:
                    clippingCluster = new_metacluster.subclusters['RIGHT-CLIPPING']
                    new_metacluster.longestClip = clippingCluster.longest_clipping_seq(offset = 0)
                    new_metacluster.consensusEvent = clippingCluster.consEventBridge()
                    new_metacluster.consensusFasta = formats.FASTA()
                    new_metacluster.consensusFasta.seqDict[new_metacluster.consensusEvent.readName] = new_metacluster.consensusEvent.readSeq
                    new_metacluster.strand = '+'

                # b) Left clipping
                else:
                    clippingCluster = new_metacluster.subclusters['LEFT-CLIPPING']
                    new_metacluster.longestClip = clippingCluster.longest_clipping_seq(offset = 0)
                    new_metacluster.consensusEvent = clippingCluster.consEventBridge()
                    new_metacluster.consensusFasta = formats.FASTA()
                    new_metacluster.consensusFasta.seqDict[new_metacluster.consensusEvent.readName] = new_metacluster.consensusEvent.readSeq
                    new_metacluster.strand = '-'
                    
                ## Determine BND breakpoint position
                new_metacluster.bkpPos = clippingCluster.infer_breakpoint()

                if complAlignments:
                    
                    ## For each clipping event composing the cluster select complementary suppl. alignments
                    complementaryAlignments = clippingCluster.search4complAlignments()

                    ## Cluster supplementary alignment positions
                    new_metacluster.supplClusters =  clippingCluster.cluster_suppl_positions(complementaryAlignments)

            # A) Initialize list containing metaclusters of a given SV type
            if new_metacluster.SV_type not in metaclustersSVType:
                metaclustersSVType[new_metacluster.SV_type] = [new_metacluster]

            # B) Add metacluster to the list        
            else:
                metaclustersSVType[new_metacluster.SV_type].append(new_metacluster)    

    return metaclustersSVType


def create_consensus(metaclusters, confDict, reference, targetSV, rootOutDir):
    '''
    Generate consensus events for a set of metacluster objects

    Input:
        1. metaclusters: Dictionary containing one key per SV type and the list of metaclusters identified as value
        2. confDict: 
            * technology     -> sequencing technology (NANOPORE, PACBIO or ILLUMINA)
            * rounds         -> number of polishing rounds to be attempled. 0 means no polishing
            * targetSV       -> list with target SV (INS: insertion; DEL: deletion; CLIPPING: left and right clippings)
            * minMAPQ        -> minimum mapping quality
            * minCLIPPINGlen -> minimum clipping lenght
            * minINDELlen    -> minimum INS and DEL lenght
            * overhang       -> Number of flanking base pairs around the INDEL events to be collected from the supporting read. If 'None' the complete read sequence will be collected)

        3. reference: Path to reference genome in fasta format    
        4. targetSV: Target SV types to generate consensus
        5. rootOutDir: Root output directory
    ''' 

    ## For each type of SV 
    for SV in targetSV:

        ## Abort if no metacluster from this SV type has been identified
        if SV not in metaclusters:
            continue

        ## For each metacluster
        for metacluster in metaclusters[SV]:
            metaInterval = '_'.join([str(metacluster.ref), str(metacluster.beg), str(metacluster.end)])
            outDir = rootOutDir + '/' + metaInterval

            ## 1. Polish metacluster´s consensus sequence
            metacluster.polish(confDict, reference, outDir)

            ## 2. Obtain consensus metacluster´s event
            metacluster.consensus_event(confDict, reference, 10000, outDir)
            
            if metacluster.consensusEvent is None:
                
                #unix.rm([outDir])
                unix.mkdir(outDir)
                metacluster.consensusFasta = metacluster.consensusFasta_raw
                metacluster.polish(confDict, reference, outDir, assemblyStep = False)
                metacluster.consensus_event(confDict, reference, 10000, outDir)

            ## Cleanup
            unix.rm([outDir])
    

def lighten_up_metaclusters(metaclusters, bam, confDict):
    '''
    Make metacluster objects lighter by removing events and subcluster objects.

    Collect relevant info for downstream analysis as metacluster attributes

    Input:
        1. metaclusters: Dictionary containing one key per SV type and the list of metaclusters identified as value
    '''
    # For each SV type
    for SV_type in metaclusters:
        
        # if INS, fill bkp args
        if SV_type == 'INS': bkp.bkp(metaclusters[SV_type])

        # For each metacluster
        for metacluster in metaclusters[SV_type]:
            
            ## Set some object attributes before lightening up
            # A. Nb of reads
            metacluster.nbTotal, metacluster.nbTumour, metacluster.nbNormal, metacluster.nbINS, metacluster.nbDEL, metacluster.nbCLIPPING, metacluster.nbCLIPPING_left, metacluster.nbCLIPPING_right = metacluster.nbEvents()
            metacluster.nbReadsTotal, metacluster.nbReadsTumour, metacluster.nbReadsNormal, metacluster.reads, metacluster.readsTumour, metacluster.readsNormal = metacluster.supportingReads()

            # B. Depth
            tumour_depthObj = depth.read_depth_caller(bam, 'TUMOUR', confDict)
            tumour_depthSegObj = tumour_depthObj.read_depth_segment(metacluster.ref, metacluster.beg-10, metacluster.end+10)
            metacluster.depth = tumour_depthSegObj.readCount
            metacluster.depth_5end = [metacluster.end, tumour_depthObj.read_depth_segment(metacluster.ref, metacluster.end-2, metacluster.end-1).readCount]
            metacluster.depth_3end = [metacluster.beg, tumour_depthObj.read_depth_segment(metacluster.ref, metacluster.beg+1, metacluster.beg+2).readCount]
            if metacluster.refLeftBkp and metacluster.refRightBkp:
                metacluster.TSD = metacluster.refRightBkp - metacluster.refLeftBkp
            
            # C. Phasing info
            metacluster.HP1_ALL = len(list(set(tumour_depthSegObj.HP1)))
            metacluster.HP1_ALT = len(list(set(tumour_depthSegObj.HP1).intersection(set(metacluster.reads))))
            metacluster.HP2_ALL = len(list(set(tumour_depthSegObj.HP2)))
            metacluster.HP2_ALT = len(list(set(tumour_depthSegObj.HP2).intersection(set(metacluster.reads))))
            metacluster.HPX_ALL = len(list(set(tumour_depthSegObj.HPX)))
            metacluster.HPX_ALT = len(list(set(tumour_depthSegObj.HPX).intersection(set(metacluster.reads))))
            metacluster.PS = tumour_depthSegObj.PS
            
            # D. If INS, CV of length
            if 'INS' in metacluster.subclusters:
                metacluster.cv = metacluster.subclusters['INS'].cv_len()[1]

            metacluster.subclusters = None


def double_clipping_supports_INS(clusterA, clusterB, minINDELlen, technology, outDir):
    '''
    Assess if two clipping clusters support an insertion event.

    Input:
        1. clusterA: clipping cluster A
        2. clusterB: clipping cluster B
        3. minINDELlen: minimum INS and DEL lenght
        4. technology: sequencing technology (NANOPORE, PACBIO or ILLUMINA)
        5. outDir: output file
    
    Output:
        1. boolean: clippings support an INS event (True) or not (False) 
        2. consensusFasta: fasta containing consensus read sequence spanning the insertion event. None if no insertion supporting evidences found 
    '''
    ## Search for chimeric alignments completely spanning the INS fragment
    primary, supplementary, chimeric, percSameStrand = find_chimeric_alignments(clusterA, clusterB)

    ## A) Chimeric alignment found with both pieces aligning in the same orientation
    if (primary is not None) and (percSameStrand >= 75):

        ## Search for inserted sequence at clipped clusters breakpoints
        insert = find_insertion_at_clipping_bkp(primary, supplementary)

        ## a) Inserted sequence longer than threshold
        if len(insert) >= minINDELlen:
            boolean = True            
            consensusFasta = formats.FASTA()
            consensusFasta.seqDict[primary.readName] = primary.readSeq
            
        ## b) No inserted sequence or shorter than threshold
        else:
            boolean = False
            consensusFasta = None

    ## B) Chimeric alignment NOT found -> search for complementary clippings
    else:
        boolean = False
        consensusFasta = None

    return boolean, consensusFasta


def find_chimeric_alignments(clusterA, clusterB):
    '''
    Search for chimeric read alignments connecting two clipping clusters. Select one as consensus if multiple are identified. 

    Input:
        1. clusterA: Clipping cluster object
        2. clusterB: Clipping cluster object

    Output:
        1. primary: clipping event for representative primary alignment
        2. supplementary: clipping event for representative supplementary alignment
        3. chimericSorted: list of tuples. Each tuple is composed by two clipping events corresponding to primary and supplementary alignments, respectively. 
        4. percSameStrand: percentage of chimeric alignments with both fragments having the same orientation
    ''' 
    ### 1. Identify chimeric read alignments connecting both clipping clusters
    chimeric = []
    sameStrand = 0
    oppositeStrand = 0

    # For each clipping event composing cluster A        
    for clippingA in clusterA.events:

        # For each clipping event composing cluster B
        for clippingB in clusterB.events:

            # Clipping events supported by the same read (chimeric alignments)
            if (clippingA.readName == clippingB.readName):
                
                ## Determine if fragments in chimeric alignments have the same or opposite orientations
                # A) Same
                if (clippingA.reverse == clippingB.reverse):
                    sameStrand += 1

                # B) Opposite
                else:
                    oppositeStrand += 1

                ## Determine which clipping is primary and supplementary
                # A) Clipping A primary; clipping B supplementary
                if (clippingA.supplementary == False) and (clippingB.supplementary == True):
                    primary = clippingA
                    supplementary = clippingB
                    chimeric.append((primary, supplementary))

                # B) Clipping A supplementary; clipping B primary
                elif (clippingA.supplementary == True) and (clippingB.supplementary == False):
                    primary = clippingB
                    supplementary = clippingA
                    chimeric.append((primary, supplementary))

                # C) Both clippings supplementary (Discard! These cases are not informative as supplementary alignments are hardclipped so don´t allow to pick the inserted fragment)
                
    # Exit if not chimeric alignments found
    if not chimeric:
        return None, None, None, None

    ### 2. Select the clipping events with the longest supplementary alignment as representative
    chimericSorted = sorted(chimeric, key=lambda alignments: alignments[1].refLen, reverse=True)
    primary = chimericSorted[0][0]
    supplementary = chimericSorted[0][1]

    ### 3. Compute fraction of chimeric alignments with same orientation
    percSameStrand = float(sameStrand) / (sameStrand + oppositeStrand) * 100

    return primary, supplementary, chimericSorted, percSameStrand


def find_insertion_at_clipping_bkp(primary, supplementary):
    '''
    Search for inserted DNA between the two clipping event breakpoints. 

    Input:
        1. primary: Clipping event object corresponding to the primary alignment
        2. supplementary: Clipping event object corresponding to the supplementary alignment

    Output:

        1. insert: DNA fragment inserted between clipping breakpoints
    ''' 
    ### Check if there is an unaligned piece of read sequence between the primary and supplementary alignment breakpoints
    ## A) Primary right clipped 
    # -: aligned; _: clipped            readBkpA
    # primary -----------------------------*______________________
    #                                       <<<<insert>>>>*------- supplementary
    #                                                  readBkpB == total read length -  length of the supplementary piece of sequence aligned
    if (primary.clippedSide == 'right'):
        readBkpA = primary.readBkp
        readBkpB = supplementary.readBkp

    ## B) Primary left clipped
    # -: aligned; _: clipped        readBkpB
    # primary        ______________________*-----------------------------
    # supplementary  -------*<<<<insert>>>>                            
    #               readBkpA == length of the supplementary piece of sequence aligned
    else:
        readBkpA = supplementary.readBkp
        readBkpB = primary.readBkp             

    # Extract inserted sequence between clipping breakpoints
    insert = primary.readSeq[readBkpA:readBkpB] if readBkpA < readBkpB else primary.readSeq[readBkpB:readBkpA]

    return insert


def INS_type_metaclusters(metaclusters, reference, annotations, processes, rootOutDir):
    '''
    For each metacluster provided as input determine the type of insertion

    Input:
        1. metaclusters: list of metaclusters supporting insertion events
        2. reference: Path to the reference genome in fasta format (bwa mem and minimap2 indexes must be located in the same folder)
        3. annotations: Dictionary containing one key per type of annotation loaded and bin databases containing annotated features as values 
        4. processes: Number of processes
        5. rootOutDir: Root output directory
    '''

    # Initialize variables:
    allHits_genome = {}
    groupedEntries = {}

    ## 1. Create fasta containing all consensus inserted sequences 
    msg = '1. Create fasta containing all consensus inserted sequences'
    log.info(msg) 
    fastaPath = insertedSeq2fasta(metaclusters, rootOutDir)

    ## 2. Align consensus inserted sequences
    msg = '2. Align consensus inserted sequences'
    log.info(msg) 

    ## 2.1 Align consensus inserted sequences into the reference genome
    msg = '2.1 Align consensus inserted sequences into the reference genome'
    log.info(msg)    
    SAM_genome = alignment.alignment_bwa(fastaPath, reference, 'alignments_genome', processes, rootOutDir)

    ## Convert SAM to PAF
    PAF_genome = alignment.sam2paf(SAM_genome, 'alignments_genome', rootOutDir)
    
    index = os.path.splitext(reference)[0] + '.mmi'
    PAF_genome = alignment.alignment_minimap2(fastaPath, index, 'alignments_genome', processes, rootOutDir)

    ## Organize hits according to their corresponding metacluster
    allHits_genome = alignment.organize_hits_paf(PAF_genome)
    
    ## 2.2 Align consensus inserted sequences into the reference genome (splicing-aware)
    msg = '2.2 Align consensus inserted sequences into the reference genome (splicing-aware)'
    log.info(msg)    

    ## Minimap index for the reference
    index = os.path.splitext(reference)[0] + '.mmi'
    SAM_splicing = alignment.alignment_minimap2_spliced(fastaPath, index, 'alignments_spliced', processes, rootOutDir)

    ## Convert SAM to BAM
    BAM_splicing = bamtools.SAM2BAM(SAM_splicing, rootOutDir)

    ## Convert BAM to BED
    BED_path = bamtools.BAM2BED(BAM_splicing, rootOutDir)

    ## Organize hits according to their corresponding metacluster
    allHits_splicing = formats.BED()
    allHits_splicing.read(BED_path, 'List', None)
    groupedEntries = allHits_splicing.group_entries_by_name()
    
    ## 3. For each metacluster determine the insertion type
    msg = '3. For each metacluster determine the insertion type'
    log.info(msg)   
    
    # For each metacluster
    for metacluster in metaclusters:

        ## 3.1 Collect consensus inserted sequence hits
        metaId = str(metacluster.ref) + ':' + str(metacluster.beg) + '-' + str(metacluster.end)

        ## Hits in the reference genome
        if metaId in allHits_genome:
            hits_genome = allHits_genome[metaId]
                
        else:
            hits_genome = formats.PAF()

        ## Hits in the reference genome (splice-aware alignment)
        if metaId in groupedEntries:
            hits_splicing = groupedEntries[metaId]

        else:
            hits_splicing = []

        ## 3.2 Insertion type inference
        metacluster.determine_INS_type(hits_genome, hits_splicing, annotations['RETROTRANSPOSONS'], annotations['TRANSDUCTIONS'], annotations['EXONS'])

def structure_inference_parallel(metaclusters, consensusPath, transducedPath, transducedSomaticPath, processes, rootDir):
    '''
    Infer structure for a list of INS metacluster objects. Parallelize by distributing metaclusters by processes. 

    Input:
        1. metaclusters: list of metacluster objects
        2. consensusPath: path to fasta file containing retrotransposon consensus sequences
        3. transducedPath: path to fasta containing transduced sequences downstream of source elements
        4. transductionSearch: boolean specifying if transduction search is enabled (True) or not (False)
        5. processes: number of processes
        6. rootDir: Root output directory
    
    Output:
        1. metaclustersOut: list of metacluster objects with structure information stored at 'SV_features' dict attribute
    '''
    ## 1. Create tuple list for multiprocessing
    msg = '1. Create tuple list for multiprocessing'
    log.subHeader(msg)      
    metaclustersOut = []
    tupleList = []

    for metacluster in metaclusters:
        
        ## Skip structure inference if insertion type not available or not solo, partnered or orphan transduction
        # Note: investigate why INS_TYPE is not defined in some metaclusters
        if ('INS_TYPE' not in metacluster.SV_features) or (metacluster.SV_features['INS_TYPE'] not in ['solo', 'partnered', 'orphan']):
            metaclustersOut.append(metacluster) 
            continue

        ## Add to the list
        fields = (metacluster, consensusPath, transducedPath, transducedSomaticPath, rootDir)
        tupleList.append(fields)

    ## 2. Infer structure
    msg = '2. Infer structure'
    log.subHeader(msg)       

    pool = mp.Pool(processes=processes)
    metaclustersStructure = pool.starmap(structure_inference, tupleList)
    pool.close()
    pool.join()
      
    ## 3. Create final list with metaclusters to be reported as output
    msg = '3. Create final list with metaclusters to be reported as output'
    metaclustersOut = metaclustersOut + metaclustersStructure 

    return metaclustersOut

def structure_inference(metacluster, consensusPath, transducedPath, transducedSomaticPath, rootDir):
    '''
    Wrapper to call 'determine_INS_structure' method for a given INS metacluster provided as input

    Input:
        1. metacluster: INS metacluster 
        2. consensusPath: path to fasta file containing retrotransposon consensus sequences
        3. transducedPath: path to fasta containing transduced sequences downstream of source elements
        4. transducedSomaticPath: path to fasta containing transduced sequences downstream of somatic source elements
        5. rootDir: Root output directory
    
    Output:
        1. metacluster: INS metacluster containing structural properties  
    '''
    # Create output directory
    metaInterval = '_'.join([str(metacluster.ref), str(metacluster.beg), str(metacluster.end)])
    outDir = rootDir + '/' + metaInterval
    unix.mkdir(outDir)

    # Infer structure
    structure = metacluster.determine_INS_structure(consensusPath, transducedPath, transducedSomaticPath, outDir)

    # Add structure info to the metacluster
    metacluster.SV_features.update(structure) 

    if 'HITS' in structure:
        metacluster.SV_features['HITS'] = structure['HITS']
    
    # Remove output directory
    unix.rm([outDir])

    return metacluster

def insertedSeq2fasta(metaclusters, outDir):
    '''
    Collect all the consensus inserted sequences from a list of metaclusters supporting INS and 
    generate fasta file containing them 

    Input:
        1. metaclusters: list of metaclusters
        2. outDir: output directory

    Output:
        1. fastaPath: fasta file containing all the consensus inserted sequences
    '''
    ## Initiate FASTA object
    FASTA = formats.FASTA()

    # For each metacluster
    for metacluster in metaclusters:

        ## Skip insertion type inference if consensus event not available
        if metacluster.consensusEvent is None:
            continue               

        ## Retrieve inserted sequence and add to the FASTA
        metaclusterId = metacluster.ref + ':' + str(metacluster.beg) + '-' + str(metacluster.end)
        insert = metacluster.consensusEvent.pick_insert()
        FASTA.seqDict[metaclusterId] = insert
        
    ## Write fasta         
    fastaPath = outDir + '/inserted_sequences.fa'
    FASTA.write(fastaPath)    

    return fastaPath

def assignAligments2metaclusters_paf(metaclusters, PAF_path):
    '''
    Map alignments to their corresponding metacluster. 

    Input:
        1. metaclusters: list of metaclusters
        2. PAF_path: Path to path file containing alignments to asign

    Output:
        1. tupleList: List of tuples. Each tuple contain two elements: 
            1) metacluster object  
            2) PAF object containing the corresponding alignments for the metacluster consensus inserted sequence
    '''
    ## 1. Create a dictionary to organize the data
    hits = {}

    for metacluster in metaclusters:
        metaclusterId = metacluster.ref + ':' + str(metacluster.beg) + '-' + str(metacluster.end)
        hits[metaclusterId] = (metacluster, formats.PAF())

    ## 2. Read PAF file and add hits to the metaclusters
    ## Read PAF 
    PAF = formats.PAF()
    PAF.read(PAF_path)

    # For each read alignment 
    for alignment in PAF.alignments:
        hits[alignment.qName][1].alignments.append(alignment)
    
    ## 3. Generate list of tuples
    tupleList = list(hits.values())

    return tupleList


def assignAligments2metaclusters_sam(metaclusters, SAM_path):
    '''
    Map alignments to their corresponding metacluster. 

    Input:
        1. metaclusters: list of metaclusters
        2. SAM_path: Path to SAM file containing alignments to asign

    Output:
        1. metaclustersHits: Nested list. Each list element is composed by a list with two elements: 
            1) metacluster object  
            2) List of aligned segment objects
    '''
    ## 1. Create a dictionary to organize the data
    hits = {}

    for metacluster in metaclusters:
        metaclusterId = metacluster.ref + ':' + str(metacluster.beg) + '-' + str(metacluster.end)
        hits[metaclusterId] = [metacluster, []]

    ## 2. Read SAM file and add hits to the metaclusters
    ## Read SAM 
    SAM = pysam.AlignmentFile(SAM_path, "r")

    # For each read alignment 
    for alignment in SAM:
        hits[alignment.query_name][1].append(alignment)
    
    ## 3. Generate nested list
    metaclustersHits = list(hits.values())

    return metaclustersHits
    

#############
## CLASSES ##
#############

class cluster():
    '''
    Events cluster class. A cluster is composed by a set of events.
    Each event is supported by a single read. One cluster can completely represent a single structural
    variation event or partially if multiple clusters are required (see 'metaCluster' class)
    '''
    number = 0 # Number of instances

    def __init__(self, events, clusterType):
        '''
        '''
        cluster.number += 1 # Update instances counter
        self.id = 'CLUSTER_' + str(cluster.number)
        self.clusterId = None

        # Define list of events composing the cluster and cluster type
        self.events = events
        self.clusterType = clusterType

        # Set cluster's reference, begin and end position
        self.ref, self.beg, self.end = self.coordinates() 

        # Cluster filtering
        self.filters = None
        self.failedFilters = None
        self.nbOutliers = 0

        # Update event's clusterId attribute
        for event in self.events:
            event.clusterId = self.id        

    def length(self):
        '''
        Compute cluster interval length
        '''
        return self.end - self.beg
        
    def sort(self):
        '''
        Sort events in increasing coordinates order
        '''
        self.events.sort(key=lambda event: event.beg)

    def sort_by_length(self):
        '''
        Sort events in increasing length ordering
        '''
        return sorted(self.events, key=lambda event: event.length)
        
    def coordinates(self):
        '''
        Compute cluster ref, beg and end coordinates. 
        
        Begin and end will correspond to the left and rightmost positions, respectively
        '''
        # Sort events from lower to upper beg coordinates
        self.sort()  
        
        # Define cluster coordinates 
        ref = self.events[0].ref
        beg = self.events[0].beg
        end = max([event.end for event in self.events])
        
        return ref, beg, end

    def add(self, events2add):
        '''
        Incorporate events into the cluster and redefine cluster beg and end
        positions accordingly

        Input:
            1. events2add: List of events to be added to the cluster
        '''
        # Add events to the cluster  
        self.events = self.events + events2add

        # Resort and redefine cluster begin and end coordinates
        self.ref, self.beg, self.end = self.coordinates() 

        # Update event's clusterId attribute
        for event in events2add:
            event.clusterId = self.id

    def remove(self, events2remove):
        '''
        Remove list of events from the cluster and redefine cluster beg and end
        positions accordingly

        Input:
            1. events2remove: List of events to be removed 
        '''
        ## 1. Remove events from the metacluster ##
        self.events = [event for event in self.events if event not in events2remove]

        ## 2. Resort and redefine metacluster begin and end coordinates ##
        if self.events:
            self.ref, self.beg, self.end = self.coordinates()

    def pick_median_length(self):
        '''
        Return event whose length is at the median amongst all cluster supporting events
        '''
        ## Sort events by their length
        sortedEvents = self.sort_by_length()

        ## Compute the index for the event with the median length
        median = (len(sortedEvents) - 1)/2  # minus 1 because the first element is index 0
        medianIndex = int(math.ceil(median))

        ## Pick event located at the median 
        return sortedEvents[medianIndex]

    def cv_len(self):
        '''
        Compute mean length and coefficient of variation (cv)

        Output:
            1. meanLen: Mean length
            2. cv: Coefficient of variation 
        '''
        lengths = [ event.length for event in self.events]
        meanLen = np.mean(lengths)
        std = np.std(lengths)
        cv = std / meanLen * 100 

        return meanLen, cv

    def collect_reads(self):
        '''
        Create FASTA object containing cluster supporting reads.
        
        Output:
            1. FASTA: FASTA object containing cluster supporting reads
        '''
        ## Initiate FASTA object
        FASTA = formats.FASTA()

        ## Add reads supporting the events to the FASTA
        for event in self.events:

            FASTA.seqDict[event.readName] = event.readSeq

        return FASTA
    
    def nbReads(self):
        '''
        Return the total number of reads supporting the cluster and the list of read ids
        '''
        readList = list(set([event.readName for event in self.events]))
        nbReads = len(readList)
        return nbReads, readList

    def supportingReads(self):
        '''
        Compute the total number of cluster supporting reads and generate a list of supporting read ids

        Output:
            1. nbTotal: Total number of cluster supporting reads
            2. nbTumour: Number of cluster supporting reads in the tumour 
            3. nbNormal: Number of cluster supporting reads in the normal
            4. reads: List containing all the cluster supporting reads
            5. readsTumour: List containing cluster supporting reads in the tumour sample
            6. readsNormal: List containing cluster supporting reads in the normal sample
        '''
        reads = []
        readsTumour = []
        readsNormal = []

        ## 1. Create non-redundant list of metacluster supporting reads
        for event in self.events: 

            # Add read name not already included in the list  
            if event.readName not in reads:
                reads.append(event.readName)

            ## Tumour and matched normal
            # a) Event identified in the TUMOUR sample
            if event.sample == "TUMOUR":
            
                # Add read name not already included in the list  
                if event.readName not in readsTumour:
                    readsTumour.append(event.readName)
            
            # b) Event identified in the matched NORMAL sample
            elif event.sample == "NORMAL":

                # Add read name not already included in the list  
                if event.readName not in readsNormal:
                    readsNormal.append(event.readName)            

        ## 2. Compute number of supporting reads
        nbTotal = len(reads) # total

        # a) Unpaired mode
        if self.events[0].sample is None:
            nbTumour, nbNormal, readsTumour, readsNormal = [None, None, None, None]

        # b) Paired mode
        else:

            nbTumour = len(readsTumour) # tumour
            nbNormal = len(readsNormal) # normal

        return nbTotal, nbTumour, nbNormal, reads, readsTumour, readsNormal
        
    def nbEvents(self):
        '''
        Return the number of events composing the cluster
        '''
        nbTumour = 0
        nbNormal = 0

        for event in self.events:
            # a) Event identified in the TUMOUR sample
            if event.sample == "TUMOUR":
                nbTumour += 1
            
            # b) Event identified in the matched NORMAL sample
            elif event.sample == "NORMAL":
                nbNormal += 1
            
            # c) SINGLE sample mode
            else:
                nbTumour = None
                nbNormal = None
                break

        nbTotal = len(self.events)

        return nbTotal, nbTumour, nbNormal
    
    def dupPercentage(self):
        '''
        Return the percentage of duplicates in cluster
        '''
        
        nbDup = 0
        nbTotal = len(self.events)
        
        for event in self.events:
            
            if event.isDup == True:
                nbDup += 1
        
        dupPercentage = stats.fraction(nbDup, nbTotal) * 100
        
        return dupPercentage
        


class INS_cluster(cluster):
    '''
    Insertion (INS) cluster subclass
    '''
    def __init__(self, events):

        cluster.__init__(self, events, 'INS')

    def correct_fragmentation(self):
        '''
        Correct fragmented alignments over INS
        
        Before merging:
        ############<<<INS>>>##<<INS>>###<<<INS>>>##############
        
        After merging:
        ############<<<<<<<<INS>>>>>>>>##############        
        '''
        ## 1. Organize INS events into a dictionary according to their supporting read
        eventsByReads =  {}

        for INS in self.events:
        
            # a) First event supported by that read
            if INS.readName not in eventsByReads:
                eventsByReads[INS.readName] = [INS]

            # b) There are previous events supported by that read
            else:
                eventsByReads[INS.readName].append(INS)
        
        ## 2. Merge INS events supported by the same read
        merged_list = []
        fragmented_list = []

        # For each read
        for readId, INS_list in eventsByReads.items():
            
            ## Read supporting multiple INS events
            if len(INS_list) > 1:
                
                ## 2.1 Do merging of fragmented INS
                merged = events.merge_INS(INS_list)

                ## 2.2 Add merged INS
                merged_list.append(merged)

                ## 2.3 Update fragmented alignments list
                fragmented_list = fragmented_list + INS_list

        ## 3. Update cluster
        self.add(merged_list)
        self.remove(fragmented_list)

    def identify_subclusters(self, minClusterSize):
        '''
        Identify subclusters of INS with similar lengths

        Input:
            1. minClusterSize: minimum size to create a subcluster

        Output:
            1. subclusters: list of subclusters
        '''

        subclusters = []

        ## Compute metrics based on events length
        meanLen, cv = self.cv_len()

        ## A) Cluster with heterogeneous lengths  
        if cv > 15:

            ## 1. Cluster events according to their length
            clustered = clustering.KMeans_clustering(self.events, None, 'length')

            ## 2. Filter out subclusters supported by less than X events
            clusteredFiltered = []

            for cluster in clustered.values():

                if len(cluster) >= minClusterSize:
                    clusteredFiltered.append(cluster)

            ## 3. Create subclusters
            # NOTE: Don´t create subclusters if:
            #   a) Cluster no fragmented into any subcluster passing support filter OR
            #   b) Cluster fragmented in more than 2 subclusters passing support filter (indicative of noisy region)
            if len(clusteredFiltered) in [1, 2]:

                for events in clusteredFiltered:

                    subcluster = INS_cluster(events)
                    subclusters.append(subcluster)

        return subclusters     


class DEL_cluster(cluster):
    '''
    Deletion (DEL) cluster subclass
    '''
    def __init__(self, events):

        cluster.__init__(self, events, 'DEL')

    def correct_fragmentation(self):
        '''
        Correct fragmented alignments over DEL
        
        Before merging:
        ############<<<DEL>>>##<<DEL>>###<<<DEL>>>##############
        
        After merging:
        ############<<<<<<<<DEL>>>>>>>>##############        
        '''
        ## 1. Organize DEL events into a dictionary according to their supporting read
        eventsByReads =  {}

        for DEL in self.events:
        
            # a) First event supported by that read
            if DEL.readName not in eventsByReads:
                eventsByReads[DEL.readName] = [DEL]

            # b) There are previous events supported by that read
            else:
                eventsByReads[DEL.readName].append(DEL)
        
        ## 2. Merge DEL events supported by the same read
        merged_list = []
        fragmented_list = []

        # For each read
        for readId, DEL_list in eventsByReads.items():
            
            ## Read supporting multiple DEL events
            if len(DEL_list) > 1:
                
                ## 2.1 Do merging of fragmented DEL
                merged = events.merge_DEL(DEL_list)

                ## 2.2 Add merged DEL
                merged_list.append(merged)

                ## 2.3 Update fragmented alignments list
                fragmented_list = fragmented_list + DEL_list

        ## 3. Update cluster
        self.add(merged_list)
        self.remove(fragmented_list)

class CLIPPING_cluster(cluster):
    '''
    Clipping cluster subclass
    '''
    def __init__(self, events):

        cluster.__init__(self, events, 'CLIPPING')
        self.supplCluster = None
        self.orientation = None  

    def infer_breakpoint(self):
        '''
        Infer clipping cluster breakpoint position

        Output:
            1. bkpPos: consensus breakpoint position
        '''
        ## Retrieve all positions
        positions = [clipping.beg for clipping in self.events]

        ## Count the number of reads supporting each position
        positionCounts = Counter(positions)

        ## Organize positions by degree of support into a dictionary
        supportDict = {}

        for position in positionCounts:
            nbReads = positionCounts[position]

            if nbReads not in supportDict:
                supportDict[nbReads] = [position]
            else:
                supportDict[nbReads].append(position)
        
        ## Sort by level of support
        supportLevels = sorted(supportDict.keys(), reverse=True)

        ## Select breakpoints with maximum support level
        maxSupportLevel = supportLevels[0]
        maxSupportBkps = supportDict[maxSupportLevel]
        nbBkps = len(maxSupportBkps)

        ## Resolve breakpoint
        # A) No ambiguity (single breakpoint maximum read support)
        if nbBkps == 1:
            bkpPos = maxSupportBkps[0]

        # B) Ambiguous bkp (several breakpoints with maximum read support)
        else:
            ## Compute median
            medianPos = int(np.median(positions))

            ## Compute breakpoint distance to the median
            dist2medians = []

            for bkp in maxSupportBkps:

                dist2median = abs(bkp - medianPos)
                dist2medians.append((dist2median, bkp))

            ## Sort breakpoints in decreasing distance order
            dist2medians = sorted(dist2medians, key=itemgetter(0))

            ## Select breakpoint closest to the median 
            # Note: use one arbitrary if still several possibilities
            bkpPos = dist2medians[0][1]

        return bkpPos

    def discard_outliers(self, offset = 3):
        
        ## Infer bkp
        bkp = self.infer_breakpoint()

        ## Retrieve outliers
        events2remove = [ clipping for clipping in self.events if abs(clipping.beg - bkp) > offset ]
        self.remove(events2remove)

    def search4complAlignments(self):
        '''
        Search for complementary alignments for each clipping event composing the clipping cluster.
        
        A complementary alignment span a piece of the clipped read sequence, so contributes to explain 
        the full conformation of the read alignment. 

        Output:
            1. allComplementaryAlignments: complementary alignments identified for all the clipping events composing the cluster. 
            These are organized into a dictionary with the references as keys and the list of complementary alignments as values 
        '''
        ## 1. Collect complementary alignments for each clipping event            
        complementaryAlignmentsDictList = []

        for clipping in self.events:

            ## 1.1 Retrieve suppl. alignments            
            supplAlignmentsDict = clipping.parse_supplAlignments_field()

            ## 1.2 Organize suppl. alignments into a simple list
            supplAlignments = structures.dict2list(supplAlignmentsDict)

            ## 1.3 Search for complementary alignments
            complementaryAlignments = clipping.search4complAlignments(supplAlignments)

            ## 1.4 Organize complementary alignments into a dictionary
            complementaryAlignmentsDict = events.events2Dict(complementaryAlignments) 
            complementaryAlignmentsDictList.append(complementaryAlignmentsDict)

        ## 2. Merge complementary alignments into a single dictionary
        allComplementaryAlignments = structures.merge_dictionaries(complementaryAlignmentsDictList)

        return allComplementaryAlignments

    def cluster_suppl_positions(self, supplAlignmentsDict):
        '''
        Cluster supplementary alignments based on their begin/end alignment positions

        Input:
            1. supplAlignmentsDict: dictionary with the references as keys and the list of supplementary alignments as values 

        Output:
            1. supplClusters: Dictionary containing references as keys and nested lists of supplementary 
            alignment clusters as values.
        '''
        supplClusters = {}

        # For each reference
        for ref in supplAlignmentsDict:

            # Collect suppl. alignments list
            supplAlignments = supplAlignmentsDict[ref]

            ## Cluster suppl. alignments based on their beg and end alignment positions
            clustersBeg = clustering.distance_clustering_targetPos(supplAlignments, 100, 'beg')
            clustersEnd = clustering.distance_clustering_targetPos(supplAlignments, 100, 'end')

            ## Determine bkp side based on the biggest cluster
            biggestLenBeg = max([len(cluster.events) for cluster in clustersBeg])
            biggestLenEnd = max([len(cluster.events) for cluster in clustersEnd])

            # a) Bkp at the beg of supplementary alignment interval
            if biggestLenBeg >= biggestLenEnd:
                clusters = clustersBeg
            
            # b) Bkp at the end of supplementary alignment interval
            else:
                clusters = clustersEnd
                
            ## Add clusters to the dictionary
            supplClusters[ref] = clusters

        return supplClusters
    
    def consEventBridge(self):
        '''
        Identify event with the longest clipping and aligned regions (Representative, consensus event)
        It's the best event to define the RG.
        
        Input:
        1. CLIPPING cluster 
        
        Output:
        1. Clipping event with longest clipping and aligned regions
        '''
        ## 0. sort by length
        clippingLens = sorted(self.events, key=lambda event: event.length, reverse=True) # sort by descending order of clipping length
        alignLens = sorted(self.events, key=lambda event: event.alignedLen, reverse=True) # same for aligned regions
        
        ## 1. create dictionaries using list indexes
        dictClip = {event.readName: index for index, event in enumerate(clippingLens)}
        dictAlign = {event.readName: index for index, event in enumerate(alignLens)}

        ## 2. intersect dictionaries adding both indexes length 
        dictEvents = {}
        for i, j in dictClip.items():
            for x, y in dictAlign.items():
                if i == x:
                    dictEvents[i]=(j+y)

        ## 3. select event with lowest sum of both indexes (greater length for both clipping and aligned regions)
        consensusKey = min(dictEvents, key=dictEvents.get)
        consensusEvent = [event for event in self.events if event.readName == consensusKey][0]
        
        return consensusEvent
    
    def longest_clipping_seq(self, offset = 1000):
        '''
        Identify event with the longest clipping
        
        Input:
        1. CLIPPING cluster 
        
        Output:
        1. Clipping event with longest clipping
        '''
        clippingLens = sorted([event for event in self.events if event.clippingType == 'soft'], key=lambda event: event.length, reverse=True) # sort by descending order of clipping length
        
        if clippingLens:
            longestClipping = clippingLens[0]
            longestClipping.altSeq = longestClipping.clipped_seq(offset)
        
        else:
            print("Hard clipping issue", self.ref, self.beg)
            longestClipping = self.events[0]
            longestClipping.altSeq = ""

        return longestClipping


class SUPPLEMENTARY_cluster(cluster):
    '''
    Supplementary alignment cluster subclass
    '''
    def __init__(self, events):

        cluster.__init__(self, events, 'SUPPLEMENTARY')
        self.representative = None
        self.bkpSide = None
        self.annot = None
        self.bridge = False
        self.bridgeInfo = {}
        self.orientation = None
    
    def clippOrientation(self):
        '''
        Determine cluster bkp side
        '''
        # 1. Collect supplementary events clipping sides
        [event.clippingSide() for event in self.events]
        clipSides = set([event.clipSide for event in self.events])
        
        # 2. Set supplementary cluster bkp side
        if clipSides == {'left', 'right'}:
            self.orientation = 'RECIPROCAL'
            
        elif clipSides == {'right'}:
            self.orientation = 'PLUS'
            
        elif clipSides == {'left'}:
            self.orientation = 'MINUS'

    def inferBkp_shortReads(self):
        '''
        Compute and return breakpoint position for PE short reads
        '''
        # Determine cluster bkp side
        if self.orientation == None:
            self.clippOrientation()
        
        # a) Breakpoint on the left
        if self.orientation == 'MINUS':
            coordList = [event.beg for event in self.events]
            bkpPos = max(set(coordList), key=coordList.count)

        # b) Breakpoint on the right
        elif self.orientation == 'PLUS':
            coordList = [event.end for event in self.events]
            bkpPos = max(set(coordList), key=coordList.count)
        
        # c) If orientation reciprocal or unknown, return coordinate with max nb of counts
        else:
            coordList = [event.beg for event in self.events] + [event.end for event in self.events]
            bkpPos = max(set(coordList), key=coordList.count)
        
        return bkpPos

    def bkpPos(self):
        '''
        Compute and return breakpoint position
        '''
        # a) Breakpoint on the left
        if self.bkpSide == 'beg':
            bkpPos = self.beg

        # b) Breakpoint on the right
        else:
            bkpPos = self.end
        
        return bkpPos
        
    def clusterIndex(self):
        '''
        Compute supplementary cluster read level index. Index relative to the clipping
        cluster

        ########################################## Hipothetical consensus read
        ------------- ------------ ---------------
         -----------   ----------   -------------
        ------------   ----------   -------------
      clipping_cluster suppl_cluster suppl_cluster
                        (index: 0)    (index: 1)

        Output:
            1. index: read level index relative to the clipping cluster
        '''
        ## 1. Generate list containing index position for all the suppl. alignments composing the cluster
        indexes = [supplementary.readIndex for supplementary in self.events]

        ## 2. Count number occurrences for each index
        occurences = [[index ,indexes.count(index)] for index in set(indexes)]

        ## 3. Sort indexes in decreasing order of occurrences
        occurences.sort(key = lambda x: x[1], reverse = True) 

        # 4. Select index with maximum number of instances
        index = occurences[0][0]

        ## Note: return an ambiguous flag if several possible maximum
        return index
    
class META_cluster():
    '''
    Meta cluster class
    '''

    def __init__(self, clusters):
        '''
        '''
        self.id = 'META_' + str(uuid.uuid4())

        # Define list of events composing the cluster 
        self.events = list(itertools.chain(*[cluster.events for cluster in clusters]))

        # Set cluster's reference, begin and end position
        self.ref, self.beg, self.end = self.coordinates()
        self.refLeftBkp = None
        self.refRightBkp = None
        
        # Organize events into subclusters
        self.subclusters = self.create_subclusters()
        # NOTE MERGE SR2020: To avoid key META error in clustering.reciprocal_overlap_clustering
        self.rawSubclusters = clusters

        # Set some metacluster properties as None
        self.bkpPos = None
        self.mutOrigin = None
        self.failedFilters = None
        self.consensusEvent = None                
        self.insertHits = None
        self.nbTotal, self.nbTumour, self.nbNormal, self.nbINS, self.nbDEL, self.nbCLIPPING = [None, None, None, None, None, None] 
        self.nbReadsTotal, self.nbReadsTumour, self.nbReadsNormal, self.reads, self.readsTumour, self.readsNormal = [None, None, None, None, None, None] 
        self.cv = None
        self.repreLeftSeq = None
        self.repreRightSeq = None
        self.consLeftSeq = None
        self.consRightSeq = None
        self.intLeftBkp = None
        self.intRightBkp = None
        self.rightClipType = None
        self.leftClipType = None

        ## Short reads:
        self.orientation = None
        # ins type and identity
        self.ins_type = None
        self.identity = None
        self.plus_id = None
        self.minus_id = None
        # transduction attributes
        self.src_id = None
        self.plus_src_id = None
        self.minus_src_id = None
        self.src_end = None
        self.plus_src_end = None
        self.minus_src_end = None
        self.src_type = None
        # polyA
        self.pA = None
        self.plus_pA = None
        self.minus_pA = None
        # mei characteristics
        self.TSD = None
        self.strand = None
        self.repeatAnnot = None

        # Short reads:
        if hasattr(self.events[0], 'identity'):
            self.identity = self.events[0].identity
            
        if hasattr(clusters[0], 'orientation'):
            if all (cluster.orientation == 'PLUS' for cluster in clusters):
                self.orientation = 'PLUS'
            elif all (cluster.orientation == 'MINUS' for cluster in clusters):
                self.orientation = 'MINUS'
            else:
                self.orientation = 'RECIPROCAL'
                    
        # Update input cluster's clusterId attribute
        for cluster in clusters:
            cluster.clusterId = self.id

        # Initialize dictionaries 
        self.SV_features = {}
        self.supplClusters = {}
        self.bridge = None
    
    def identity_shortReads_ME(self):
        '''
        Define plus and minus cluster identities, and set polyA attributes if polyA present
        '''
        self.identity = None
        self.plus_id = None
        self.minus_id = None

        ## 1. collect events identities
        plus_identities = [event.identity for event in self.events if event.orientation == 'PLUS']
        minus_identities = [event.identity for event in self.events if event.orientation == 'MINUS']     
        
        # get total counts
        countPlus = len(plus_identities)
        countMinus = len(minus_identities)
        
        ## 2. set pA support
        if 'Simple_repeat' in plus_identities:
            self.plus_pA, self.pA = True, True
            plus_identities = [value for value in plus_identities if value != 'Simple_repeat']
        
        if 'Simple_repeat' in minus_identities:
            self.minus_pA, self.pA = True, True
            minus_identities = [value for value in minus_identities if value != 'Simple_repeat']
                    
        ## 3. set clusters identity
        plus_identityDict = Counter(plus_identities)
        minus_identityDict = Counter(minus_identities)
        
        # select most supported identity for plus cluster
        if plus_identityDict:
            
            # select identity with the max support
            plus_identityDict_max = max(plus_identityDict.items(), key=itemgetter(1))
            
            # if the support is > 10% of total identity counts
            if plus_identityDict_max[1]/countPlus > 0.1:
                
                plus_id = plus_identityDict_max[0].split('_')
                self.plus_id = plus_id[0]
                
                # if it's a TD
                if len(plus_id) == 3:
                    self.plus_src_id, self.plus_src_end = plus_id[1], plus_id[2]
            
        # if polyA ('Simple_repeat') support is more than 90%
        if not self.plus_id and countPlus > 0:
            self.plus_id = 'Simple_repeat'
        
        # select most supported identity for minus cluster    
        if minus_identityDict:
            
            # select identity with the max support
            minus_identityDict_max = max(minus_identityDict.items(), key=itemgetter(1))
            
            # if the support is > 10% of total identity counts
            if minus_identityDict_max[1]/countMinus > 0.1:
                
                minus_id = minus_identityDict_max[0].split('_')
                self.minus_id = minus_id[0]
                
                # if it's a TD
                if len(minus_id) == 3:
                    self.minus_src_id, self.minus_src_end = minus_id[1], minus_id[2]
            
        # if polyA ('Simple_repeat') support is more than 90%
        if not self.minus_id and countMinus > 0:
            self.minus_id = 'Simple_repeat'
                   
    def sort(self):
        '''
        Sort events in increasing coordinates order
        '''
        self.events.sort(key=lambda event: event.beg)

    def coordinates(self):
        '''
        Compute cluster ref, beg and end coordinates. 
        
        Begin and end will correspond to the left and rightmost positions, respectively
        '''
        # Sort events from lower to upper beg coordinates
        self.sort()  

        # Define cluster coordinates 
        ref = self.events[0].ref
        beg = self.events[0].beg
        end = max([event.end for event in self.events])
        
        return ref, beg, end

    def mean_pos(self):
        '''
        Compute cluster mean genomic position and confidence interval around the mean 
        
        Output:
            1. pos: mean genomic position
            2. cipos: confidence interval around the mean position
        '''
        ## 1. Collect all begin positions
        begs = [event.beg for event in self.events]

        ## 2. Compute mean position and standard error 
        mean = int(np.mean(begs))
        sem = scipy.stats.sem(begs)

        ## 3. Compute confidence interval positions
        if (sem == 0) or (math.isnan(sem)):
            CI = (mean, mean)
        else:
            CI = scipy.stats.t.interval(0.95, len(begs)-1, loc=mean, scale=sem)
            
        ## Make confidence interval relative to the mean position
        CI = (int(CI[0] - mean), int(CI[1] - mean))
        
        return mean, CI

    def median_pos(self):
        '''
        Compute cluster median genomic position and confidence interval  
        
        Output:
            1. pos: median genomic position
            2. cipos: confidence interval around the median position
        '''
        ## 1. Collect all begin positions
        begs = [event.beg for event in self.events]

        ## 2. Compute median position and standard error 
        median = int(np.median(begs))
        sem = scipy.stats.sem(begs)

        ## 3. Compute confidence interval positions
        if (sem == 0) or (math.isnan(sem)):
            CI = (median, median)
        else:
            CI = scipy.stats.t.interval(0.95, len(begs)-1, loc=median, scale=sem)
            
        ## Make confidence interval relative to the median position
        CI = (int(CI[0] - median), int(CI[1] - median))
        
        return median, CI
    
    def create_subclusters(self):
        '''
        Organize cluster composing events into subclusters

        Output:
            1. subclusters: dictionary containing cluster types as keys and cluster object as values
        '''
        ## 1. Separate events according to their type into multiple lists ##
        eventTypes = events.separate(self.events)

        ## 2. Create subclusters ##
        subclusters = {}

        for eventType, eventList in eventTypes.items():

            ## Create subcluster
            subcluster = create_cluster(eventList, eventType) 

            ## Set subcluster metacluster id attribute:
            subcluster.clusterId = self.id

            ## Add subcluster to the dict
            subclusters[eventType] = subcluster 

        return subclusters

    def add(self, clusters2add):
        '''
        Add a list of clusters to the metacluster 

        Input:
            1. clusters2add: List of clusters to be added 
        '''
        ## 0. Update metacluster's id attribute
        for cluster in clusters2add:
            cluster.clusterId = self.id

        ## 1. Add events within the input cluster to the metacluster ##
        events2add = list(itertools.chain(*[cluster.events for cluster in clusters2add]))
        self.events = self.events + events2add

        ## 2. Resort and redefine metacluster begin and end coordinates ##
        self.ref, self.beg, self.end = self.coordinates()

        ## 3. Separate events according to their type into multiple lists ##
        eventTypes = events.separate(events2add)

        ## 4. Add events to the subclusters ##
        for eventType, eventList in eventTypes.items():
            
            # a) Create subcluster if not pre-existing one
            if eventType not in self.subclusters:
         
                ## Create subcluster
                subcluster = create_cluster(eventList, eventType) 
            
                ## Set subcluster metacluster id attribute:
                subcluster.clusterId = self.id

                ## Add subcluster to the dict
                self.subclusters[eventType] = subcluster 

            # b) Add events to pre-existing subcluster
            else:
                self.subclusters[eventType].add(eventList)

        # Update input cluster's clusterId attribute
        for cluster in clusters2add:
            cluster.clusterId = self.id
        # NOTE MERGE SR2020: To avoid key META error in clustering.reciprocal_overlap_clustering
        # Also add clusters to rawSubclusters:
        self.rawSubclusters.extend(clusters2add)

    def addEvents(self, eventsList):
        '''

        Input:
            1. events: List of events to be added to the metacluster
        '''
        ## 1. Add events to the cluster ##
        previous = self.events
        self.events = self.events + eventsList

        ## 2. Resort and redefine cluster begin and end coordinates ##
        self.ref, self.beg, self.end = self.coordinates()

        ## 3. Separate events according to their type into multiple lists ##
        eventTypes = events.separate(eventsList)

        ## 4. Add events to the subclusters ##
        for eventType, eventList in eventTypes.items():
            
            # a) Create subcluster if not pre-existing one
            if eventType not in self.subclusters:
         
                ## Create subcluster
                subcluster = create_cluster(eventList, eventType) 
            
                ## Add subcluster to the dict
                self.subclusters[eventType] = subcluster 

            # b) Add events to pre-existing subcluster
            else:
                self.subclusters[eventType].add(eventList)

    def remove(self, events2remove):
        '''
        Remove a list of events from the metacluster and corresponding subclusters

        Input:
            1. events2remove: List of events to be removed 
        '''
        ## 1. Remove events from the metacluster ##
        self.events = [event for event in self.events if event not in events2remove]

        ## 2. Resort and redefine metacluster begin and end coordinates ##
        if self.events:
            self.ref, self.beg, self.end = self.coordinates()

        ## 3. Separate events according to their type into multiple lists ##
        eventTypes = events.separate(events2remove)

        ## 4. Remove events from the subclusters ##
        for eventType, eventList in eventTypes.items():

            if eventType in self.subclusters:
                self.subclusters[eventType].remove(eventList)
                
            else:
                log.info('WARNING at remove method from META_cluster class. Event with unkown type')

    def collect_reads(self):
        '''
        Create FASTA object containing metacluster supporting reads.
        
        Output:
            1. FASTA: FASTA object containing metacluster supporting reads
        '''
        ## Initiate FASTA object
        FASTA = formats.FASTA()

        ## For each event composing the metacluster         
        for event in self.events: 

            ## A) Read sequence supporting the event not included in the FASTA yet
            if event.readName not in FASTA.seqDict:
                FASTA.seqDict[event.readName] = event.readSeq 

            ## B) Read sequence supporting the event already included in the FASTA
            else:

                # Current read sequence aligned as primary -> replace previously included sequence (therefore supplementary)
                if not event.supplementary:
                    FASTA.seqDict[event.readName] = event.readSeq 
     
        return FASTA

    def collect_altSeqs(self, offset = 2500):
        '''
        Create FASTA object containing the alt sequence of metacluster supporting reads.
        
        Output:
            1. FASTA: FASTA object containing alt sequences
        '''
        ## Initiate FASTA object
        FASTA = formats.FASTA()

        ## For each event composing the metacluster         
        for event in self.events:
            
            if event.type == 'INS':
                event.altSeq = event.pick_ALT(offset)
                
            elif event.type == 'CLIPPING':
                event.altSeq = event.clipped_seq(offset)
            
            else:
                print('event type in cluster weird')
                print(event.type)
                
            ## A) Read sequence supporting the event not included in the FASTA yet
            if event.readName not in FASTA.seqDict:
                FASTA.seqDict[event.readName] = event.altSeq 

            ## B) Read sequence supporting the event already included in the FASTA
            else:

                # Current read sequence aligned as primary -> replace previously included sequence (therefore supplementary)
                if not event.supplementary:
                    FASTA.seqDict[event.readName] = event.altSeq 
        
        return FASTA

    def supportingReads(self):
        '''
        Compute the total number of metacluster supporting reads and generate a list of supporting read ids

        Output:
            1. nbTotal: Total number of metacluster supporting reads
            2. nbTumour: Number of metacluster supporting reads in the tumour 
            3. nbNormal: Number of metacluster supporting reads in the normal
            4. reads: List containing all the metacluster supporting reads
            5. readsTumour: List containing metacluster supporting reads in the tumour sample
            6. readsNormal: List containing metacluster supporting reads in the normal sample
        '''
        reads = []
        readsTumour = []
        readsNormal = []

        ## 1. Create non-redundant list of metacluster supporting reads
        for event in self.events: 

            # Add read name not already included in the list  
            if event.readName not in reads:
                reads.append(event.readName)

            ## Tumour and matched normal
            # a) Event identified in the TUMOUR sample
            if event.sample == "TUMOUR":
            
                # Add read name not already included in the list  
                if event.readName not in readsTumour:
                    readsTumour.append(event.readName)
            
            # b) Event identified in the matched NORMAL sample
            elif event.sample == "NORMAL":

                # Add read name not already included in the list  
                if event.readName not in readsNormal:
                    readsNormal.append(event.readName)            

        ## 2. Compute number of supporting reads
        nbTotal = len(reads) 

        # a) Unpaired mode
        if self.events[0].sample is None:
            nbTumour, nbNormal, readsTumour, readsNormal = [None, None, None, None]

        # b) Paired mode
        else:

            nbTumour = len(readsTumour) # tumour
            nbNormal = len(readsNormal) # normal

        return nbTotal, nbTumour, nbNormal, reads, readsTumour, readsNormal

    def nbEvents(self):
        '''
        Return the number of events composing the metacluster. 
        '''
        ## Initialize counters
        nbTumour = 0
        nbNormal = 0

        nbINS = 0
        nbINS_reads = []
        nbDEL = 0
        nbCLIPPING = 0
        nbCLIPPING_reads = []
        nbCLIPPING_left = 0
        nbCLIPPING_right = 0

        # For each event composing the metacluster
        for event in self.events:

            ## Tumour and matched normal counts
            # a) Event identified in the TUMOUR sample
            if event.sample == "TUMOUR":
                nbTumour += 1
            
            # b) Event identified in the matched NORMAL sample
            elif event.sample == "NORMAL":
                nbNormal += 1
            
            # c) SINGLE sample mode
            else:
                nbTumour = None
                nbNormal = None
                            
            ## Event type counts
            # a) INS event
            if event.type == 'INS':
                
                if event.readName not in nbINS_reads:
                    nbINS += 1
                    nbINS_reads.append(event.readName)

            # b) DEL event
            elif event.type == 'DEL':
                nbDEL += 1

            # c) CLIPPING event
            elif event.type == 'CLIPPING':
                
                if event.readName not in nbCLIPPING_reads:
                    nbCLIPPING += 1
                    nbCLIPPING_reads.append(event.readName)
                
                if event.clippedSide == 'left':
                    nbCLIPPING_left += 1
                    
                elif event.clippedSide == 'right':
                    nbCLIPPING_right += 1                

        nbTotal = len(list(set([event.readName for event in self.events])))

        return nbTotal, nbTumour, nbNormal, nbINS, nbDEL, nbCLIPPING, nbCLIPPING_left, nbCLIPPING_right

    def percDuplicates(self):
        '''
        Return the number of events that compose the metacluster and are labbeled as duplicate in the input bam file. 
        '''
        ## Initialize counters
        nbDiscordantDuplicatesTumour = 0
        nbDiscordantDuplicatesNormal = 0
        nbDiscordantDuplicatesTotal = 0
        nbClippingDuplicatesTumour = 0
        nbClippingDuplicatesNormal = 0
        nbClippingDuplicatesTotal = 0

        nbDiscordantTumour = 0
        nbClippingTumour = 0
        nbDiscordantNormal = 0
        nbClippingNormal = 0
        nbDiscordantTotal = 0
        nbClippingTotal = 0

        # For each event composing the metacluster
        for event in self.events:

            ## Tumour and matched normal counts
            # a) Event identified in the TUMOUR sample
            if event.sample == "TUMOUR":
                if event.isDup == True:
                    if event.type == 'DISCORDANT':
                        nbDiscordantDuplicatesTumour += 1
                    elif event.type == 'CLIPPING':
                        nbClippingDuplicatesTumour  += 1
                elif event.isDup == False:
                    if event.type == 'DISCORDANT':
                        nbDiscordantTumour += 1
                    elif event.type == 'CLIPPING':
                        nbClippingTumour  += 1

            elif event.sample == "NORMAL":
                if event.isDup == True:
                    if event.type == 'DISCORDANT':
                        nbDiscordantDuplicatesNormal += 1
                    elif event.type == 'CLIPPING':
                        nbClippingDuplicatesNormal  += 1
                elif event.isDup == False:
                    if event.type == 'DISCORDANT':
                        nbDiscordantNormal += 1
                    elif event.type == 'CLIPPING':
                        nbClippingNormal  += 1
            
            # c) SINGLE sample mode
            elif event.sample == None:
                if event.isDup == True:
                    if event.type == 'DISCORDANT':
                        nbDiscordantDuplicatesTotal += 1
                    elif event.type == 'CLIPPING':
                        nbClippingDuplicatesTotal += 1
                elif event.isDup == False:
                    if event.type == 'DISCORDANT':
                        nbDiscordantTotal += 1
                    elif event.type == 'CLIPPING':
                        nbClippingTotal  += 1
                            
        if nbDiscordantDuplicatesTotal == 0:
            nbDiscordantDuplicatesTotal = nbDiscordantDuplicatesTumour + nbDiscordantDuplicatesNormal
            nbClippingDuplicatesTotal = nbClippingDuplicatesTumour + nbClippingDuplicatesNormal
            nbDiscordantTotal = nbDiscordantTumour + nbDiscordantNormal
            nbClippingTotal = nbClippingTumour + nbClippingNormal

        if nbDiscordantDuplicatesTotal > 0:
            percDiscordantDuplicates = (nbDiscordantDuplicatesTotal / (nbDiscordantDuplicatesTotal + nbDiscordantTotal)) * 100
        else:
            percDiscordantDuplicates = 0
        if nbClippingDuplicatesTotal > 0:
            percClippingDuplicates = (nbClippingDuplicatesTotal / (nbClippingDuplicatesTotal + nbClippingTotal)) * 100
        else:
            percClippingDuplicates = 0


        return percDiscordantDuplicates, percClippingDuplicates

    def nbDISCORDANT(self):
        '''
        Return the number of discordant events composing the metacluster. 
        '''                       
        nbDISCORDANT = len(set([event.readName for event in self.events if event.type == 'DISCORDANT']))

        return nbDISCORDANT

    def nbSUPPLEMENTARY(self):
        '''
        Return the number of supplementary events composing the metacluster. 
        '''        
        nbSUPPL = len(set([event.readName for event in self.events if event.type == 'SUPPLEMENTARY']))
        
        return nbSUPPL

    def nbCLIPPINGS(self):
        '''
        Return the number of clipping events composing the metacluster. 
        '''        
        nbCLIPP = len(set([event.readName for event in self.events if event.type == 'CLIPPING']))
        
        return nbCLIPP

    def supportingCLIPPING(self, buffer, confDict, bam, normalBam, mode):
        # Note: This function works but you have to allow duplicates in the clipping 

        # Make custom conf. dict for only selecting duplicates
        clippingConfDict = dict(confDict)
        clippingConfDict['targetSV'] = ['CLIPPING']
        clippingConfDict['minMAPQ'] = 0

        clippingEventsDict = {}

        ## Define region
        binBeg = self.beg - buffer if self.beg > buffer else 0
        
        # TODO check as above
        binEnd = self.end

        ref = self.ref

        if mode == "SINGLE":
            clippingEventsDict = bamtools.collectSV(ref, binBeg, binEnd, bam, clippingConfDict, None)
        elif mode == "PAIRED":
            clippingEventsDict = bamtools.collectSV_paired(ref, binBeg, binEnd, bam, normalBam, clippingConfDict)

        ## When the discordant cluster is RIGHT, add the biggest right clipping cluster if any:
        if all (event.side == 'PLUS' for event in self.events):
            
            ## Get clipping clusters:
            clippingRightEventsDict = dict((key,value) for key, value in clippingEventsDict.items() if key == 'RIGHT-CLIPPING')
            CLIPPING_cluster = self.add_clippingEvents(ref, binBeg, binEnd, clippingRightEventsDict, ['RIGHT-CLIPPING'], confDict)

        ## When the discordant cluster is LEFT, add the biggest left clipping cluster if any:
        elif all (event.side == 'MINUS' for event in self.events):
            
            ## Get clipping clusters:
            clippingLeftEventsDict = dict((key,value) for key, value in clippingEventsDict.items() if key == 'LEFT-CLIPPING')
            CLIPPING_cluster = self.add_clippingEvents(ref, binBeg, binEnd, clippingLeftEventsDict, ['LEFT-CLIPPING'], confDict)

        # TODO if it is reciprocal
        else:
            CLIPPING_cluster = self.add_clippingEvents(ref, binBeg, binEnd, clippingEventsDict, ['RIGHT-CLIPPING', 'LEFT-CLIPPING'], confDict)

        return CLIPPING_cluster

    def supportingCLIPPING_sonia(self, confDict, bam, normalBam):
        '''
        Collect supporting clippings 
        '''
        
        # Make custom conf. dict
        clippingConfDict = dict(confDict)
        clippingConfDict['targetEvents'] = ['CLIPPING']
        clippingConfDict['minMAPQ'] = 1
        clippingConfDict['readFilters'] = ['SMS']
        clippingConfDict['minClusterSize'] = 1

        clippingEventsDict = {}

        # Define region
        ref = self.ref
        beg = self.refLeftBkp if self.refLeftBkp is not None else self.beg
        end = self.refRightBkp if self.refRightBkp is not None else self.end

        # Collect clippings
        if not normalBam:
            clippingEventsDict = bamtools.collectSV(ref, beg, end, bam, clippingConfDict, None)
            
        else:
            clippingEventsDict = bamtools.collectSV_paired(ref, beg, end, bam, normalBam, clippingConfDict)
        
        # When cluster orientation is 'PLUS', add the biggest right clipping cluster if any:
        if self.orientation == 'PLUS':
            
            ## Get clipping clusters:
            clippingRightEventsDict = dict((key,value) for key, value in clippingEventsDict.items() if key == 'RIGHT-CLIPPING')
            CLIPPING_cluster = self.add_clippingEvents(ref, beg, end, clippingRightEventsDict, ['RIGHT-CLIPPING'], confDict)

        # When cluster orientation is LEFT, add the biggest left clipping cluster if any:
        elif self.orientation == 'MINUS':
            
            ## Get clipping clusters:
            clippingLeftEventsDict = dict((key,value) for key, value in clippingEventsDict.items() if key == 'LEFT-CLIPPING')
            CLIPPING_cluster = self.add_clippingEvents(ref, beg, end, clippingLeftEventsDict, ['LEFT-CLIPPING'], confDict)

        # If it is reciprocal
        elif self.orientation == 'RECIPROCAL':
            
            CLIPPING_cluster = self.add_clippingEvents(ref, beg, end, clippingEventsDict, ['RIGHT-CLIPPING', 'LEFT-CLIPPING'], confDict)

        return CLIPPING_cluster
        
    def add_clippingEvents(self, ref, binBeg, binEnd, clippingEventsDict, eventTypes, confDict):
        '''
        Create clipping clusters and add them to metacluster
        '''
        binSizes = [1000]
        clippingBinDb = structures.create_bin_database_interval(ref, binBeg, binEnd, clippingEventsDict, binSizes)
        binSize = clippingBinDb.binSizes[0]
        CLIPPING_clusters = clustering.distance_clustering(clippingBinDb, binSize, eventTypes, 'CLIPPING', confDict['maxBkpDist'], confDict['minClusterSize']) 
        
        # If there is a clipping cluster
        for CLIPPING_cluster in CLIPPING_clusters:
            
            ## Add cluster's reads to the discordant metacluster:
            self.addEvents(CLIPPING_cluster.events)

        return CLIPPING_clusters
 

    def polish(self, confDict, reference, outDir, assemblyStep = True):
        '''
        Polish metacluster consensus sequence

        Input: 
            1. confDict: 
                * technology     -> sequencing technology (NANOPORE, PACBIO or ILLUMINA)
                * rounds         -> number of polishing rounds to be attempled. 0 means no polishing
            
            2. reference: path to reference genome in fasta format    
            3. outDir: Output directory
        
        Output: Update 'consensusFasta' attribute with the polished sequence
        '''
        ## 0. Create directory 
        unix.mkdir(outDir)
        
        # create attr to preserve first raw consensus
        self.consensusFasta_raw = self.consensusFasta
        
        ## 1.1 Collect metacluster supporting reads and write a fasta       
        supportingReads = self.collect_altSeqs()
        supportingReadsFasta = outDir + '/supportingReads_altSeq.fa'
        supportingReads.write(supportingReadsFasta)
        
        ## 1.2 Run wtbgs2 to obtain a consensus
        if assemblyStep:
            unpolishedFasta = assembly.cons_wtdbg2(supportingReadsFasta, outDir, confDict['processes'], confDict['technology_subtype'])
            
        else:
            unpolishedFasta = None
            
        ## If wtbgs2 fails, use median insert size read as consensus to be polished
        if unpolishedFasta is None:
            
            ## 1.1 Write raw consensus sequence
            unpolishedFasta = outDir + '/raw_consensus.fa'
            self.consensusFasta.write(unpolishedFasta)

            ## 1.2 Remove consensus from FASTA
            consensusReadName = list(self.consensusFasta.seqDict.keys())[0]

            if consensusReadName in supportingReads.seqDict:
                del supportingReads.seqDict[consensusReadName]

            ## Write supporting reads FASTA 
            supportingReadsFasta = outDir + '/supportingReads_altSeq.fa'
            supportingReads.write(supportingReadsFasta)
    
        ## 2. Consensus polishing using Racon
        polishedFasta = assembly.polish_racon(unpolishedFasta, supportingReadsFasta, confDict['technology'], confDict['rounds'], outDir)
            
        ## If polishing is successful replace consensus by new polished sequence
        if polishedFasta is not None:
            
            polished = formats.FASTA()
            polished.read(polishedFasta)  
            self.consensusFasta = polished

        return 

    def consensus_event(self, confDict, reference, offset, outDir):
        '''
        Define metacluster´s consensus event based on consensus sequence realignment

        Input: 
            1. confDict: 
                * targetSV       -> list with target SV (INS: insertion; DEL: deletion; CLIPPING: left and right clippings)
                * minMAPQ        -> minimum mapping quality
                * minCLIPPINGlen -> minimum clipping lenght
                * minINDELlen    -> minimum INS and DEL lenght
                * overhang       -> Number of flanking base pairs around the INDEL events to be collected from the supporting read. If 'None' the complete read sequence will be collected)            2. outDir: output directory
            
            2. reference: path to reference genome in fasta format    
            3. offset: number of base pairs to extend cluster begin and end coordinates when defining target region for consensus sequence realignment
            4. outDir: Output directory
        
        Output: Update 'consensusEvent' attribute in the metacluster class
        '''

        ## 1. Local realignment of the consensus sequence into the SV genomic interval ##
        ## 1.1 Write consensus sequence into a fasta file 
        consensusFile = outDir + '/consensus.fa'
        self.consensusFasta.write(consensusFile)

        ## 1.2 Define SV cluster genomic interval
        # ------------------<***SV_cluster***>-----------------
        #       <--offset-->                  <--offset-->
        intervalBeg = self.beg - offset
        intervalBeg = intervalBeg if intervalBeg >= 0 else 0 ## Set lower bound
        intervalEnd = self.end + offset
        intervalCoord = self.ref + ':' + str(intervalBeg) + '-' + str(intervalEnd)
            
        ## 1.3 Do realignment
        # ------------------<***SV_cluster***>-----------------
        #         -------------consensus_seq-------------
        BAM = alignment.targeted_alignment_minimap2(consensusFile, intervalCoord, reference, outDir, 'BAM')
 
        ## Continue if realignment is succesfull 
        if BAM is not None:
            
            ## 2. Search for metacluster´s consensus event  ##
            ## 2.1 Extract events from consensus sequence realignment
            # ------------------<***SV_cluster***>-----------------
            #        -------------consensus_seq-------------
            #               <--->----------------<---> overhang (100 bp)
            #                   event_search_space         
            overhang = 100
            clusterIntervalLen = self.end - self.beg
            targetBeg = offset - overhang
            targetEnd = offset + clusterIntervalLen + overhang
            confDict['minCLIPPINGlen'] = 200 # By default set to 500 bp. It won't detect short insertions 
            eventsDict = bamtools.collectSV(intervalCoord, targetBeg, targetEnd, BAM, confDict, None)
            
            # filter eventsDict by selecting all alignments corresponding to longest contig for each type  [INS, RIGHT-CLIPPING, LEFT-CLIPPING]
            for key in eventsDict:
                eventsList = eventsDict[key]
                
                if eventsList != []:
                    # get name of the longest read (contig)
                    longestName=sorted(eventsList, key=lambda event: len(event.readSeq), reverse=True)[0].readName
                    # select only alignments of the longest read (contig)
                    eventsDict[key] = [event for event in eventsList if event.readName == longestName]
            
            ## 2.2 Define consensus event based on the events resulting from consensus sequence realignment
            ## A) Metacluster supports an INS and realignment leads to one INS event 
            if (self.SV_type == 'INS') and ('INS' in eventsDict) and (len(eventsDict['INS']) == 1):

                ## Convert coordinates
                self.consensusEvent = alignment.targetered2genomic_coord(eventsDict['INS'][0], self.ref, intervalBeg)

            ## B) Metacluster supports an INS and realignment leads to multiple INS events
            elif (self.SV_type == 'INS') and ('INS' in eventsDict) and (len(eventsDict['INS']) > 1):

                ## Do merging
                merged = events.merge_INS(eventsDict['INS'])

                ## Convert coordinates
                self.consensusEvent = alignment.targetered2genomic_coord(merged, self.ref, intervalBeg)

            ## C) Metacluster supports a DEL and realignment leads to one DEL event 
            elif (self.SV_type == 'DEL') and ('DEL' in eventsDict) and (len(eventsDict['DEL']) == 1):

                ## Convert coordinates
                self.consensusEvent = alignment.targetered2genomic_coord(eventsDict['DEL'][0], self.ref, intervalBeg)

            ## D) Metacluster supports a DEL and realignment leads to multiple DEL events (TO DO)
            #elif (self.SV_type == 'DEL') and (len(eventsDict['DEL']) > 1):
                    ## Do merging
                    ## Convert coordinates
                        
            ## E) Metacluster supports an INS and realignment leads to one left and one right CLIPPING 
            elif (self.SV_type == 'INS') and ('LEFT-CLIPPING' in eventsDict) and ('RIGHT-CLIPPING' in eventsDict) and (len(eventsDict['RIGHT-CLIPPING']) == 1) and (len(eventsDict['LEFT-CLIPPING']) == 1):

                ## Compute the inserted sequence length as the difference between both clipping breakpoints in the long sequence 
                rightClipping = eventsDict['RIGHT-CLIPPING'][0]
                leftClipping = eventsDict['LEFT-CLIPPING'][0]
                length = leftClipping.readBkp - rightClipping.readBkp

                ## Create consensus INS from clipping alignments if inserted sequence found
                # and ins length >= minINDELlen and both clippings come from the same contig (fully cover the inserted seq)
                if (length >= confDict['minINSlen']) and (rightClipping.readName == leftClipping.readName):

                    event = events.INS(rightClipping.ref, rightClipping.beg, rightClipping.end, length, rightClipping.readName, rightClipping.readSeq, rightClipping.readBkp, None, None)
        
                    ## Convert coordinates
                    self.consensusEvent = alignment.targetered2genomic_coord(event, self.ref, intervalBeg)
                    
                else:
                    
                    self.consensusEvent = None
                    print('INS_NOT_FOUND!', self.ref, self.beg, self.end)

            ## F) Another possibility (Don´t do anything, leave previous. Later we may need to include new conditions)

    def determine_SV_type(self, minINDELlen, technology, outDir): 
        '''
        Determine the type of structural variant (SV) supported by the metacluster and select a consensus metacluster supporting sequence and event

        SV types:
            INSERTION: 
                        >>>>>>>>>>>>>/////INS/////>>>>>>>>>>>    * Completely spanned insertion
                        >>>>>>>>>>>>>/////INS///                 * Insertion partially spanned
                                      ////INS/////>>>>>>>>>>>
            DELETION:   >>>>>>>>>>>>>-----DEL----->>>>>>>>>>>    

            DUPLICATION (TO DO) 
            INVERSION (TO DO)
            BREAK END (TO DO)      

        Input:
            1. minINDELlen: minimum INS and DEL lenght
            2. technology: sequencing technology (NANOPORE, PACBIO or ILLUMINA)
            3. outDir: Output directory

        Output, set the following object attributes:
            
            1. SV_type: structural variant type supported by the metacluster. None if sv type 
            2. consensusEvent: consensus event attribute 
            3. consensusFasta: fasta file containing consensus metacluster supporting sequence 
        '''
        ## Create output directory 
        unix.mkdir(outDir)
        subClusterTypes = list(self.subclusters.keys())

        ## A) Metacluster supports an insertion:
        if ('INS' in subClusterTypes) and ('DEL' not in subClusterTypes):
            self.SV_type = 'INS'

            ## Select consensus INS event and sequence
            self.consensusEvent = self.subclusters['INS'].pick_median_length()
            
            self.consensusFasta = formats.FASTA()
            self.consensusFasta.seqDict[self.consensusEvent.readName] = self.consensusEvent.readSeq

        ## B) Metacluster supports a deletion:
        elif ('DEL' in subClusterTypes) and ('INS' not in subClusterTypes):
            self.SV_type = 'DEL'

            ## Select consensus DEL event and sequence
            self.consensusEvent = self.subclusters['DEL'].pick_median_length()

            self.consensusFasta = formats.FASTA()
            self.consensusFasta.seqDict[self.consensusEvent.readName] = self.consensusEvent.readSeq
                        
        ## C) Metacluster only composed by double clipping -> Long insertion candidate
        elif (len(subClusterTypes) == 2) and ('RIGHT-CLIPPING' in subClusterTypes) and ('LEFT-CLIPPING' in subClusterTypes):

            self.consensusEvent = None                

            ## Assess if clipping clusters support an insertion
            is_INS, self.consensusFasta = double_clipping_supports_INS(self.subclusters['RIGHT-CLIPPING'], self.subclusters['LEFT-CLIPPING'], minINDELlen, technology, outDir)

            ## a) Double clippings support an INS
            if is_INS:
                self.SV_type = 'INS'

            # CHANGE 2020!
            ## b) Double clippings not support an INS, but they are still BND
            else: 
                # self.SV_type = 'INS_noOverlap'
                self.SV_type = 'BND'

        ## D) Metacluster only composed by one clipping side (left or right) -> break end
        elif (len(subClusterTypes) == 1) and (('RIGHT-CLIPPING' in subClusterTypes) or ('LEFT-CLIPPING' in subClusterTypes)):

            self.SV_type = 'BND'

        ## E) Other combination -> Unknown SV type (Temporal, extend later)
        else:
            self.SV_type = None
            self.consensusEvent = None                
            self.consensusFasta = None
        
    def determine_INS_type(self, hits_genome, hits_splicing, repeatsDb, transducedDb, exonsDb, types2Search=['EXPANSION','DUP','INTERSPERSED','PSEUDOGENE']):
        '''
        Determine the type of insertion based on the alignments of the inserted sequence on the reference genome

        Input:
            1. hits_genome: PAF object containing inserted sequence alignments on the reference genome
            2. hits_splicing: list of bed entries containing inserted sequence alignments on the reference genome (splice-aware alignment). None if not hit found
            3. repeatsDb: bin database containing annotated repeats in the reference. None if not available
            4. transducedDb: bin database containing regions transduced by source elements. None if not available
            5. exonsDb: bin database containing annotated exons. None if not available. 
            6. types2Search: Comma separated list containing insertion types to look for. Default: ['EXPANSION','DUP','INTERSPERSED','PSEUDOGENE']

        Output: Add INS type annotation to the attribute SV_features
        ''' 
        ## 0. Abort if consensus event not available 
        if self.consensusEvent is None:
            self.SV_features['INS_TYPE'] = 'unknown'
            self.SV_features['PERC_RESOLVED'] = 0
            return 

        ## 1. Assess if input sequence corresponds to a repeat expansion
        if 'EXPANSION' in types2Search:
            is_EXPANSION, self.insertHits = self.is_expansion(hits_genome, repeatsDb)

            # Stop if insertion is a expansion
            if is_EXPANSION:
                return

        ## 2. Assess if input sequence corresponds to duplication 
        if 'DUP' in types2Search:
            is_DUP, self.insertHits = self.is_duplication(hits_genome, 100)

            # Stop if insertion is a duplication
            if is_DUP:
                return

        ## 3. Assess if input sequence corresponds to solo interspersed insertion or transduction
        # Note: return boolean as well specifying if interspersed or not
        if 'INTERSPERSED' in types2Search:

            is_INTERSPERSED, INS_features, self.insertHits = retrotransposons.is_interspersed_ins(self.consensusEvent.pick_insert(), hits_genome, repeatsDb, transducedDb)

            # Update metacluster with insertion features
            self.SV_features.update(INS_features) 

            # Stop if insertion is a interspersed insertion
            if is_INTERSPERSED:
                return    

        ## 4. Assess if input sequence corresponds to processed pseudogene insertion
        if 'PSEUDOGENE' in types2Search:
            is_PSEUDOGENE, outHits = self.is_processed_pseudogene(hits_splicing, exonsDb)

            # Stop if insertion is a processed pseudogene
            if is_PSEUDOGENE:
                return

    def determine_INS_structure(self, consensusPath, transducedPath, transducedSomaticPath, outDir):
        '''
        Infer inserted sequence structural features

        Input:
            1. consensusPath: path to fasta file containing retrotransposon consensus sequences
            2. transducedPath: path to fasta containing transduced sequences downstream of source elements
            3. transducedSomaticPath: path to fasta containing transduced sequences downstream of somatic source elements
            4. outDir: output directory
    
        Output: 
            structure: dictionary containing insertion structural properties
        '''
        ##  Skip structure inference if consensus event not available
        if self.consensusEvent is None:
            return {}

        ## 1. Read fasta files 
        #  1.1 Consensus sequences
        consensus = formats.FASTA()
        consensus.read(consensusPath)

        #  1.2 Transduced regions
        transduced = formats.FASTA()
        transduced.read(transducedPath)
            
        if transducedSomaticPath: 
            transducedSomatic = formats.FASTA()
            transducedSomatic.read(transducedSomaticPath)

        ## 2. Create fasta object containing database of sequences
        ## The database will contain the following sequences depending on the insertion type:
        ## - Solo      -> consensus sequences for the same family
        ## - Partnered -> consensus sequences for the same family
        #              -> corresponding transduced area
        ## - Orphan    -> corresponding transduced area

        ## Initialize fasta
        fasta = formats.FASTA()

        ## Add to the fasta subfamily consensus sequences for the corresponding family
        if self.SV_features['INS_TYPE'] in ['solo', 'partnered']:
            for seqId, seq in consensus.seqDict.items(): 
                family = seqId.split('|')[1]

                if family in self.SV_features['FAMILY']:
                    fasta.seqDict[seqId] = seq

        ## Add to the fasta transduced region or regions
        if self.SV_features['INS_TYPE'] in ['partnered', 'orphan']:
        
            for seqId, seq in transduced.seqDict.items(): 
                family, srcId = seqId.split('|')[1:3]

                if (family in self.SV_features['FAMILY']) and (srcId in self.SV_features['CYTOBAND']):
                    fasta.seqDict[seqId] = seq

        ## Add to the fasta somatic transduced region or regions
        if transducedSomaticPath and self.SV_features['INS_TYPE'] in ['partnered', 'orphan']:
            
            for seqId, seq in transducedSomatic.seqDict.items(): 
                family, srcId = seqId.split('|')[1:3]

                if (family in self.SV_features['FAMILY']) and (srcId in self.SV_features['CYTOBAND']):
                    fasta.seqDict[seqId] = seq

        ##  Skip structure inference if empty database of sequences
        if not fasta.seqDict:
            return {}

        ## 3. Create fasta file
        fastaPath = outDir + '/reference_sequences.fa'
        unix.mkdir(outDir)
        fasta.write(fastaPath)

        ## 4. Index fasta file
        fileName = 'reference_sequences'  
        indexPath = alignment.index_minimap2(fastaPath, fileName, outDir)

        ## 5. Create fasta file containing consensus inserted sequence
        # Create fasta object
        FASTA = formats.FASTA()
        insert = self.consensusEvent.pick_insert()
        FASTA.seqDict['consensus_insert'] = insert

        # Write fasta
        insertPath = outDir + '/consensus_insert.fa'
        FASTA.write(insertPath)    

        ## 6. Structure inference        
        structure = retrotransposons.retrotransposon_structure(insertPath, indexPath, outDir)

        return structure

    def is_duplication(self, PAF, buffer):
        '''
        Determine if metacluster corresponds to a short tandem duplication
        
        Input:
            1. PAF: PAF object containing consensus inserted sequence alignments on the reference genome
            2. buffer: number of base pairs to extend metacluster interval when assessing overlap

        Output:
            1. DUP: boolean specifying if inserted sequence corresponds to a duplication (True) or not (False)
            2. HITS: PAF object containing inserted sequence alignments supporting a duplication. None if no hit found

        Update SV_features attribute with 'INS_TYPE' and 'PERC_RESOLVED' info

        Note: I need to modify the way PERC_RESOLVED. Now it´s not precise, do it based on the qBeg and qEnd
        alignment coordinates
        '''
        ## 0. Abort if no hit available
        if not PAF.alignments:
            DUP = False
            self.SV_features['INS_TYPE'] = 'unknown'
            self.SV_features['PERC_RESOLVED'] = 0

            return DUP, None   

        ## 1. Initialize
        totalPerc = 0
        HITS = formats.PAF()
        
        ## 2. Search hits matching metacluster interval
        # For each hit
        for hit in PAF.alignments: 

            ## Hit in the same ref as the metacluster
            if hit.tName == self.ref: 
                
                overlap, nbBp = gRanges.overlap(self.beg - buffer, self.end + buffer, hit.tBeg, hit.tEnd)
                perc = float(nbBp) / hit.qLen * 100

                ## Hit within metacluster interval
                if overlap:
                    totalPerc += perc
                    HITS.alignments.append(hit)

        ## set upper bound to 100
        if totalPerc > 100:
            totalPerc = 100 

        ## 3. Determine if duplication or not
        # a) Duplication
        if totalPerc >= 40:

            DUP = True
            self.SV_features['INS_TYPE'] = 'duplication'
            self.SV_features['PERC_RESOLVED'] = totalPerc

        # b) Not duplication
        else:

            DUP = False
            self.SV_features['INS_TYPE'] = 'unknown'
            self.SV_features['PERC_RESOLVED'] = 0

        return DUP, HITS

    def is_expansion(self, PAF, repeatsDb):
        '''
        Determine if metacluster corresponds to a repeat expansion
        
        Input:
            1. PAF: PAF object containing consensus inserted sequence alignments on the reference genome
            2. repeatsDb: bin database containing annotated repeats in the reference

        Output:
            1. EXPANSION: Boolean specifying if inserted sequence corresponds to an expansion (True) or not (False)
            2. HITS: PAF object containing inserted sequence alignments supporting an expansion. None if no hit found

        Update SV_features attribute with 'INS_TYPE', 'FAMILY', 'SUBFAMILY', 'PERC_RESOLVED' info

        Note: I need to modify the way PERC_RESOLVED. Now it´s not precise, do it based on the qBeg and qEnd
        alignment coordinates        
        '''

        ## 0. Abort if no hit or repeats database not available
        if (not PAF.alignments) or (repeatsDb is None):

            EXPANSION = False
            self.SV_features['INS_TYPE'] = 'unknown'
            self.SV_features['PERC_RESOLVED'] = 0

            return EXPANSION, None   
            
        ## 1. Initialize
        totalPerc = 0
        HITS = formats.PAF()

        ## 2. Assess if metacluster located over an annotated repeat sequence 
        # Make list of annotated repeat categories according to repeatmasker
        repeatTypes = ['Low_complexity', 'Simple_repeat', 'Satellite', 'telo', 'centr', 'acro']

        # Selecting those annotated repeats over the target region 
        repeatsFiltered = [ repeat for repeat in self.repeatAnnot if repeat['distance'] == 0 ]

        # Make list of annotated families at the target region
        targetFamilies = [ repeat['family'] for repeat in repeatsFiltered ]
        targetSubfamilies = [ repeat['subfamily'] for repeat in repeatsFiltered ]

        # A) Metacluster over annotated repeat
        if any(family in repeatTypes for family in targetFamilies):

            families = []
            subfamilies = []

            ### Search for inserted sequence hits on repeats of the same family
            # For each hit
            for hit in PAF.alignments: 

                ## Intersect hit coordinates with annotated repeats
                overlaps = annotation.annotate_interval(hit.tName, hit.tBeg, hit.tEnd, repeatsDb)
                
                ## For each repeat overlapping hit coordinates
                for overlap in overlaps:
                    event, nbBp, percBp, coord = overlap
                    
                    # Repeat belonging to the same family as the ones at metacluster interval
                    if event.optional['family'] in targetFamilies:
                        perc = float(nbBp) / hit.qLen * 100
                        totalPerc += perc
                        families.append(event.optional['family'])
                        subfamilies.append(event.optional['subfamily'])

                        if hit not in HITS.alignments:
                            HITS.alignments.append(hit)

            ## set upper bound to 100
            if totalPerc > 100:
                totalPerc = 100 

            ## Determine if expansion or not
            # a) Expansion
            if totalPerc >= 40:

                EXPANSION = True
                self.SV_features['INS_TYPE'] = 'expansion'
                self.SV_features['PERC_RESOLVED'] = totalPerc
                self.SV_features['FAMILY'] = list(set(families))
                self.SV_features['SUBFAMILY'] = list(set(subfamilies))

            # b) Not expansion
            else:

                EXPANSION = False
                self.SV_features['INS_TYPE'] = 'unknown'
                self.SV_features['PERC_RESOLVED'] = 0

        # B) Metacluster outside annotated repeat
        else:
            EXPANSION = False
            self.SV_features['INS_TYPE'] = 'unknown'
            self.SV_features['PERC_RESOLVED'] = 0

        return EXPANSION, HITS     

    def is_processed_pseudogene(self, hits, exonsDb):     
        '''
        Determine if metacluster corresponds to a processed pseudogene insertion
        
        Input:
            1. hits: list of bed entries corresponding to inserted sequence hits on the reference 
            2. exonsDb: bin database containing annotated exons in the reference. None if not available

        Output:
            1. PSEUDOGENE: Boolean specifying if inserted sequence corresponds to an expansion (True) or not (False)
            2. outHits: list of bed entries corresponding to hits matching annotated exons
        
        Update SV_features attribute with 'INS_TYPE', 'PERC_RESOLVED', 'NB_EXONS', 'SOURCE_GENE', 'POLYA', 'STRAND'

        Note: I need to modify the way PERC_RESOLVED. Now it´s not precise, do it based on the qBeg and qEnd
        alignment coordinates        
        '''

        ## 0. Abort if no hit available
        if (not hits) or (exonsDb is None):

            PSEUDOGENE = False
            self.SV_features['INS_TYPE'] = 'unknown'
            self.SV_features['PERC_RESOLVED'] = 0

            return PSEUDOGENE, None   

        ## 1. Search for polyA/T tails
        insert = self.consensusEvent.pick_insert()
        windowSize = 8
        maxWindowDist = 2
        minMonomerSize = 10
        minPurity = 80 

        # 1.1 Search for poly(A) monomers on the 3' end 
        targetSeq = insert[-50:]
        targetMonomer = 'A'
        monomers3end = sequences.find_monomers(targetSeq, targetMonomer, windowSize, maxWindowDist, minMonomerSize, minPurity)

        ## 1.2 Search for polyT on the 5' end 
        targetSeq = insert[:50]
        targetMonomer = 'T'
        monomers5end = sequences.find_monomers(targetSeq, targetMonomer, windowSize, maxWindowDist, minMonomerSize, minPurity)

        if monomers3end and monomers5end:
            polyA = False
            strand = None

        elif monomers3end:
            polyA = True
            strand = '+'

        elif monomers5end:
            polyA = True
            strand = '-'

        else:
            polyA = False
            strand = None

        ## 2. Intersect each hit with the annotated exons
        outHits = []
        nbResolvedBp = 0
        nbExons = 0

        # For each hit
        for hit in hits: 

            hit.annot = {}

            ## Do intersection
            overlaps = annotation.annotate_interval(hit.ref, hit.beg, hit.end, exonsDb)

            ## Hit intersect with exon
            if overlaps:
                longestOverlap = overlaps[0] # Select exon with longest overlap
                hit.annot['EXON'] = longestOverlap[0] 
                
                outHits.append(hit)
                nbResolvedBp += longestOverlap[1]
                nbExons += 1

        ## 3. Compute percentage of inserted sequence matching on annotated exons
        percResolved = float(nbResolvedBp) / self.consensusEvent.length * 100

        # set upper bound to 100
        if percResolved > 100:
            percResolved = 100 

        ## 4. Determine if pseudogene or not
        # a) Pseudogene (enough % of sequence resolved + polyA)
        if (percResolved >= 60) and (polyA):

            PSEUDOGENE = True
            self.SV_features['INS_TYPE'] = 'pseudogene'
            self.SV_features['PERC_RESOLVED'] = percResolved
            self.SV_features['NB_EXONS'] = nbExons
            self.SV_features['SOURCE_GENE'] = list(set([hit.annot['EXON'].optional['geneName'] for hit in outHits]))
            self.SV_features['POLYA'] = polyA
            self.SV_features['STRAND'] = strand

        # b) Not pseudogene
        else:
            PSEUDOGENE = False
            self.SV_features['INS_TYPE'] = 'unknown'
            self.SV_features['PERC_RESOLVED'] = percResolved        

        return PSEUDOGENE, outHits

    def setElement(self):
        self.element = None
        for event in self.events:
            if hasattr(event, 'element'):
                self.element = event.element
                break
        return self.element
    
class BRIDGE():
    '''
    Rearrangement bridge class
    '''
    def __init__(self, supplClusters, info):

        self.supplClusters = supplClusters
        self.bridgeType = info['bridgeType']
        self.family = info['family']
        self.srcId = info['srcId']
        self.bridgeSeq = info['bridgeSeq']
        self.bridgeLen = info['bridgeLen']
        self.polyA = info['polyA']
        self.qHits = info['qHits'] if 'qHits' in info else []
        self.percRes = info['percRes'] if 'percRes' in info else None
        self.mechanism = info['mechanism'] if 'mechanism' in info else None
        self.strand = info['strand'] if 'strand' in info else None

    def add(self, supplClusters):
        '''
        Incorporate additional supplementary clusters into the bridge

        Input:
            1. supplClusters: list of supplementary clusters
        '''
        self.supplClusters.extend(supplClusters)
        
    def nbClusters(self):
        '''
        Return the number of supplementary clusters composing the bridge
        '''
        return len(self.supplClusters)

    def readIds(self):
        '''
        Return a list containing bridge supporting reads 
        '''
        ## Collect read ids supporting the suppl. clusters composing the bridge
        readIdsNested = [cluster.nbReads()[1] for cluster in self.supplClusters]
        readIds = list(itertools.chain.from_iterable(readIdsNested))

        ## Remove redundant read ids
        readIds = list(set(readIds))

        return readIds

    def nbReads(self):
        '''
        Return the number of reads supporting a rearrangement bridge
        '''

        ## Collect read ids supporting the suppl. clusters composing the bridge
        readIdsNested = [cluster.nbReads()[1] for cluster in self.supplClusters]
        readIds = list(itertools.chain.from_iterable(readIdsNested))

        ## Remove redundant read ids
        readIds = list(set(readIds))

        ## Compute number of distinct reads supporting the bridge
        nbReads = len(readIds)

        return nbReads
            
    def support_types(self):
        '''
        Return list of supplementary clusters types supporting the bridge (aligned or unaligned)
        '''
        supportTypes = []

        for cluster in self.supplClusters:

            if cluster.bridgeInfo['supportType'] not in supportTypes:
                supportTypes.append(cluster.bridgeInfo['supportType'])

        return supportTypes

    def return_unaligned_cluster(self):
        '''
        Return supplementary cluster supporting unaligned bridge
        '''
        targetCluster = None

        for cluster in self.supplClusters:
        
            if cluster.bridgeInfo['supportType'] == 'unaligned':
                targetCluster = cluster
        
        return targetCluster


class BND_junction():
    '''
    Rearrangement bridge class
    '''
    def __init__(self, metaclusterA, metaclusterB):

        self.id = 'JUNCTION_' + str(uuid.uuid4())

        self.metaclusterA = metaclusterA
        self.metaclusterB = metaclusterB

        ## Initialize bridge information
        self.bridge = None

        ## Consensus information
        self.junctionConsSeq = None
        self.bkpsConsSupport = None # Boolean telling if metaclsuters bkps are supported by consensus realignment with buffr of 100 bp

        self.identity = None

    def junctionCoord(self):
        '''
        Return a string containing junction coordinates with the following format: 
            refA:bkpCoordA-refB:bkpCoordB
        '''
        coord = self.metaclusterA.ref + ':' + str(self.metaclusterA.bkpPos) + '-' + self.metaclusterB.ref + ':' + str(self.metaclusterB.bkpPos)
        return coord

    def junctionCoord_rev(self):
        '''
        Return a string containing junction coordinates with the following format: 
            refB:bkpCoordB-refA:bkpCoordA        
        '''
        coord = self.metaclusterB.ref + ':' + str(self.metaclusterB.bkpPos) + '-' + self.metaclusterA.ref + ':' + str(self.metaclusterA.bkpPos)
        return coord

    def junctionType(self):
        '''
        Determine BND junctions type.

        Output:
            1. junctionType: intrachromosomal or interchromosomal (for now... later make code able to differenciate tanden dup, inversions, ...)
        '''

        # 1) Intrachromosomal
        if self.metaclusterA.ref == self.metaclusterB.ref:
            junctionType = 'intrachromosomal'

        # 2) Interchromosomal 
        else:
            junctionType = 'interchromosomal'

        return junctionType             
        
    def supportingReads(self):
        '''
        Compute the number of reads supporting the BND junction

        Output:
            1. nbTotal: Total number of BND junction supporting reads
            2. nbTumour: Number of BND junction supporting reads in the tumour 
            3. nbNormal: Number of BND junction supporting reads in the normal
        '''
        ## Total number of reads
        nbTotal = len(set(self.metaclusterA.reads + self.metaclusterB.reads))

        ## Number of reads in the tumour

        # NOTE 2020: change 2020 check it!!!
        if self.metaclusterA.readsTumour != None:


            nbTumour = len(set(self.metaclusterA.readsTumour + self.metaclusterB.readsTumour))

            ## Number of reads in the normal
            nbNormal = len(set(self.metaclusterA.readsNormal + self.metaclusterB.readsNormal))
        
        else:
            nbTumour = None
            nbNormal = None


        return nbTotal, nbTumour, nbNormal

    def polish_junctions(self, confDict, reference, outDir, assemblyStep = True):
        '''
        Polish metacluster consensus sequence

        Input: 
            1. confDict: 
                * technology     -> sequencing technology (NANOPORE, PACBIO or ILLUMINA)
                * rounds         -> number of polishing rounds to be attempled. 0 means no polishing
            
            2. reference: path to reference genome in fasta format    
            3. outDir: Output directory
        
        Output: Update 'consensusFasta' attribute with the polished sequence
        '''
        ## 0. Create directory 
        unix.mkdir(outDir)
        
        # split metaclusters
        clusterA = self.metaclusterA
        clusterB = self.metaclusterB

        # create attr to preserve first raw consensus
        clusterA.consensusFasta_raw = clusterA.consensusFasta
        clusterB.consensusFasta_raw = clusterB.consensusFasta

        ## 1.1 Collect metacluster supporting reads and write a fasta
        supportingReadsA = clusterA.collect_altSeqs()
        supportingReadsB = clusterB.collect_altSeqs()
        supportingReads = formats.FASTA()
        supportingReads.seqDict = {**supportingReadsA.seqDict, **supportingReadsB.seqDict}

        supportingReadsFasta = outDir + '/supportingReads_altSeq.fa'
        supportingReads.write(supportingReadsFasta)

        ## 1.2 Run wtbgs2 to obtain a consensus
        if assemblyStep:
            unpolishedFasta = assembly.cons_wtdbg2(supportingReadsFasta, outDir, confDict['processes'], confDict['technology_subtype'])
            
        else:
            unpolishedFasta = None
            
        ## If wtbgs2 fails, use median insert size read as consensus to be polished
        if unpolishedFasta is None:
            
            ## 1.1 Write raw consensus sequence
            unpolishedFasta = outDir + '/raw_consensus.fa'

            # substring consensus read around bkp
            consensusReadName = list(clusterA.consensusFasta.seqDict.keys())[0]
            consensusBkp = [event.readBkp for event in clusterA.events if event.readName == consensusReadName][0]
            
            buffer = 5000
            if hasattr(clusterA, 'bridgeSeq'):  
                lenBridge = len(clusterA.bridgeSeq)

            else:
                lenBridge = 1000
                            
            if clusterA.strand == "+":
                bkp1 = consensusBkp - buffer
                bkp2 = consensusBkp + buffer + lenBridge

            else:
                bkp1 = consensusBkp - buffer - lenBridge
                bkp2 = consensusBkp + buffer
            
            if bkp1 < 0: bkp1 = 0

            # update consensus sequence
            consensusSubstring = clusterA.consensusFasta.seqDict[consensusReadName][bkp1:bkp2]
            clusterA.consensusFasta.seqDict[consensusReadName] = consensusSubstring

            clusterA.consensusFasta.write(unpolishedFasta)

            ## 1.2 Remove consensus from FASTA
            if consensusReadName in supportingReads.seqDict:
                del supportingReads.seqDict[consensusReadName]

            ## Write supporting reads FASTA
            supportingReads.write(supportingReadsFasta)
    
        ## 2. Consensus polishing using Racon
        polishedFasta = assembly.polish_racon(unpolishedFasta, supportingReadsFasta, confDict['technology'], confDict['rounds'], outDir)
            
        ## If polishing is successful replace consensus by new polished sequence
        if polishedFasta is not None:
            
            polished = formats.FASTA()
            polished.read(polishedFasta)  
            self.consensusFasta = polished
        
        else:
            
            self.consensusFasta = clusterA.consensusFasta

        return 

    def consensus_event_junctions(self, confDict, reference, offset, outDir):
        '''
        Define metacluster´s consensus event based on consensus sequence realignment

        Input: 
            1. confDict: 
                * targetSV       -> list with target SV (INS: insertion; DEL: deletion; CLIPPING: left and right clippings)
                * minMAPQ        -> minimum mapping quality
                * minCLIPPINGlen -> minimum clipping lenght
                * minINDELlen    -> minimum INS and DEL lenght
                * overhang       -> Number of flanking base pairs around the INDEL events to be collected from the supporting read. If 'None' the complete read sequence will be collected)            2. outDir: output directory
            
            2. reference: path to reference genome in fasta format    
            3. offset: number of base pairs to extend cluster begin and end coordinates when defining target region for consensus sequence realignment
            4. outDir: Output directory
        
        Output: Update 'consensusEvent' attribute in the metacluster class
        '''

        ## 1. Local realignment of the consensus sequence into the SV genomic interval ##
        ## 1.1 Write consensus sequence into a fasta file 
        consensusFile = outDir + '/consensus.fa'
        self.consensusFasta.write(consensusFile)

        ## 1.2 Define SV cluster genomic interval
        # ------------------<***SV_cluster***>-----------------
        #       <--offset-->                  <--offset-->
        intervalBeg = self.beg - offset
        intervalBeg = intervalBeg if intervalBeg >= 0 else 0 ## Set lower bound
        intervalEnd = self.end + offset
        intervalCoord = self.ref + ':' + str(intervalBeg) + '-' + str(intervalEnd)
            
        ## 1.3 Do realignment
        # ------------------<***SV_cluster***>-----------------
        #         -------------consensus_seq-------------
        BAM = alignment.targeted_alignment_minimap2(consensusFile, intervalCoord, reference, outDir, 'BAM')

        ## Continue if realignment is succesfull 
        if BAM is not None:
            
            ## 2. Search for metacluster´s consensus event  ##
            ## 2.1 Extract events from consensus sequence realignment
            # ------------------<***SV_cluster***>-----------------
            #        -------------consensus_seq-------------
            #               <--->----------------<---> overhang (100 bp)
            #                   event_search_space         
            overhang = 100
            clusterIntervalLen = self.end - self.beg
            targetBeg = offset - overhang
            targetEnd = offset + clusterIntervalLen + overhang
            confDict['minCLIPPINGlen'] = 200 # By default set to 500 bp. It won't detect short insertions 
            eventsDict = bamtools.collectSV(intervalCoord, targetBeg, targetEnd, BAM, confDict, None)
            
            # filter eventsDict by selecting all alignments corresponding to longest contig for each type  [INS, RIGHT-CLIPPING, LEFT-CLIPPING]
            for key in eventsDict:
                eventsList = eventsDict[key]
                
                if eventsList != []:
                    # get name of the longest read (contig)
                    longestName=sorted(eventsList, key=lambda event: len(event.readSeq), reverse=True)[0].readName
                    # select only alignments of the longest read (contig)
                    eventsDict[key] = [event for event in eventsList if event.readName == longestName]
            
            ## 2.2 Define consensus event based on the events resulting from consensus sequence realignment
            ## A) Metacluster supports an INS and realignment leads to one INS event 
            if (self.SV_type == 'INS') and ('INS' in eventsDict) and (len(eventsDict['INS']) == 1):

                ## Convert coordinates
                self.consensusEvent = alignment.targetered2genomic_coord(eventsDict['INS'][0], self.ref, intervalBeg)

            ## B) Metacluster supports an INS and realignment leads to multiple INS events
            elif (self.SV_type == 'INS') and ('INS' in eventsDict) and (len(eventsDict['INS']) > 1):

                ## Do merging
                merged = events.merge_INS(eventsDict['INS'])

                ## Convert coordinates
                self.consensusEvent = alignment.targetered2genomic_coord(merged, self.ref, intervalBeg)

            ## C) Metacluster supports a DEL and realignment leads to one DEL event 
            elif (self.SV_type == 'DEL') and ('DEL' in eventsDict) and (len(eventsDict['DEL']) == 1):

                ## Convert coordinates
                self.consensusEvent = alignment.targetered2genomic_coord(eventsDict['DEL'][0], self.ref, intervalBeg)

            ## D) Metacluster supports a DEL and realignment leads to multiple DEL events (TO DO)
            #elif (self.SV_type == 'DEL') and (len(eventsDict['DEL']) > 1):
                    ## Do merging
                    ## Convert coordinates
                        
            ## E) Metacluster supports an INS and realignment leads to one left and one right CLIPPING 
            elif (self.SV_type == 'INS') and ('LEFT-CLIPPING' in eventsDict) and ('RIGHT-CLIPPING' in eventsDict) and (len(eventsDict['RIGHT-CLIPPING']) == 1) and (len(eventsDict['LEFT-CLIPPING']) == 1):

                ## Compute the inserted sequence length as the difference between both clipping breakpoints in the long sequence 
                rightClipping = eventsDict['RIGHT-CLIPPING'][0]
                leftClipping = eventsDict['LEFT-CLIPPING'][0]
                length = leftClipping.readBkp - rightClipping.readBkp

                ## Create consensus INS from clipping alignments if inserted sequence found
                # and ins length >= minINDELlen and both clippings come from the same contig (fully cover the inserted seq)
                if (length >= confDict['minINSlen']) and (rightClipping.readName == leftClipping.readName):

                    event = events.INS(rightClipping.ref, rightClipping.beg, rightClipping.end, length, rightClipping.readName, rightClipping.readSeq, rightClipping.readBkp, None, None)
        
                    ## Convert coordinates
                    self.consensusEvent = alignment.targetered2genomic_coord(event, self.ref, intervalBeg)
                    
                else:
                    
                    self.consensusEvent = None
                    print('INS_NOT_FOUND!', self.ref, self.beg, self.end)

            ## F) Another possibility (Don´t do anything, leave previous. Later we may need to include new conditions)

    def extractSupportingRead(self):
        '''
        Method for extracting the events that completely span the BND_junction 
        Output: 
            - junctionsList:
                - field 0: list of metaclusters that contain the same read
                - field 1: times that the read appears in a different events
                - field 2: number of supplementary alignments of the read.
        '''

        readNamesJunction={}
        '''
        - Dictionary:
        - keys: read names supporting the BND_junction
        - values: list:
            - field 0: list containing event objects corresponding to read name (only metaclsuterA and metaclsuterB events are kept, no bridge events)
            - field 1: number of times that this read name is repeated in the BND_junction components
            - field 2: number of read supplementary alignments 
        '''

        # 1. Make a list of readNames of events of metaclusterA
        for event in self.metaclusterA.events:
            try:
                readNamesJunction[event.readName]=[[event], 1, event.supplAlignment.count(';')]
            except AttributeError:
                readNamesJunction[event.readName]=[[event], 1, 0]

        # 2. Add to the list of readNames those from events of metaclusterB
        for event in self.metaclusterB.events:
            try:
                readNamesJunction[event.readName][0].append(event)
                readNamesJunction[event.readName][1] = readNamesJunction[event.readName][1] + 1
            except KeyError:
                try:
                    readNamesJunction[event.readName]=[[event], 1, event.supplAlignment.count(';')]
                except AttributeError:
                    readNamesJunction[event.readName]=[[event], 1, 0]

        if self.bridge:
            # 3. Add to the list of readNames those from events of bridge
            for SUPPLEMENTARY_cluster in self.bridge.supplClusters:
                for event in SUPPLEMENTARY_cluster.events:
                    try:
                        readNamesJunction[event.readName][1] = readNamesJunction[event.readName][1] + 1
                    except KeyError:
                        readNamesJunction[event.readName]=[event, 1, event.supplAlignment.count(';')]
        
        # TODO: delete readNamesJunction

        # 4. Make list from dictionary, excluding readNames.
        junctionsList = []
        for junctionsLists in readNamesJunction.values():
            junctionsList.append(junctionsLists)
        junctionsList = sorted(junctionsList, key = lambda x: (-x[1], x[2]))

        return junctionsList

    def get_consensus_BNDInterval(self, polishedFastaEntireSequence, junctionInterval, outDir):
        '''
        From a BND_junction consensus fasta file, get only the sequence containing bkps +- 1000
        Input:
            - polishedFastaEntireSequence: consensus fasta file of the entire sequence
            - junctionInterval: bkps of BND_junction
        Output:
            - polishedFastaInterval: Path to the consensus BND_junction fasta file
            - polishedFastaIntervalObj: object of the consensus BND_junction fasta file
        '''

        # Pick from polished fasta only the region involving the junction (using a buffer of 1000bp)
        polishedFastaIntervalDict = {}
        polishedFastaObj = formats.FASTA()
        polishedFastaObj.read(polishedFastaEntireSequence)
        for key, value in polishedFastaObj.seqDict.items():
            beg = min(junctionInterval)-5000 if min(junctionInterval) > 5000 else 0
            polishedFastaIntervalDict[key]=value[beg:max(junctionInterval)+5000]

        polishedFastaInterval = polishedFastaEntireSequence + 'fasta' 
        polishedFastaIntervalObj = formats.FASTA()
        polishedFastaIntervalObj.seqDict = polishedFastaIntervalDict
        polishedFastaIntervalObj.write(polishedFastaInterval)

        return polishedFastaInterval, polishedFastaIntervalObj
    
def metacluster_mate_suppl(discordants, leftClippings, rightClippings, minReads, refLengths):
    '''
    Group discordant and clipping clusters into metaclusters

    Input:
        1. discordants: list of discordant cluster objects
        2. leftClippings: list of left clipping cluster objects
        3. rightClippings: list of right clipping cluster objects
        4. minReads: minimum number of reads
        5. refLengths: dictionary containing references as keys and their lengths as values

    Output:
        1. filteredMeta: list of metaclusters
    '''
    ## 1. Create list of discordant mate clusters
    mateClusters = [discordant.create_matesCluster() for discordant in discordants]
    
    ## 2. Create list of supplementary clusters
    supplClustersLeft = [clipping.supplCluster for clipping in leftClippings]
    supplClustersRight = [clipping.supplCluster for clipping in rightClippings]
    supplClusters = supplClustersLeft + supplClustersRight
    
    ## 2.1. Fill supplementary cluster orientation attribute
    [supplCluster.clippOrientation() for supplCluster in supplClusters]
    
    ## 3. Organize discordant mate and suppl. clusters into a dictionary
    mateDict = events.events2nestedDict(mateClusters, 'DISCORDANT')
    supplDict = events.events2nestedDict(supplClusters, 'SUPPLEMENTARY')
    clustersDict = events.mergeNestedDict(mateDict, supplDict)

    ## 4. Organize discordant mate and suppl. clusters into a bin database
    wgBinDb = structures.create_bin_database(refLengths, clustersDict)

    ## 5. Perform metaclustering
    allMeta = []

    for binDb in wgBinDb.values():
        
        meta = clustering.reciprocal_overlap_clustering(binDb, 1, 1, binDb.eventTypes, 500, 'META')
        allMeta = allMeta + meta

    ## 6. Filter metaclusters based on read support
    filteredMeta = []

    for meta in allMeta:

        if meta.supportingReads()[0] >= minReads:
            filteredMeta.append(meta)
    
    return filteredMeta

