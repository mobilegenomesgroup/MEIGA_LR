## DEPENDENCIES ##
# External
import pybedtools
import mappy as mp
from ordered_set import OrderedSet

# Internal
from GAPI import formats
from GAPI import unix

def INS2VCF(metaclusters, index, refLengths, source, build, species, outName, outDir, extraFields):
    '''
    Write INS calls into a VCF file

    Input:
        1. metaclusters: list containing list of INS metaclusters
        2. index: minimap2 index for the reference genome 
        3. refLengths: Dictionary containing reference ids as keys and as values the length for each reference
        4. source: software version used to generate the insertion calls
        5. build: reference genome build
        6. species: specie
        7. outName: Output file name
        8. outDir: Output directory

    Output: vcf file containing identified metaclusters
    '''
    ## 1. Initialize VCF 
    VCF = formats.VCF()

    ## 2. Create header
    ## Define info 
    info = {'FAM': ['.', 'String', 'ME family'], \
            'SUBFAM': ['.', 'String', 'ME subfamily'], \
            'ITYPE': ['.', 'String', 'MEI type'], \
            'CYTOID': ['.', 'String', 'Source element identifier'], \
            'VTYPE': ['.', 'String', 'Origin'], \
            'LEN': ['1', 'Integer', 'Insertion length'], \
            'POLYA': ['0', 'Flag', 'PolyA tail identified'], \
            'STRAND': ['.', 'String', 'Insertion DNA strand (+ or -)'], \
            'MECHANISM': ['.', 'String', 'Insertion mechanism'], \
            'PERCR': ['1', 'Float', 'Percentage of inserted sequence that has been resolved'], \
            'FULL': ['0', 'Flag', 'Full length mobile element'], \
            'GERMDB': ['.', 'String', 'Germline MEI databases where the variant was reported'], \
            'SRCGENE': ['.', 'String', 'Source gene for a processed psendogene insertion'], \
            'NBEXONS': ['1', 'Integer', 'Number of exons for a processed pseudogene insertion'], \
            'NBTOTAL': ['1', 'Integer', 'Total number of insertion supporting reads'], \
            'NBTUMOR': ['1', 'Integer', 'Number of insertion supporting reads in the tumour'], \
            'NBNORMAL': ['1', 'Integer', 'Number of insertion supporting reads in the normal'], \
            'REGION': ['.', 'String', 'Genomic region where insertion occurs'], \
            'GENE': ['.', 'String', 'HUGO gene symbol'], \
            'REP': ['.', 'String', 'Families for annotated repeats at the insertion region'], \
            'REPSUB': ['.', 'String', 'Subfamilies for annotated repeats at the insertion region'], \
            'REPDIV': ['.', 'String', 'milliDivergence for annotated repeats at the insertion region'], \
            'DIST': ['1', 'Integer', 'Distance between insertion breakpoint and annotated repeat'], \
            'QHITS': ['.', 'String', 'Coordinates for inserted sequence hits on the reference'], \
            'INSEQ': ['.', 'String', 'Inserted sequence'] }

    if extraFields:

        info['CIPOS'] = ['2', 'Integer', 'Confidence interval around POS for imprecise variants']
        info['TSD'] = ['.', 'String', 'Target Site Duplication']
        info['CV'] = ['1', 'Float', 'Length coefficient of variation']
        info['RTLEN'] = ['1', 'Integer', 'Inserted retrotransposon length']
        info['TRUN5LEN'] = ['1', 'Integer', 'Size of 5prime truncation']
        info['TRUN3LEN'] = ['1', 'Integer', 'Size of 3prime truncation']
        info['TDLEN'] = ['1', 'Integer', 'Transduction length']
        info['TDLEN'] = ['1', 'Integer', 'Transduction length']
        info['INVLEN'] = ['1', 'Integer', '5-inversion length']
        info['RTCOORD'] = ['.', 'String', 'Coordinates for inserted retrotransposon piece of sequence']
        info['END5'] = ['1', 'Integer', 'Metacluster 5 end coordinate']
        info['END3'] = ['1', 'Integer', 'Metacluster 3 end coordinate']
        info['NBSPAN'] = ['1', 'Integer', 'Number of spanning supporting reads']
        info['NBCLIP'] = ['1', 'Integer', 'Number of clipping supporting reads']
        info['NBCLIP_5END'] = ['1', 'Integer', 'Number of clipping supporting reads at the 5 end']
        info['NBCLIP_3END'] = ['1', 'Integer', 'Number of clipping supporting reads at the 3 end']
        info['DEPTH'] = ['1', 'Integer', 'Total number of reads at POS (tumour when paired)']
        info['DEPTH_5END'] = ['1', 'Integer', 'Total number of reads at the 5 end (tumour when paired)']
        info['DEPTH_3END'] = ['1', 'Integer', 'Total number of reads at the 3 end (tumour when paired)']
        info['HP1'] = ['.', 'String', 'Number of ALT tumour reads from haplotype 1']
        info['THP1'] = ['.', 'String', 'Total number of tumour reads from haplotype 1']
        info['HP2'] = ['.', 'String', 'Number of ALT tumour reads from haplotype 2']
        info['THP2'] = ['.', 'String', 'Total number of tumour reads from haplotype 2']
        info['HPX'] = ['.', 'String', 'Number of ALT tumour reads with no haplotype assigned']
        info['THPX'] = ['.', 'String', 'Total number of tumour reads with no haplotype assigned']
        info['PS'] = ['.', 'String', 'List of phasing set values']
        info['READIDS'] = ['1', 'String', 'All supporting reads identifiers']
                
    ## Define VCF filters
    filters = {'PERC-RESOLVED': 'Percentage resolved', 'SCORE': 'Low score variant'}   

    ## Create header
    VCF.create_header(source, build, species, refLengths, info, filters, {}, None)

    ## 3. Add insertion calls to the VCF
    ## 3.1 Load reference index
    # reference = mp.Aligner(fn_idx_in=index) # comment as time consuming
    
    ## 3.2 Iterate over INS metaclusters
    ID = 0
    for metacluster in metaclusters:

        ## Collect insertion basic features
        CHROM = metacluster.ref
        POS, CIPOS = metacluster.median_pos()
        ID += 1
        # REF = reference.seq(CHROM, POS, POS + 1)
        REF = 'N'
        ALT = '<INS>'
        QUAL = '.'
        FILTER = 'PASS' if not metacluster.failedFilters else ','.join(metacluster.failedFilters)
        
        ## Collect extra insertion features to include at info field
        INFO = {}
        repeats = metacluster.repeatAnnot if hasattr(metacluster, 'repeatAnnot') else []
        region, gene = metacluster.geneAnnot if hasattr(metacluster, 'geneAnnot') else ("None", "None")      
    
        INFO['FAM'] = ','.join(metacluster.SV_features['FAMILY']) if ('FAMILY' in metacluster.SV_features and metacluster.SV_features['FAMILY']) else None
        INFO['SUBFAM'] = ','.join(metacluster.SV_features['SUBFAMILY']) if ('SUBFAMILY' in metacluster.SV_features and metacluster.SV_features['SUBFAMILY']) else None
        INFO['ITYPE'] = metacluster.SV_features['INS_TYPE'] if 'INS_TYPE' in metacluster.SV_features else None
        if INFO['ITYPE'] == "poly(A/T)": 
            INFO['FAM'] = "poly(A/T)"
            INFO['SUBFAM'] = "."
        INFO['CYTOID'] = ','.join(metacluster.SV_features['CYTOBAND']) if ('CYTOBAND' in metacluster.SV_features and metacluster.SV_features['CYTOBAND']) else None
        INFO['VTYPE'] = metacluster.mutOrigin
        INFO['LEN'] = metacluster.consensusEvent.length if metacluster.consensusEvent is not None else None
        INFO['POLYA'] = metacluster.SV_features['POLYA'] if 'POLYA' in metacluster.SV_features else None
        INFO['STRAND'] = metacluster.SV_features['STRAND'] if 'STRAND' in metacluster.SV_features else None
        INFO['TSD'] = metacluster.TSD if hasattr(metacluster, 'TSD') else None    
        INFO['MECHANISM'] = metacluster.SV_features['MECHANISM'] if 'MECHANISM' in metacluster.SV_features else None
        INFO['GERMDB'] = metacluster.germlineDb if hasattr(metacluster, 'germlineDb') else None       
        INFO['SRCGENE'] = ','.join(metacluster.SV_features['SOURCE_GENE']) if 'SOURCE_GENE' in metacluster.SV_features else None
        INFO['NBEXONS'] = metacluster.SV_features['NB_EXONS'] if 'NB_EXONS' in metacluster.SV_features else None
        INFO['NBTOTAL'], INFO['NBTUMOR'], INFO['NBNORMAL'] = str(metacluster.nbTotal), str(metacluster.nbTumour), str(metacluster.nbNormal) 
        INFO['FULL'] = metacluster.SV_features['IS_FULL'] if 'IS_FULL' in metacluster.SV_features else None
        INFO['PERCR'] = round(metacluster.SV_features['PERC_RESOLVED'], 2) if 'PERC_RESOLVED' in metacluster.SV_features else None
        
        # if structure not resolved
        if (('HITS' not in metacluster.SV_features) or (metacluster.SV_features['HITS'] is None)):
            INFO['QHITS'] = None
            if FILTER == 'PASS' and INFO['ITYPE'] not in ['pseudogene', 'poly(A/T)']: FILTER = 'SCORE'

        # if structure resolved             
        else:
            sortedAlignments = sorted(metacluster.SV_features['HITS'], key=lambda x: x.qBeg, reverse=False)
            INFO['QHITS'] = ','.join(list(OrderedSet([ 'ins' + ':' + str(alignment.qBeg) + '-' + str(alignment.qEnd) + '>' + (alignment.tName.split('|')[2] if len(alignment.tName.split('|'))>1 else alignment.tName) + ':' + str(alignment.tBeg) + '-' + str(alignment.tEnd) + '_'  + str(alignment.strand) for alignment in sortedAlignments ])))
                
        INFO['INSEQ'] = metacluster.consensusEvent.pick_insert() if metacluster.consensusEvent is not None else None
        INFO['REGION'] = str(region).replace(";",",")
        INFO['GENE'] = str(gene).replace(";",",")
        INFO['REP'] = ','.join([repeat['family'] for repeat in repeats]) if repeats else None 
        INFO['REPSUB'] = ','.join([repeat['subfamily'] for repeat in repeats]) if repeats else None
        INFO['REPDIV'] = ','.join([repeat['milliDiv'] for repeat in repeats]) if repeats else None 
        INFO['DIST'] = ','.join([str(repeat['distance']) for repeat in repeats]) if repeats else None
               
        if extraFields:
        
            INFO['CIPOS'] = str(CIPOS[0]) + ',' + str(CIPOS[1]) 
            INFO['RTLEN'] = metacluster.SV_features['RETRO_LEN'] if 'RETRO_LEN' in metacluster.SV_features else None
            INFO['TRUN5LEN'] = metacluster.SV_features['TRUNCATION_5_LEN'] if 'TRUNCATION_5_LEN' in metacluster.SV_features else None
            INFO['TRUN3LEN'] = metacluster.SV_features['TRUNCATION_3_LEN'] if 'TRUNCATION_3_LEN' in metacluster.SV_features else None
            INFO['TDLEN'] = metacluster.SV_features['TRANSDUCTION_LEN'] if 'TRANSDUCTION_LEN' in metacluster.SV_features else None
            INFO['INVLEN'] = metacluster.SV_features['INVERSION_LEN'] if 'INVERSION_LEN' in metacluster.SV_features else None
            INFO['RTCOORD'] = metacluster.SV_features['RETRO_LEN'] if 'RETRO_LEN' in metacluster.SV_features else None
            INFO['NBSPAN'], INFO['NBCLIP'], INFO['DEPTH'] = str(metacluster.nbINS), str(metacluster.nbCLIPPING), str(metacluster.depth)
            INFO['NBCLIP_5END'], INFO['NBCLIP_3END'] = str(metacluster.nbCLIPPING_right), str(metacluster.nbCLIPPING_left)
            INFO['END5'], INFO['DEPTH_5END'] = str(metacluster.depth_5end[0]), str(metacluster.depth_5end[1])
            INFO['END3'], INFO['DEPTH_3END'] = str(metacluster.depth_3end[0]), str(metacluster.depth_3end[1])
            INFO['HP1'] = str(metacluster.HP1_ALT)
            INFO['THP1'] = str(metacluster.HP1_ALL)
            INFO['HP2'] = str(metacluster.HP2_ALT)
            INFO['THP2'] = str(metacluster.HP2_ALL)
            INFO['HPX'] = str(metacluster.HPX_ALT)
            INFO['THPX'] = str(metacluster.HPX_ALL)
            INFO['PS'] = metacluster.PS
            INFO['READIDS'] = ','.join(list(set(metacluster.reads)))
            INFO['CV'] = round(metacluster.cv, 2) if metacluster.cv else None

        ## Create VCF variant object
        fields = [CHROM, POS, "ID"+str(ID), REF, ALT, QUAL, FILTER, INFO, {}]

        ## Add variant to the VCF
        INS = formats.VCF_variant(fields)
        VCF.add(INS)
        
    ## 4. Sort VCF
    VCF.sort()
    
    ## 5. Write VCF in disk
    infoIds = ['FAM', 'SUBFAM', 'ITYPE', 'CYTOID', 'VTYPE', 'LEN', 
               'POLYA', 'STRAND', 'MECHANISM', 'PERCR', 'FULL', 'GERMDB',  
               'SRCGENE', 'NBEXONS', 'NBTOTAL', 'NBTUMOR', 'NBNORMAL', 
               'REGION', 'GENE', 'REP', 'REPSUB', 'REPDIV', 'DIST', 'QHITS', 'INSEQ']

    if extraFields:

        infoIds += ['CIPOS', 'TSD', 'END5', 'END3', 'NBSPAN', 'NBCLIP', 
                'DEPTH', 'NBCLIP_5END', 'NBCLIP_3END', 'DEPTH_5END', 'DEPTH_3END', 
                'HP1', 'THP1', 'HP2', 'THP2', 'HPX', 'THPX', 'PS', 'READIDS', 
                'CV', 'RTLEN', 'TRUN5LEN', 'TRUN3LEN', 'TDLEN', 'INVLEN', 'RTCOORD']

    meiDir = outDir + '/ME_INS'
    unix.mkdir(meiDir)
    VCF.write(infoIds, [], outName, meiDir)

def write_junctions(junctions, outDir, extraFields):
    '''
    Write BND junction calls into a tsv file

    Input:
        1. junctions: list containing list of BND junction objects 
        2. outDir: Output directory
        
    Output: bedpe file containing identified BND junctions
    '''

    ## 1. Open output files
    rgDir = outDir + '/ME_RG'
    unix.mkdir(rgDir)

    outPath = rgDir + '/RG_MEIGA.PASS.bedpe'
    outPass = open(outPath, 'w')
    outPath_failed = rgDir + '/RG_MEIGA.FAILED.bedpe'
    outFailed = open(outPath_failed, 'w')

    ## 2. Write header 
    row = "#chrom1\tstart1\tend1\tchrom2\tstart2\tend2\tname\tscore\tstrand1\tstrand2\ttype\tfamily\tsrc\tdist\tlen\tstrand\tmechanism\tpolyA\tpercRes\tqHits\tseq\trepeatsA\trepeatsB\tnbBridge\tnbTotal\tnbTumour\tnbNormal"
    if extraFields: row += "\talignStdA\talignStdB\treadsA\treadsB"

    outPass.write(row + "\n")
    outFailed.write(row + "\n")

    ## 3. Write BNDs
    for junction in junctions:

        discard, row = outputJunction(junction, extraFields)

        if discard: 
            outFailed.write(row + "\n")

        else:
            outPass.write(row + "\n")

    ## 4. Close output files
    outPass.close()
    outFailed.close()

    ## 5. Sort output 
    bins = pybedtools.BedTool(outPath)
    bins.sort(header=True).moveto(outPath)
    bins = pybedtools.BedTool(outPath_failed)
    bins.sort(header=True).moveto(outPath_failed)

def outputJunction(junction, extraFields):

    ## A. Set lower and upper bounds
    if (junction.junctionType() == 'intrachromosomal') and (junction.metaclusterA.bkpPos > junction.metaclusterB.bkpPos): 
        junction.clusterA = junction.metaclusterB
        junction.clusterB = junction.metaclusterA

    else:
        junction.clusterA = junction.metaclusterA
        junction.clusterB = junction.metaclusterB
    
    ## B. Metaclusters details
    junction.callA = outputMeta(junction.clusterA)
    junction.callB = outputMeta(junction.clusterB)

    ## C. Junction specific attrib
    # Bridge info
    bridgeType = junction.bridge.bridgeType if junction.bridge is not None else None
    family = junction.bridge.family if junction.bridge is not None else None
    srcId = junction.bridge.srcId if junction.bridge is not None else None
    bkpDist = str(junction.callB['beg'] - junction.callA['beg']) if junction.junctionType() == 'intrachromosomal' else '.'
    bridgeLen = int(junction.bridge.bridgeLen) if junction.bridge is not None else None
    strand = junction.bridge.strand if junction.bridge is not None else None
    mechanism = junction.bridge.mechanism if junction.bridge is not None else None
    polyA = junction.bridge.polyA if junction.bridge is not None else None
    percRes = round(junction.bridge.percRes, 1) if junction.bridge is not None else None
    bridgeSeq = junction.bridge.bridgeSeq if junction.bridge is not None else None

    # qHits
    sortedAlignments = sorted(junction.bridge.qHits, key=lambda x: x.qBeg, reverse=False) if junction.bridge else []
    if sortedAlignments:
        qHits = ','.join(list(OrderedSet([ 'bridge' + ':' + str(alignment.qBeg) + '-' + str(alignment.qEnd) + '>' + (alignment.tName.split('|')[2] if len(alignment.tName.split('|'))>1 else alignment.tName) + ':' + str(alignment.tBeg) + '-' + str(alignment.tEnd) + '_'  + str(alignment.strand) for alignment in sortedAlignments ])))    
    else:
        qHits = "None"
    
    # Read counts
    sharedReads = junction.callA['reads'].intersection(junction.callB['reads'])
    nbSharedReads = len(list(sharedReads))
    nbReadsTotal, nbReadsTumour, nbReadsNormal = junction.supportingReads()

    # Set score
    repeatsA = junction.callA['repeats'] 
    repeatsB = junction.callB['repeats']
    alignStdA = junction.callA['alignStd']
    alignStdB = junction.callB['alignStd']

    score, discard = scoreJunction(nbReadsNormal, percRes, bridgeType, srcId, family, alignStdA, alignStdB, repeatsA, repeatsB, qHits, nbSharedReads, bkpDist, polyA)
    
    rawCall = [junction.callA['ref'], junction.callA['beg'], junction.callA['end'], 
               junction.callB['ref'], junction.callB['beg'], junction.callB['end'], 
               junction.svType, str(score), junction.callA['strand'], junction.callB['strand'], 
               bridgeType, family, srcId, bkpDist, bridgeLen, 
               strand, mechanism, polyA, percRes, qHits, bridgeSeq, repeatsA, repeatsB, 
               nbSharedReads, nbReadsTotal, nbReadsTumour, nbReadsNormal]
    
    if extraFields: 
        rawCall += [alignStdA, alignStdB, ','.join(list(junction.callA['reads'])), ','.join(list(junction.callB['reads']))]

    call = '\t'.join([str(i) for i in rawCall])

    return discard, call 

def outputMeta(metacluster):
    
    callDict = {}
    callDict['ref'] = metacluster.ref
    callDict['beg'] = metacluster.bkpPos
    callDict['end'] = metacluster.bkpPos
    callDict['strand'] = metacluster.strand
    callDict['reads'] = set(metacluster.reads)
    callDict['alignStd'] = int(round(metacluster.alignLenStd, 0))
    repeats = metacluster.repeatAnnot if hasattr(metacluster, 'repeatAnnot') else []
    callDict['repeats'] = ','.join([repeat['subfamily'] for repeat in repeats]) if repeats else "None"

    return callDict

def scoreJunction(nbReadsNormal, percRes, bridgeType, srcId, family, alignLenStdA, alignLenStdB, repeatsA, repeatsB, qHits, nbSharedReads, bkpDist, polyA):

    FILTER = None
    DISCARDED = True

    # Less than 2 normal reads and percRes >30%
    if (nbReadsNormal < 2) and percRes and (percRes > 30):

        # Proper identity assigned
        if ((str(bridgeType) != "None") and (str(bridgeType) != "unknown")):
            
            if ((bridgeType != "orphan") and (str(family) in str(qHits))) or (bridgeType in ["orphan", "partnered"] and srcId in str(qHits)):
            
                # Aligned length standard deviation > 100 bp for both bkps
                if (alignLenStdA > 100) and (alignLenStdB > 100):
                    
                    # Discard if SVA -- Source of FPs
                    if str(family) != "SVA":
                        
                        # Discard Alus when Alu in annotations
                        if (str(family) == "Alu") and ("Alu" not in repeatsA) and ("Alu" not in repeatsB):

                            # Discard when identity is Alu and no more than 1 shared read
                            if (str(family) == "Alu") and (nbSharedReads > 1):       

                                FILTER = "SCORE"

                        # Discard when identity is L1 and L1PA in annotations
                        elif (str(family) == "L1") and ("L1HS" not in repeatsA) and ("L1HS" not in repeatsB):
                                    
                            # Set filter to PASS or SCORE
                            if (str(family) == "L1"): 

                                if polyA:
                                    FILTER = "PASS"

                                elif ("L1PA" in repeatsA) or ("L1PA" in repeatsB):
                                    FILTER = "SCORE"

                                else:
                                    FILTER = "PASS"

                        elif (str(family) == "poly(A/T)"):

                            FILTER = "PASS"

                        if FILTER:

                            # if nbSharedReads less than 3, SCORE
                            if nbSharedReads < 3:
                                FILTER = "SCORE"

                            # if bkpDist > 50 bp
                            if bkpDist != '.':

                                if abs(int(bkpDist)) > 50:
                                    DISCARDED = False

                            # interchromosomic 
                            else:
                                DISCARDED = False
    
    return FILTER, DISCARDED
