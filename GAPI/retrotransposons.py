'''
Module 'retrotransposons' - Contains functions for the identification and characterization of retrotransposon sequences
'''

## DEPENDENCIES ##
# External
import operator
import itertools
import intervals as I

# Internal
from GAPI import unix
from GAPI import formats
from GAPI import alignment
from GAPI import sequences
from GAPI import annotation
from GAPI import bnds
from GAPI import retrotransposons


## FUNCTIONS ##
def retrotransposon_structure(FASTA_file, index, outDir):
    '''    
    Infer the insertion size, structure, poly-A, target site duplication length and other insertion structural features

    Input:
        1. FASTA_file: Path to FASTA file containing the sequence
        2. index: Minimap2 index for consensus retrotransposon sequences database
        3. outDir: Output directory
        
    Output:
        1. structure: dictionary containing insertion structure information
    '''     
    structure = {}

    ## 0. Create logs directory ##
    logDir = outDir + '/Logs'
    unix.mkdir(logDir)

    ## 1. Align the sequence into the retrotransposon sequences database ##
    PAF_file = alignment.alignment_minimap2(FASTA_file, index, 'alignment2consensus', 1, outDir)

    ## 2. Read PAF alignments ##
    PAF = formats.PAF()
    PAF.read(PAF_file)
    
    # Exit function if no hit on the retrotransposons database
    if not PAF.alignments:
        
        ## 4.1 Collect polyA hits
        # Retrieve inserted seq
        FASTA = formats.FASTA()
        FASTA.read(FASTA_file)
        sequence = list(FASTA.seqDict.values())[0]
        id = list(FASTA.seqDict.keys())[0]
        seq, chain = bnds.is_there_polyA(sequence, formats.PAF_chain([]), id=id)

        if chain.alignments:
            
            ## Update structure
            polyA_bases = sum([(alignment.qEnd-alignment.qBeg) for alignment in chain.alignments if 'poly(A/T)' in alignment.tName])
            polyA_perc = polyA_bases/chain.alignments[0].qLen*100
            structure['PERC_RESOLVED'] = polyA_perc
            structure['HITS'] = chain.alignments
            structure['INS_TYPE'] = 'poly(A/T)'
            structure['FAMILY'] = ['poly(A/T)']
            structure['POLYA'] = True
            structure['STRAND'] = chain.alignments[0].strand
            structure['MECHANISM'] = 'TPRT'

            return structure

        else:
            return structure

    ## 3. Chain complementary alignments ##
    chain = PAF.chain(300, 50)

    ## 4. Infer insertion features ##
    # Retrieve inserted seq
    FASTA = formats.FASTA()
    FASTA.read(FASTA_file)
    sequence = list(FASTA.seqDict.values())[0]

    ## 4.1 Insertion type
    structure['INS_TYPE'], structure['FAMILY'], structure['SUBFAMILY'], structure['CYTOBAND'], structure['PERC_RESOLVED'] = insertion_type(chain)
    
    ## 4.2 Insertion strand
    structure['STRAND'], structure['POLYA'] = infer_strand(structure['INS_TYPE'], sequence, chain)

    ## 4.3 Sequence lengths 
    lengths = infer_lengths(structure['INS_TYPE'], chain, structure['STRAND'])
    structure.update(lengths)

    ## 4.4 Insertion mechanism (TPRT or EI)
    structure['MECHANISM'] = infer_integration_mechanism(chain, structure['TRUNCATION_3_LEN'], structure['POLYA'])

    ## 4.5 Target site duplication (TO DO LATER...)
    #search4tsd()
    
    ## 4.6 Update percentage resolved
    polyA_bases = sum([(alignment.qEnd-alignment.qBeg) for alignment in chain.alignments if 'poly(A/T)' in alignment.tName])
    polyA_perc = polyA_bases/chain.alignments[0].qLen*100
    structure['PERC_RESOLVED'] += polyA_perc
    if structure['PERC_RESOLVED'] > 100: structure['PERC_RESOLVED'] = 100
    
    # update structure['HITS']
    structure['HITS'] = chain.alignments

    return structure
    

def insertion_type(chain, minMAPQ=5):
    '''
    Scan alignments chain to determine the type of insertion (solo, transduction...)

    Input:
        1. chain: Sequence chain of alignments over retrotranposon consensus sequences and/or transduced regions
        
    Output:
        1. insType: Insertion type (solo, nested, orphan, partnered or None)
        2. family: List of retrotransposon families
        3. srcId: List of source element ids
    ''' 
    ## Make list containing all the different templates the sequence aligns into
    templateTypes = list(set([alignment.tName.split("|")[0] for alignment in chain.alignments]))
    nbTemplateTypes = len(templateTypes)
    
    ## A. Make list containing the families the sequence aligns into
    repAlignments = [alignment for alignment in chain.alignments if ('consensus' in alignment.tName)]
    families = list(set([alignment.tName.split("|")[1] for alignment in chain.alignments if ('consensus' in alignment.tName)]))
    subfamilies = list(set([alignment.tName.split("|")[2] for alignment in chain.alignments if ('consensus' in alignment.tName)]))

    nbFamilies = len(families)
    
    ## B. Make list containing the td regions the sequence aligns into 
    if ('td' in templateTypes):
        
        ### a). Partnered
        if ('consensus' in templateTypes):
            
            # select only alignments at the begging or end (partnered) of td regions
            offset = 50
            tdLen = 10000
            
            srcElements = list(set([alignment.tName.split("|")[2] for alignment in chain.alignments if ('td' in alignment.tName)]))
            nb_srcElements = len(srcElements)
            
            if nb_srcElements == 1:
                # filter by MAPQ
                tdAlignments = [alignment for alignment in chain.alignments if ('td' in alignment.tName and alignment.MAPQ >= minMAPQ)]
            
            else:
                # filter by start position when overlapping srcElements
                # awk '{if($7 == "+"){print $1, $3, $3+10000, $4, $5, $6, $7} else if($7 == "-"){print $1, $2-10000, $2, $4, $5, $6, $7}}' srcElements.bed | tr ' ' '\t' | sort -k1,1 -k2,2n | bedtools merge -i - -c 5,7 -o distinct,distinct | grep "," | awk '{print $0"\t"($3-$2)/2}'
                #     chr1	86669080	86684896	chr1_86674895,chr1_86679080	+,-	7908
                #     chr14	30671602	30684809	chr14_30681602,chr14_30684809	-	6603.5
                #     chr2	143866987	143878657	chr2_143860956,chr2_143868656	+	5835
                #     chr3	99046640	99058682	chr3_99056640,chr3_99058682	-	6021
                #     chr9	72279997	72291743	chr9_72281742,chr9_72289997	+,-	5873
            
                tdAlignments = [alignment for alignment in chain.alignments if ('td' in alignment.tName and (alignment.tBeg < offset or alignment.tEnd > tdLen-offset))]
            
            if tdAlignments:
                sourceElements = list(set([alignment.tName.split("|")[2] for alignment in tdAlignments]))
                familiesTd = list(set([alignment.tName.split("|")[1] for alignment in tdAlignments]))
                nbSource = len(sourceElements)

                # some elements are very similar at the begging of the TD seq. Choose the one with greater MAPQ
                if nbSource > 1:
                    aln = sorted(tdAlignments, key=lambda x: x.MAPQ, reverse=True)[0]
                    sourceElements = [aln.tName.split("|")[2]]
                    familiesTd = [aln.tName.split("|")[1]]
                    nbSource = 1
                
            else:
                templateTypes.remove('td')
                nbTemplateTypes = len(templateTypes)

        ### b). Orphan
        else:
            tdAlignments = [alignment for alignment in chain.alignments if ('td' in alignment.tName and alignment.MAPQ >= minMAPQ)]
            sourceElements = list(set([alignment.tName.split("|")[2] for alignment in tdAlignments]))
            familiesTd = list(set([alignment.tName.split("|")[1] for alignment in tdAlignments]))
            nbSource = len(sourceElements)
    
    ## a) Solo insertion
    # //////RT//////     
    if (nbTemplateTypes == 1) and ('consensus' in templateTypes) and (nbFamilies == 1):
        insType = 'solo'
        family = families
        subfamily = subfamilies
        srcId = []
        queryPerc = chain_queryPerc(repAlignments)
        
    ## b) Nested insertion (Insertion composed by multiple retrotransposons from different families)
    # //////RT_1//////\\\\\\\\RT_2\\\\\\\     
    elif (nbTemplateTypes == 1) and ('consensus' in templateTypes):
        insType = 'nested'
        family = families
        subfamily = subfamilies
        srcId = []
        queryPerc = chain_queryPerc(repAlignments)
        
    ## c) Orphan (inserted sequence only matching one transduced region)
    # SOURCE//////TD//////    
    elif (nbTemplateTypes == 1) and ('td' in templateTypes) and (nbSource == 1):
        insType = 'orphan'
        family = familiesTd
        subfamily = []
        srcId = sourceElements
        queryPerc = chain_queryPerc(tdAlignments)
        
    ## d) Partnered (inserted sequence matching consensus and one transduced sequence)
    # SOURCE >>>>>>L1>>>>>>//////TD///////    
    elif (nbTemplateTypes == 2) and (set(['consensus', 'td']).issubset(templateTypes)) and (nbFamilies == 1) and (nbSource == 1):
        insType = 'partnered'
        family = families
        subfamily = subfamilies
        srcId = sourceElements
        queryPerc = chain_queryPerc(repAlignments+tdAlignments)

    ## e) Unknown insertion type
    else:
        insType = 'unknown'
        family = []
        subfamily = []
        srcId = [] 
        queryPerc = 0

    return insType, family, subfamily, srcId, queryPerc


def chain_queryPerc(alignments, polyA_qPos = None):
    '''
    From alignments, estimate the percentage of the query covered by the alignment length
    
    Input:
    1. alignments: list of alignments objects
    2. polyA_qPos: query coordinates of polyA tracks (optional)
    
    Output:
    1. Percetage of query covered
    '''
    # Get array of (start, end) positions
    arr = [[alignment.qBeg, alignment.qEnd] for alignment in alignments]
    
    # Add poly positions if available
    if polyA_qPos:
        arr += polyA_qPos
    
    # Get query length
    queryLen = alignments[0].qLen
     
    # Sort based on the increasing order of start coordinates
    arr.sort(key=lambda x: x[0])
 
    # Store index of last element in output array (modified arr[])
    index = 0
    lenIntervals = []
 
    # Traverse all input Intervals starting from second interval
    for i in range(1, len(arr)):
 
        # If this is not first interval and overlaps
        # with the previous one, merge previous and current intervals
        if (arr[index][1] >= arr[i][0]):
            arr[index][1] = max(arr[index][1], arr[i][1])
        else:
            index = index + 1
            arr[index] = arr[i]
            
    for i in range(index+1):
        end = arr[i][1]
        beg = arr[i][0]
        lenIntervals.append(end-beg)
    
    return round(sum(lenIntervals) * 100 / queryLen, 3)

def trim_polyA_seq(insSeq):
    '''
    Trim insSeq from polyA tails
    '''
    # find polyA tracks
    perc_polyA, polyA_tracks, uniq_positions = retrotransposons.perc_polyA(insSeq, windowSize = 10)
    
    # trim polyA tracks from sequence
    if polyA_tracks:
        
        complexSeq = ''
        r1 = I.closed(0, len(insSeq))
        r2 = [I.closed(polyA[0], polyA[1]) for polyA in polyA_tracks]
        
        for interval in r2:
            r1 = r1 - interval
        
        for i in r1:
            complexSeq += insSeq[i.lower:i.upper]
            
    else:
        complexSeq = insSeq
        
    return complexSeq
        
def infer_strand(insType, sequence, chain):
    '''
    Infer insertion strand based on two criteria:
        1) Alignment orientation for the insert 3' end over the template sequence
        2) Location of polyA/T tail at sequence ends

    Input:
        1. insType: Insertion type (solo, nested, orphan, partnered or None)
        2. sequence: consensus inserted sequence
        3. chain: Sequence chain of alignments over retrotranposon consensus sequences and/or transduced regions

    Output:
        1. strand: Insertion strand (+, - or None) 
        2. polyA: boolean specifying if polyA/T sequence was found
    '''

    ## 1. Strand based on polyA/T presence 
    strandPolyA, polyA = infer_strand_polyA(sequence, chain)

    ## 2. Strand based on alignment orientation
    strandAlignment = infer_strand_alignment(insType, chain)

    ## 3. Define consensus strand
    # a) PolyA/T has preference over the alignment orientation
    if strandPolyA is not None:
        strand = strandPolyA
    
    # b) Strand based on the alignment orientation
    elif strandAlignment is not None:
        strand = strandAlignment
    
    # c) Unknown strand
    else:
        strand = None
    
    return strand, polyA
    

def infer_strand_polyA(sequence, chain):
    '''
    Infer insertion strand based on two criteria:
        1) Location of polyA/T tail at sequence ends
        2) Alignment strand for the insert 3' end over the template sequence

    Input: 
        1. sequence: consensus inserted sequence
        2. chain: Sequence chain of alignments over retrotranposon consensus sequences and/or transduced regions

    Output:
        1. strand: Insertion strand (+, - or None) 
        2. polyA: boolean specifying if polyA/T sequence was found
    '''
    ### Set up configuration parameters
    windowSize = 8
    maxWindowDist = 2
    minMonomerSize = 10
    minPurity = 80 

    maxDist2Ends = 10 
    minInternalMonomerSize = 20

    ## 1. Search for polyA at the insert 3' end ##
    # 1.1 Extract unaligned 3' end of the inserted sequence
    lastHit = chain.alignments[-1]
    targetSeq = sequence[lastHit.qEnd:]

    # 1.2 Search for poly(A) monomers on the 3' end 
    targetMonomer = 'A'
    monomers3end = sequences.find_monomers(targetSeq, targetMonomer, windowSize, maxWindowDist, minMonomerSize, minPurity)

    # 1.3 Filter internal monomers
    monomers3end = sequences.filter_internal_monomers(monomers3end, targetSeq, maxDist2Ends, minInternalMonomerSize)

    ## 2. Search for polyT at the insert 5' end ##
    # 2.1 Extract unaligned 5' end of the inserted sequence
    firstHit = chain.alignments[0]
    targetSeq = sequence[:firstHit.qBeg]

    # 2.2 Search for poly(T) monomers on the 5' end 
    targetMonomer = 'T'
    monomers5end = sequences.find_monomers(targetSeq, targetMonomer, windowSize, maxWindowDist, minMonomerSize, minPurity)

    # 2.3 Filter internal monomers
    monomers5end = sequences.filter_internal_monomers(monomers5end, targetSeq, maxDist2Ends, minInternalMonomerSize)

    ## 3. Determine strand ##
    # 3.1 Compute 3' monomers accumulative length
    monomers3endLengths = [monomer.length() for monomer in monomers3end]
    accumulative3prime = sum(monomers3endLengths)
     
    # 3.2 Compute 5' monomers accumulative length
    monomers5endLengths = [monomer.length() for monomer in monomers5end]
    accumulative5prime = sum(monomers5endLengths)

    # 3.3 Determine if polyA/T at 5' or 3' end (indicative of strand orientation) 
    # a) Unknown strand if:
    # - No monomer found in any end OR
    # - Ambiguity as 3' and 5' monomers with equal size
    if ((accumulative3prime == 0) and (accumulative5prime == 0)) or (accumulative3prime == accumulative5prime) :
        monomers = None
        strand = None
        polyA = False

    # b) Positive strand
    elif accumulative3prime > accumulative5prime:
        monomers = monomers3end
        strand = '+'
        polyA = True

    # c) Negative strand
    else:
        monomers = monomers5end
        strand = '-'
        polyA = True

    ## 4. Convert monomer coordinates to inserted sequence space ##
    ## a) + strand
    # -----insert------**unaligned**
    if strand == '+':

        for monomer in monomers:
            monomer.beg = monomer.beg + lastHit.qEnd
            monomer.end = monomer.end + lastHit.qEnd 

    ## b) - strand
    # NOT NEEDED as the unaligned sequence correspond to the leftmost end of the insert 
    # **unaligned**-----insert------ 

    ## 5. Add polyA/T to the chain of alignments
    firstAlignment = chain.alignments[0]

    ## a) + strand
    if strand == '+':

        # For each monomer
        for monomer in monomers:

            # Create PAF line containing poly(A) info
            fields = [firstAlignment.qName, firstAlignment.qLen, monomer.beg, monomer.end, strand, 'poly(A/T)', 0, 0, 0, 0, 0, 0, 'tp:A:P']            
            alignment = formats.PAF_alignment(fields)

            # Add to the end of the chain
            chain.alignments.append(alignment) 

    ## b) - strand
    elif strand == '-':

        # For each monomer
        for monomer in monomers[::-1]:

            # Create PAF line containing poly(T) info
            fields = [firstAlignment.qName, firstAlignment.qLen, monomer.beg, monomer.end, strand, 'poly(A/T)', 0, 0, 0, 0, 0, 0, 'tp:A:P']
            alignment = formats.PAF_alignment(fields)

            # Add to the begin of the chain
            chain.alignments.insert(0, alignment) 

    return strand, polyA
        
def infer_strand_alignment(insType, chain):
    '''
    Determine insertion strand (+ or -). The insertion strand can be infered from the alignment
    strand for the insert 3' end over the template sequence
    
    Input:
        1. insType: Insertion type (solo, nested, orphan, partnered or None)
        2. chain: Sequence chain of alignments over retrotranposon consensus sequences and/or transduced regions
        
    Output:
        1. strand: Insertion strand (+, - or None) 
    ''' 
    ## a) Solo or orphan transduction    
    if insType in ['solo', 'orphan']:

        ## Sort hits from 5' to 3'
        sortedHits = sorted(chain.alignments, key=lambda alignment: alignment.tEnd, reverse=False)

        ## Select 3' end hit as its strand will be == insertion strand
        strand = sortedHits[-1].strand
        
    ## b) Partnered transduction
    elif insType == 'partnered':

        ## Select hits over the transduced region
        transductionHits = [alignment for alignment in chain.alignments if 'td' in alignment.tName]

        ## Sort hits from 5' to 3'
        sortedHits = sorted(transductionHits, key=lambda alignment: alignment.tEnd, reverse=False)

        ## Select 3' end hit as its strand will be == insertion strand
        strand = sortedHits[-1].strand

    ## c) Other
    else:
        strand = None

    return strand


def infer_lengths(insType, chain, strand):
    '''
    Determine the length of each type of sequence composing the insertion (transduction, retrotransposon, insertion, inversion)
    
    Input:
        1. insType: Insertion type (solo, nested, orphan, partnered or None)
        2. chain: Sequence chain of alignments over retrotranposon consensus sequences and/or transduced regions
        3. strand: Insertion strand (+ or -)

    Output:
        1. lengths: dictionary containing length information
    ''' 
    ### Initialize dictionary
    lengths = {}

    for feature in ['RETRO_COORD', 'RETRO_LEN', 'IS_FULL', 'TRUNCATION_5_LEN', 'TRUNCATION_3_LEN', 'TRANSDUCTION_COORD', 'TRANSDUCTION_LEN', 'INVERSION_LEN']:
        lengths[feature] = None

    ### 1. Compute the length of each type of sequence composing the insertion
    # 1.1 Retroelement length
    if insType in ['solo', 'partnered']:

        ## Pick only those hits over retrotransposon consensus sequence
        retroHits = [hit for hit in chain.alignments if 'consensus' in hit.tName]

        ## Determine piece of consensus sequence that has been integrated
        ref = retroHits[0].tName.split('|')[1]
        retroBeg = min([hit.tBeg for hit in retroHits])
        retroEnd = max([hit.tEnd for hit in retroHits])
        lengths['RETRO_COORD'] = str(ref) + ':' + str(retroBeg) + '-' + str(retroEnd)

        ## Compute length
        lengths['RETRO_LEN'] = retroEnd - retroBeg

        ## Assess if full length retrotransposon insertion
        consensusLen = retroHits[0].tLen 
        percConsensus = float(lengths['RETRO_LEN']) / consensusLen * 100
        lengths['IS_FULL'] = True if percConsensus >= 95 else False

        ## Compute truncation length at both ends
        lengths['TRUNCATION_5_LEN'] = retroBeg   
        lengths['TRUNCATION_3_LEN'] = consensusLen - retroEnd

    # 1.2 Transduction length
    if insType in ['partnered', 'orphan']:

        ## Pick only those hits over transduced region
        transductionHits = [hit for hit in chain.alignments if 'td' in hit.tName]
 
        ## Compute offset to translate from interval to genomic coordinates
        interval = transductionHits[0].tName.split('|')[3]
        ref, coord = interval.split(':')
        offset = int(coord.split('-')[0])
        
        ## Determine piece of transduced area that has been integrated
        transductionBeg = min([hit.tBeg for hit in transductionHits]) + offset
        transductionEnd = max([hit.tEnd for hit in transductionHits]) + offset
        lengths['TRANSDUCTION_COORD'] = (ref, transductionBeg, transductionEnd)

        ## Compute length
        lengths['TRANSDUCTION_LEN'] = transductionEnd - transductionBeg

    # 1.3 Inversion length
    inversionHits = [hit for hit in chain.alignments if ((hit.strand != 'None') and (hit.strand != strand))]

    # a) 5' inversion
    if inversionHits:
        lengths['INVERSION_LEN'] = sum([hit.tEnd - hit.tBeg for hit in inversionHits])

    # b) No inversion
    else:
        lengths['INVERSION_LEN'] = 0   
     
    return lengths


def infer_integration_mechanism(chain, truncation3len, polyA):
    '''
    Determine the mechanism of integration (TPRT: Target Primed Reversed Transcription; EI: Endonuclease Independent)
    
    Input:
        1. chain: Sequence chain of alignments over retrotranposon consensus sequences and/or transduced regions
        2. truncation3len: number of base pairs the inserted sequence has been truncated at its 3'
        3. polyA: boolean specifying if polyA/T sequence was found

    Output:
        1. mechanism: TPRT, EI or unknown
    '''  
    ## A) TPRT hallmarks: 
    # - 3' truncation <= 100bp OR None (if orphan transduction)
    # - polyA 
    # - Incorporate TSD later as an additional evidence...
    if ((truncation3len is None) or (truncation3len <= 100)) and polyA:
        mechanism = 'TPRT'

    ## B) EI hallmarks:
    # - % resolved > 95%
    # - 3' truncation > 100bp 
    # - no polyA
    elif (chain.perc_query_covered() >= 95) and ((truncation3len is not None) and (truncation3len > 100)) and not polyA:
        mechanism = 'EI'

    ## C) Unknown mechanism:
    else:
        mechanism = 'unknown'

    return mechanism


def is_interspersed_ins(sequence, PAF, repeatsDb, transducedDb):
    '''
    Determine if input sequence corresponds to a interspersed insertion

    Input:
        1. sequence: Input sequence
        2. PAF: PAF object containing input sequence alignments on the reference genome
        3. repeatsDb: bin database containing annotated repeats in the reference. None if not available
        4. transducedDb: bin database containing regions transduced by source elements. None if not available

    Output:
        1. INTERSPERSED: Boolean specifying if inserted sequence corresponds to an intersersed repeat (True) or not (False)
        2. INS_features: dictionary containing interspersed repeat insertion features
        3. chain: alignments chain on the reference. None if sequence does not align on the reference
    '''
    INS_features = {}

    ## 0. Preliminary steps
    ## 0.1 Does the sequence corresponds to a polyA/T? 
    perc_polyA_seq, polyA_positions, uniq_positions = perc_polyA(sequence, windowSize = 10)
    INS_features['PERC_RESOLVED'] = perc_polyA_seq
    
    if (perc_polyA_seq > 80) or (uniq_positions < 50 and perc_polyA_seq > 40):
        
        INTERSPERSED = True
        INS_features['INS_TYPE'] = 'poly(A/T)'
        INS_features['POLYA'] = True
        return INTERSPERSED, INS_features, None

    ## 0.2 Abort if sequence does not align on the reference 
    if (not PAF.alignments) or (repeatsDb is None):
        
        INTERSPERSED = False
        INS_features['INS_TYPE'] = 'unknown'
        INS_features['PERC_RESOLVED'] = 0
        return INTERSPERSED, INS_features, None
            
    ## 1. Create chain of alignments ##
    chain = PAF.chain_td()
    
    if (not chain):
        INTERSPERSED = False
        INS_features['INS_TYPE'] = 'unknown'
        INS_features['PERC_RESOLVED'] = 0
        return INTERSPERSED, INS_features, None
    
    ## 2. Annotate each hit in the chain ##
    repeatMatch = False
    transducedMatch = False

    ## For each hit
    for hit in chain.alignments:
        
        hit.annot = {}
        overlaps = False

        ## 2.1 Hit matches a transduced region
        # a) database containing transduced regions available
        if transducedDb is not None:
            overlaps = annotation.annotate_interval(hit.tName, hit.tBeg, hit.tEnd, transducedDb)
            overlaps = [overlap for overlap in overlaps if overlap[1] >= 30]
        
        if overlaps:
            transducedMatch = True
            hit.annot['SOURCE_ELEMENT'] = [overlap[0] for overlap in overlaps]
            overlaps = False
            
        ## 2.2 Hit matches an annotated repeat
        # take into account polyA perc for PERC-RESOLVED
        minOverl = 70 - perc_polyA_seq
        if minOverl < 0: minOverl = 0
        
        overlaps = annotation.annotate_interval(hit.tName, hit.tBeg, hit.tEnd, repeatsDb, minPercOverlap=0)        
            
        if overlaps:
            
            identity = overlaps[0][0].optional['family']
            total_percOverl = sum([overlap[2] for overlap in overlaps if overlap[0].optional['family'] == identity])
        
            if total_percOverl > minOverl:
                
                repeatMatch = True
                hit.annot['REPEAT'] = overlaps[0][0] # Select repeat with longest overlap
            
    ## 3. Make list of distinct features matching the inserted sequence ##
    features = {}
    features['SOURCE_ELEMENT'] = []   
    features['REPEATS'] = {}
    features['REPEATS']['FAMILIES'] = []
    features['REPEATS']['SUBFAMILIES'] = []

    for hit in chain.alignments:
        
        ## Hit overlaps a source element
        if 'SOURCE_ELEMENT' in hit.annot:
            
            for overlap in hit.annot['SOURCE_ELEMENT']:
                cytobandId = overlap.optional['cytobandId']
                family = overlap.optional['family']
                features['SOURCE_ELEMENT'].append(cytobandId)
                features['REPEATS']['FAMILIES'].append(family)
                
            break

        ## Hit overlaps a repeat
        elif 'REPEAT' in hit.annot:
            family = hit.annot['REPEAT'].optional['family']
            subfamily = hit.annot['REPEAT'].optional['subfamily']

            if family not in features['REPEATS']['FAMILIES']:
                features['REPEATS']['FAMILIES'].append(family)

            if subfamily not in features['REPEATS']['SUBFAMILIES']:
                features['REPEATS']['SUBFAMILIES'].append(subfamily)

    INS_features['PERC_RESOLVED'] = chain_queryPerc(chain.alignments, polyA_positions)
    # INS_features['PERC_RESOLVED'] = 100 if INS_features['PERC_RESOLVED'] + chain.perc_query_covered() > 100 else INS_features['PERC_RESOLVED'] + chain.perc_query_covered() 

    ## 4. Determine insertion type based on hits annotation ##
    # A and B) Partnered or Orphan. Set partnered by default. It will be properly defined later.
    if transducedMatch:
        INTERSPERSED = True
        INS_features['INS_TYPE'] = 'partnered'
        INS_features['FAMILY'] = list(set(features['REPEATS']['FAMILIES'])) 
        INS_features['CYTOBAND'] = list(set(features['SOURCE_ELEMENT']))

    # C) Solo
    elif repeatMatch:
        INTERSPERSED = True
        INS_features['INS_TYPE'] = 'solo'
        INS_features['FAMILY'] = list(set(features['REPEATS']['FAMILIES']))
        INS_features['SUBFAMILY'] = list(set(features['REPEATS']['SUBFAMILIES']))

    # D) Unknown 
    else:       
        INTERSPERSED = False
        INS_features['INS_TYPE'] = 'unknown'
    
    return INTERSPERSED, INS_features, chain

def perc_polyA(targetSeq, windowSize = 10):
    '''
    Input:
        1. targetSeq:  String 
        2. windowSize: Numeric value for window size
    
    Output:
        1. perc_polyA: Perc of polyA (substrings of length windowSize within targetSeq)
    '''
    indexes = []

    ## 0. Create sliding windows from targetSeq
    seqLen = len(targetSeq)
    slices = [(pos, pos + windowSize, targetSeq[pos:pos + windowSize]) for pos in range(0, seqLen - windowSize, 1)]

    ## 1. Search for polyA string by iterating over the slices of targetSeq
    for slice in slices:

        # Collect slide info
        beg, end, sliceSeq = slice

        # Test if sequence is polyA 
        polyA, perc = is_polyA(sliceSeq, 90)
        
        if polyA:
            indexes.append(beg)

    ## 2. Estimate perc of polyA in sequence
    perc_polyA = (len(indexes) * 100) / seqLen
    
    ## 3. Get polyA positions in query
    ranges = []
    for k,g in itertools.groupby(enumerate(indexes),lambda x:x[0]-x[1]):
        group = (map(operator.itemgetter(1),g))
        group = list(map(int,group))
        ranges.append([group[0],group[-1]+windowSize])

    uniq_nt = (100-perc_polyA)*len(targetSeq)/100
    
    return perc_polyA, ranges, uniq_nt


def is_polyA(sequence, minPerc):
    '''
    Determine if input sequence corresponds to a polyA/T tail or not

    Input:
        1. sequence: Input sequence
        2. minPerc: Minimum percentage of base pairs corresponding to A or T to call a polyA/T tail

    Output:
        1. polyA: boolean specifying if input sequence corresponds to a polyA/T tail
        2. percPolyA: percentage of bases corresponding to polyA/T
    '''
    ## 1. Assess input sequence base composition
    baseCounts, basePercs = sequences.baseComposition(sequence)

    ## 2. Determine if polyA/T
    # a) PolyA
    if (basePercs['A'] >= minPerc):
        polyA = True
        percPolyA = basePercs['A']

    # b) PolyT
    elif (basePercs['T'] >= minPerc):
        polyA = True
        percPolyA = basePercs['T']

    # c) Not PolyA/T
    else:
        polyA = False
        percPolyA = max([basePercs['A'], basePercs['T']])

    return polyA, percPolyA

def trim_polyA(sequence):
    '''
    Trim poly(A) at sequence end 
    '''
    ## Configuration for monomere search:
    windowSize = 8
    maxWindowDist = 2
    minMonomerSize = 10
    minPurity = 80  

    ## Seach poly(A) monomers
    targetMonomer = 'A'
    monomersA = sequences.find_monomers(sequence, targetMonomer, windowSize, maxWindowDist, minMonomerSize, minPurity)

    if not monomersA:
        return sequence
        
    ## Select monomer closest to sequence end
    candidate = monomersA[-1]

    ## Filter out monomer if more than Xbp from end
    seqLen = len(sequence)
    dist2end = seqLen - candidate.end

    if dist2end <= 30:
        sequence = sequence[:candidate.beg]

    else:
        sequence = sequence

    return sequence

def has_polyA_illumina(targetSeq):
    '''
    Search for polyA/polyT tails in consensus sequence of Illumina clipping events
    
    Input:
    1. targetSeq: consensus sequence of Illumina clipping events
    
    Output:
    1. has_polyA: True or False
    '''
    
    ## 0. Set up monomer searching parameters ##
    windowSize = 8
    maxWindowDist = 2
    minMonomerSize = 8
    minPurity = 95
    maxDist2Ends = 1 
    
    monomerTails = []
    
    ## 1. Search for polyA/polyT at the sequence ends ##
    targetMonomers = ['T', 'A']
        
    for targetMonomer in targetMonomers:
        
        monomers = sequences.find_monomers(targetSeq, targetMonomer, windowSize, maxWindowDist, minMonomerSize, minPurity)
        filtMonomers = sequences.filter_internal_monomers(monomers, targetSeq, maxDist2Ends, minMonomerSize)
        monomerTails += filtMonomers if filtMonomers is not None else monomerTails
    
    while [] in monomerTails: monomerTails.remove([])    
    has_polyA = True if monomerTails != [] else False
    
    return has_polyA

def metaclusters_MEI_type(metaclusters):
    '''
    Determine metaclusters identity
    
    Input:
    1. metaclusters: list of metaclusters
    
    Output: metacluster.identity attribute is filled
    '''
    for metacluster in metaclusters:
        
        metacluster.identity_shortReads_ME()
        
        ## 1. Set identity
        identities = [metacluster.plus_id, metacluster.minus_id]
        src_ids = [metacluster.plus_src_id, metacluster.minus_src_id]
        src_ends = [metacluster.plus_src_end, metacluster.minus_src_end]
        
        while None in identities: identities.remove(None)
        while None in src_ids: src_ids.remove(None)
        while None in src_ends: src_ends.remove(None)
        
        ## If both clusters have the identity and it's the same
        if len(identities) == 2 and len(set(identities)) == 1:
            
            # if no cluster pointing to a TD --> solo and PSD insertions
            if not src_ids:
            
                if metacluster.plus_id == 'L1':
                    metacluster.identity = 'L1'
                    metacluster.ins_type = 'TD0'
                
                elif metacluster.plus_id == 'Alu':
                    metacluster.identity = 'Alu'
                    metacluster.ins_type = 'TD0'
                    
                elif metacluster.plus_id == 'SVA':
                    metacluster.identity = 'SVA'
                    metacluster.ins_type = 'TD0'
                    
                elif metacluster.plus_id == 'Simple_repeat':
                    metacluster.identity = 'pA'
                    metacluster.ins_type = 'TD0'
                
                elif metacluster.plus_id:
                    metacluster.identity = metacluster.plus_id
                    metacluster.ins_type = 'PSD'
            
            # there is a source identifier and a src end --> TD
            elif len(set(src_ids)) == 1 and len(set(src_ends)) == 1:
                
                # partnered
                if len(src_ids) == 1:
                    metacluster.identity = metacluster.plus_id
                    metacluster.ins_type = 'TD1'
                    metacluster.src_id = metacluster.plus_src_id
                    metacluster.src_end = metacluster.plus_src_end
                    metacluster.src_type = 'GERMLINE' 
                
                # orphan
                elif len(src_ids) == 2:
                    metacluster.identity = metacluster.plus_id
                    metacluster.ins_type = 'TD2'
                    metacluster.src_id = metacluster.plus_src_id
                    metacluster.src_end = metacluster.plus_src_end
                    metacluster.src_type = 'GERMLINE' 
            
            # not canonical
            else:
                metacluster.identity = metacluster.plus_id
                metacluster.ins_type = 'UNRESOLVED-TD'
                if src_ids: metacluster.src_id = '_'.join(src_ids)
                if src_ends: metacluster.src_end = '_'.join(src_ends)
        
        # if not both clusters have identity or it is different
        else:
            
            # if only one cluster has identity
            if len(identities) == 1:
                metacluster.identity = identities[0]
                metacluster.ins_type = 'RG'
            
            # if both clusters have identity, but are different
            elif len(identities) == 2:
                
                # if one of the clusters is pA support
                if 'Simple_repeat' in identities:
                    identities.remove('Simple_repeat')
                    metacluster.identity = identities[0]
                    
                    if metacluster.identity in ['L1', 'Alu', 'SVA']:
                        metacluster.ins_type = 'TD2' if src_ids else 'TD0-TD1'
                    else:
                        metacluster.ins_type = 'PSD'
                            
                else:
                    metacluster.identity = '_'.join(identities)
                    metacluster.ins_type = 'UNDEFINED'
            
            # set src attributes if TD
            if src_ids: metacluster.src_id = '_'.join(list(set(src_ids)))
            if src_ends: metacluster.src_end = '_'.join(list(set(src_ends)))
            if metacluster.src_id: metacluster.src_type = 'GERMLINE'
            