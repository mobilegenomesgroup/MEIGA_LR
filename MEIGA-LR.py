#!/usr/bin/env python
#coding: utf-8


if __name__ == '__main__':

    ####################
    ## Import modules ##
    ####################

    # External
    import argparse
    import sys
    import os
    import multiprocessing as mp

    # GAPI
    from GAPI import log
    from GAPI import bamtools
    from GAPI import unix
    from GAPI import dependencies as cd

    # Internal
    from modules import caller

    mp.set_start_method('spawn')
        
    ######################
    ## Get user's input ##
    ######################
    
    ## 1. Define parser ##
    ### Mandatory arguments
    parser = argparse.ArgumentParser(description='Call mobile element insertions (MEI) from long-read data. Two running modes: 1) SINGLE: individual sample; 2) PAIRED: affected and matched healthy sample')
    parser.add_argument('bam', help='Input bam file. It corresponds to the affected sample in PAIRED mode', type=lambda x: cd.file_exists(x, 'bam'))
    parser.add_argument('technology', help='Sequencing technology used to generate the data: rs for PacBio RSII, sq for PacBio Sequel, ccs for PacBio CCS reads and ont for Oxford Nanopore')
    parser.add_argument('reference', help='Reference genome in fasta format. Reference indexes must be available in the same directory (Minimap2 and bwa mem)', type=lambda x: cd.file_exists(x, 'reference'))
    parser.add_argument('refDir', help='Directory containing reference databases (consensus sequences, source elements...)')

    ### Optional arguments
    ## General
    parser.add_argument('--species', default='Homo sapiens', dest='species', help='Target species. Default: Homo sapiens')
    parser.add_argument('--build', default='GRCh38', dest='build', help='Reference genome build. Default: GRCh38')
    parser.add_argument('--normalBam', default=None, dest='normalBam', help='Matched normal bam file. If provided, MEIGA will run in PAIRED mode', type=lambda x: cd.file_exists(x, 'normalBam'))
    parser.add_argument('--germlineDb', default=None, dest='germlineMEI', help='Bed file containing set of known germline MEIs')
    parser.add_argument('--transduction-search', default=None, dest='transductionSearch', type=str, help='Enable transduction search on the coordinates given. Potential source elements')
    parser.add_argument('--gene-annot-dir', default=None, dest='annovarDir', help='Directory containing annovar reference files for gene-based annotation of MEI breakpoints. If not provided gene annotation step will be skipped')
    parser.add_argument('--polishing-rounds', default=3, dest='rounds', type=int, help='Number of polishing rounds to be attempted. Default: 1')
    parser.add_argument('--genotyper', action="store_true", default=False, dest='genotyper', help='Genotype MEIs and report VAF estimation')
    parser.add_argument('-p', '--processes', default=1, dest='processes', type=int, help='Number of processes. Default: 1')

    ## BAM processing
    parser.add_argument('--targetBins', default=None, dest='targetBins', type=str, help='Bed file containing target genomic bins for SV calling. Overrides --binSize and --refs. Default: None')
    parser.add_argument('-bS', '--binSize', default=1000000, dest='binSize', type=int, help='Input bams will be analised in genomic bins of this size. Default: 1000000')
    parser.add_argument('--refs', default="ALL", dest='refs', type=str, help='Comma separated list of target references to call SV (i.e. 1,2,3,X). Default: All references included in the bam file')
    parser.add_argument('--target-SV', default="INS,BND", dest='targetSV', type=str, help='Comma separated list of SV types to be detected (INS AND/OR BND). Default: INS,BND')
    parser.add_argument('--no-duplicates', action="store_true", default=False, dest='filterDuplicates', help='Filter out reads marked as duplicates if filter enabled')
    parser.add_argument('--minMAPQ', default=15, dest='minMAPQ', type=int, help='Minimum mapping quality required for each read. Default: 20')
    parser.add_argument('--readOverhang', default=5000, dest='overhang', type=int, help='Number of flanking base pairs around the SV event to be collected from the supporting read sequence. Default: 5000')
    parser.add_argument('--minINSlen', default=50, dest='minINSlen', type=int, help='Minimum insertion length. Default: 50')
    parser.add_argument('--minDELlen', default=70, dest='minDELlen', type=int, help='Minimum deletion length. Default: 100')
    parser.add_argument('--minCLIPPINGlen', default=700, dest='minCLIPPINGlen', type=int, help='Minimum clipped sequence length for each read. Default: 500')
    parser.add_argument('--hpTag', default=False, action="store_true", dest='hpTag', help='Slit calling by each haplotag. Set to True. Default: False')

    ## Clustering
    parser.add_argument('--INSdist', default=250, dest='maxInsDist', type=int, help='Maximum distance bewteen two adjacent INS to be clustered together (Between 0-999). Default: 250')
    parser.add_argument('--BKPdist', default=50, dest='maxBkpDist', type=int, help='Maximum distance bewteen two adjacent breakpoints for CLIPPING clustering (Between 0-999). Default: 50')
    parser.add_argument('--minPercOverlap', default=70, dest='minPercRcplOverlap', type=int, help='Minimum percentage of reciprocal overlap for DEL clustering. Default: 70')

    ## Filtering
    parser.add_argument('--minClusterSize', default=2, dest='minClusterSize', type=int, help='Minimum number of reads composing a cluster. Default: 3')
    parser.add_argument('--maxClusterSize', default=500, dest='maxClusterSize', type=int, help='Maximum number of reads composing a metacluster. Default: 500')
    parser.add_argument('--maxClusterCV', default=40, dest='maxClusterCV', type=int, help='Maximum coefficient of variation of a metacluster. Default: 40')
    parser.add_argument('--minReads', default=3, dest='minReads', type=int, help='Minimum number of reads supporting a SV. Default: 3')
    parser.add_argument('--minNormalReads', default=2, dest='minNormalReads', type=int, help='Minimum number of reads supporting a SV in normal sample. Default: 2')
        
    # Output
    parser.add_argument('--extraFields', default=False, action="store_true", dest='extraFields', help='If selected, extra INFO fields will be displayed in output (read ids, hp tags, RT details, etc)')
    parser.add_argument('-o', '--outDir', default=os.getcwd(), dest='outDir', help='Output directory. Default: current working directory')
    parser.add_argument('--no-cleanUp', action="store_false", default=True, dest='noCleanUp', help='If selected, not clean up')

    ## 2. Parse user's input and initialize variables ##
    args = parser.parse_args()

    ### Mandatory arguments
    bam = args.bam
    technology = args.technology
    reference = args.reference
    refDir = args.refDir

    ### Optional arguments
    ## General
    species = args.species
    build = args.build
    normalBam = args.normalBam
    transductionSearch = args.transductionSearch
    germlineMEI = args.germlineMEI
    annovarDir = args.annovarDir
    rounds = args.rounds
    genotyper = args.genotyper
    processes = args.processes

    ## BAM processing
    targetBins = args.targetBins
    binSize = args.binSize
    refs = args.refs
    targetSV = args.targetSV
    filterDuplicates = args.filterDuplicates
    minMAPQ = args.minMAPQ
    overhang = args.overhang
    minINSlen = args.minINSlen
    minDELlen = args.minDELlen
    minCLIPPINGlen = args.minCLIPPINGlen
    hpTag = args.hpTag

    ## Clustering
    minClusterSize = args.minClusterSize
    maxClusterSize = args.maxClusterSize
    maxInsDist = args.maxInsDist
    maxBkpDist = args.maxBkpDist
    minPercRcplOverlap = args.minPercRcplOverlap

    ## Filtering thresholds
    maxClusterCV = args.maxClusterCV
    minReads = args.minReads
    minNormalReads = args.minNormalReads

    # Ouput
    extraFields = args.extraFields
    outDir = args.outDir
    noCleanUp = args.noCleanUp

    # if genotyper mode, modify the following args
    if genotyper:
        maxBkpDist = 3
        minClusterSize = 1

    ##################################################
    ## Check dependencies. Exit if some are missing ##
    ##################################################

    missing_dependencies = cd.missing_python_dependencies() or cd.missing_program_dependencies(annovarDir)
    missing_dependencies = missing_dependencies or cd.missing_input_files([bam, reference, refDir, normalBam, annovarDir, outDir]) or cd.missing_db(refDir, annovarDir)
    if missing_dependencies: exit(1)

    ###########################################
    # Input checks plus input data conversion #
    ###########################################

    # If no reference is specified, get all that are present in the bam file.
    if refs == 'ALL':
        refs = bamtools.get_refs(bam)

    # Convert comma-separated string inputs into lists:
    targetSV = targetSV.split(',')
    targetRefs = refs.split(',')

    ## Determine running mode:
    mode = 'SINGLE' if normalBam == None else 'PAIRED'

    ## If unknown technology provided raise an error and exit
    if technology not in ['rs', 'sq', 'ccs', 'ont']:
        log.info('[ERROR] Abort execution as ' + technology + ' technology subtype not supported')
        sys.exit(1)

    ## Abort if SV unknown SV type provided
    for SV_type in targetSV:		
        if SV_type not in ['INS', 'BND']:
            log.info('[ERROR] Abort execution as ' + SV_type + ' SV type not supported')
            sys.exit(1)
            
    ##############################################
    ## Display configuration to standard output ##
    ##############################################
    scriptName = os.path.basename(sys.argv[0])
    scriptName = os.path.splitext(scriptName)[0]
    version='3.0.1'
    print()
    print('***** ', scriptName, version, 'configuration *****')
    print('*** Mandatory arguments ***')
    print('bam: ', bam)
    print('technology: ', technology)
    print('reference: ', reference)
    print('refDir: ', refDir, "\n")

    print('*** Optional arguments ***')
    print('** General **')
    print('mode: ', mode)
    print('species: ', species)
    print('build: ', build)
    print('normalBam: ', normalBam)
    print('transduction-search: ', transductionSearch)
    print('polishing-rounds: ', rounds)
    print('genotyper mode: ', genotyper)
    print('processes: ', processes, "\n")

    print('** Annotation **')
    print('germlineMEI: ', germlineMEI)
    print('gene-annot-dir: ', annovarDir)

    print('** BAM processing **')
    print('targetBins: ', targetBins)
    print('binSize: ', binSize)
    print('targetRefs: ', refs)
    print('targetSV: ', targetSV)
    print('filterDuplicates: ', filterDuplicates)
    print('minMAPQ: ', minMAPQ)
    print('overhang: ', overhang)
    print('minINSlength: ', minINSlen)
    print('minDELlength: ', minDELlen)
    print('minCLIPPINGlength: ', minCLIPPINGlen)
    print('hpTag: ', hpTag, "\n")

    print('** Clustering **')
    print('minClusterSize: ', minClusterSize)
    print('maxClusterSize: ', maxClusterSize)
    print('maxInsDist: ', maxInsDist)
    print('maxBkpDist: ', maxBkpDist)
    print('minPercOverlap: ', minPercRcplOverlap)

    print('** Filtering **')
    print('maxClusterCV: ', maxClusterCV)
    print('minReads: ', minReads)
    print('minNormalReads: ', minNormalReads)

    print('** Output format**')
    print('extra output fields: ', extraFields)
    print('outDir: ', outDir) 
    print ('no-cleanUp: ', noCleanUp, "\n")
    print('***** Executing ', scriptName, '.... *****', "\n")

    ##########
    ## CORE ## 
    ##########

    ## 1. Create configuration dictionary
    #######################################
    confDict = {}

    scriptName = os.path.basename(sys.argv[0])
    scriptName = os.path.splitext(scriptName)[0]

    ## Mandatory
    confDict['technology_subtype'] = technology
    confDict['technology'] = "NANOPORE" if technology == "ont" else "PACBIO"

    ## General
    confDict['source'] = 'MEIGA-' + version
    confDict['species'] = species
    confDict['build'] = build
    confDict['germlineMEI'] = germlineMEI
    confDict['transductionSearch'] = transductionSearch
    confDict['annovarDir'] = annovarDir
    confDict['rounds'] = rounds
    confDict['genotyper'] = genotyper
    confDict['processes'] = processes

    ## BAM processing
    confDict['targetBins'] = targetBins
    confDict['binSize'] = binSize
    confDict['targetRefs'] = targetRefs
    confDict['filterDuplicates'] = filterDuplicates	
    confDict['overhang'] = overhang
    confDict['minMAPQ'] = minMAPQ
    confDict['minINSlen'] = minINSlen
    confDict['minDELlen'] = minDELlen
    confDict['minCLIPPINGlen'] = minCLIPPINGlen
    confDict['readFilters'] = None

    ## Target eventscd  events to search for
    confDict['targetSV'] = targetSV

    # a) INS 
    if set(confDict['targetSV']) == {'INS'}:
        confDict['targetEvents'] = ['INS', 'CLIPPING']

    # b) BND
    elif set(confDict['targetSV']) == {'BND'}:
        confDict['targetSV'].append('DEL')
        confDict['targetEvents'] = ['CLIPPING', 'DEL']

    # c)  BND and INS
    elif set(confDict['targetSV']) == {'INS', 'BND'}:
        confDict['targetSV'].append('DEL')
        confDict['targetEvents'] = ['INS', 'CLIPPING', 'DEL']

    ## Clustering
    confDict['maxInsDist'] = maxInsDist
    confDict['maxBkpDist'] = maxBkpDist
    confDict['minPercRcplOverlap'] = minPercRcplOverlap
        
    ## Filtering thresholds
    confDict['minClusterSize'] = minClusterSize
    confDict['maxClusterSize'] = maxClusterSize
    confDict['maxClusterCV'] = maxClusterCV
    confDict['minReads'] = minReads
    confDict['minNormalReads'] = minNormalReads
    confDict['minPercResolved'] = 40

    ## Output
    confDict['hpTag'] = False
    confDict['extraFields'] = extraFields
    confDict['noCleanUp'] = noCleanUp

    ## 2. Execute structural variation caller
    ###########################################
    ### Create caller for each haplotype
    if hpTag:

        # X corresponds to no tag
        for hp in [1,2,'X']:

            confDict['hpTag'] = hp
            hpOutDir = outDir + '/hp' + str(confDict['hpTag'])
            unix.mkdir(hpOutDir)
            call_mei = caller.MEI_caller(mode, bam, normalBam, reference, refDir, confDict, hpOutDir)
            call_mei.call()

    else:
        
        ### Create caller
        call_mei = caller.MEI_caller(mode, bam, normalBam, reference, refDir, confDict, outDir)

        ### Do calling
        call_mei.call()

    print('***** Finished! *****')
    print()
